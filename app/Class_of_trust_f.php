<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Class_of_trust_f extends Model
{
    protected $table = 'class_of_trust_f';

    public static function get_almost_all(){
		$rezult = static::where('userid', 0)
		->orwhere('userid', Auth::user()->id)
		->get();
		return $rezult;
	}

    public static function add($req) {	
		$id = static::insertGetId(['userid' => Auth::user()->id,
			'Класс' => $req['Новый_класс']]);
		return $id;
	}

}
