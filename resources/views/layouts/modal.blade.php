<!-- Modal -->
@if($target == "СОЗДАТЬ УГРОЗУ ОБЪЕКТА ОЦЕНКИ" || $target == "СОЗДАТЬ УГРОЗУ СРЕДЫ" || $target == "СОЗДАТЬ ПОЛИТИКУ" || $target == "СОЗДАТЬ ПРЕДПОЛОЖЕНИЕ" || $target == "СОЗДАТЬ ПОДКОМПОНЕНТ ДОВЕРИЯ")
<div class="modal fade bd-example-modal-lg" id="firstmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    @else
    <div class="modal fade bd-example-modal-xl" id="firstmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        @endif
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">{{$target}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body">
           <div class="container-fluid">



            @if($target == "СОЗДАТЬ УГРОЗУ ОБЪЕКТА ОЦЕНКИ" || $target == "СОЗДАТЬ УГРОЗУ СРЕДЫ")
            <div class="row">
             @if($target == "СОЗДАТЬ УГРОЗУ ОБЪЕКТА ОЦЕНКИ") <b>Аннотация угрозы объекта оценки:</b> @else <b>Аннотация угрозы среды:</b> @endif
             <textarea class="form-control" name="Аннотация" rows="2"></textarea>
           </div>
           <br>
           <div class="row">
             <b>Способ реализации:</b>
             <textarea class="form-control" name="Способ_реализации" rows="2"></textarea>
           </div>
           <br>
           <div class="row">
            <b>Используемые уязвимости:</b>
            <textarea class="form-control" name="Используемые_уязвимости" rows="2"></textarea>
          </div>
          <br>
          <div class="row">
            @if($target == "СОЗДАТЬ УГРОЗУ ОБЪЕКТА ОЦЕНКИ") <b>Возможные последствия реализации угрозы:</b> @else <b>Возможные последствия реализации угрозы среды:</b> @endif
            <textarea class="form-control" name="Возможные_последствия" rows="2"></textarea>
          </div>
          <br>
          <div class="row">
            <b>Вид информационных ресурсов, потенциально подверженых угрозе:</b>
            <textarea class="form-control" name="Вид_ресурсов" rows="2"></textarea>
          </div>
          <br>
          @endif
          @if ($target == "СОЗДАТЬ ПОЛИТИКУ")
          <div class="row">
            <b>Политика:</b>
            <textarea class="form-control" name="Политика" rows="2"></textarea>
          </div>
          <br>
          @endif
          @if ($target == "СОЗДАТЬ ПРЕДПОЛОЖЕНИЕ")
          <div class="row">
            <b>Категория предположения:</b>
            <textarea class="form-control" name="Категория_предположения" rows="2"></textarea>
          </div>
          <br>
          <div class="row">
            <b>Предположение:</b>
            <textarea class="form-control" name="Предположение" rows="2"></textarea>
          </div>
          <br>
          @endif
          <div class="row">
            @if($target == "СОЗДАТЬ УГРОЗУ ОБЪЕКТА ОЦЕНКИ" || $target == "СОЗДАТЬ УГРОЗУ СРЕДЫ" || $target == "СОЗДАТЬ ПОЛИТИКУ" || $target == "СОЗДАТЬ ПРЕДПОЛОЖЕНИЕ")
            <div class="col-md-4 ml-auto"><b>Рубрика:</b>
              <br><input type="checkbox" name="Рубрика[]" value="требование к архитектуре"> Требование к архитектуре
              <br><input type="checkbox" name="Рубрика[]" value="управление пользователем"> Управление пользователем
              <br><input type="checkbox" name="Рубрика[]" value="управление доступом"> Управление доступом
              <br><input type="checkbox" name="Рубрика[]" value="другие функции"> Другие функции
              <br><input type="checkbox" name="Рубрика[]" value="аудит"> Аудит
              <br><input type="checkbox" name="Рубрика[]" value="нет"> Нет
            </div>
            @endif
            @if($target == "СОЗДАТЬ УГРОЗУ ОБЪЕКТА ОЦЕНКИ" || $target == "СОЗДАТЬ УГРОЗУ СРЕДЫ")
            <div class="col-md-4 ml-auto"><b>Нарушаемые свойства безопасности:</b>
              <br><input type="checkbox" name="Свойства[]" value="конфиденциальность"> Конфиденциальность
              <br><input type="checkbox" name="Свойства[]" value="целостность"> Целостность
              <br><input type="checkbox" name="Свойства[]" value="доступность"> Доступность
            </div>
            <div class="col-md-4 ml-auto"><b>Источники угрозы:</b>
              <br><input type="checkbox" name="Источники[]" value="внутренний нарушитель"> Внутренний нарушитель
              <br><input type="checkbox" name="Источники[]" value="внешний нарушитель"> Внешний нарушитель
              <br><input type="checkbox" name="Источники[]" value="программное воздействие"> Программное воздействие
              <br><input type="checkbox" name="Источники[]" value="программное обеспечение ОС"> Программное обеспечение ОС
            </div>
            @endif
            @if($target == "СОЗДАТЬ ПОЛИТИКУ" || $target == "СОЗДАТЬ ПРЕДПОЛОЖЕНИЕ")
            <div class="col-md-6 ml-auto"><b>Мера:</b>
             <div class="form-group">
              <select class="form-control" name="Мера">
               <option>Предупреждает</option>
               <option>Обнаруживает</option>
               <option>Корректирует</option>
               <option>Нет</option>
             </select>
           </div>
         </div>
         @endif
       </div>
       @if($target == "СОЗДАТЬ УГРОЗУ ОБЪЕКТА ОЦЕНКИ" || $target == "СОЗДАТЬ УГРОЗУ СРЕДЫ")
       <br>
       <div class="row">
        <div class="col-md-12 ml-auto"><b>Мера:</b>
         <div class="form-group">
          <select class="form-control" name="Мера">
           <option>Предупреждает</option>
           <option>Обнаруживает</option>
           <option>Корректирует</option>
           <option>Нет</option>
         </select>
       </div>
     </div>
   </div>
   @endif



   @if($target == "СФОРМИРОВАТЬ ЦЕЛЬ ОБЪЕКТА ОЦЕНКИ")
   <b>Угрозы объекта оценки:</b>
   <div class="prokrutka shadow">
    @if(!empty($id_un_uses_threats))
    <div class="accordion" id="accordionExample3">
      @foreach ($id_un_uses_threats as $name2)
      @foreach ($name2 as $name)
      <div class="card">
        <div class="card-header row" id="heading{{$name->id}}ugroza">
          <div class="col-sm">
            <input class="bigcheckmodal" name="ugroza[]" value="{{$name->id}}" type="checkbox"/>
          </div>
          <div class="col-sm-11">
            <h5 class="mb-0">
              <button class="btn btn-link accordtext" type="button" data-toggle="collapse" data-target="#collapse{{$name->id}}ugroza" aria-expanded="true" aria-controls="collapse{{$name->id}}ugroza">
                Нераспределенна: {{$name->Аннотация}}
              </button>
            </h5>
          </div>
        </div>
        <div id="collapse{{$name->id}}ugroza" class="collapse" aria-labelledby="heading{{$name->id}}ugroza" data-parent="#accordionExample3">
          <div class="card-body">
            <b>Аннотация:</b> {{$name->Аннотация}} <br>
            <b>Источники:</b> {{$name->Источники}} <br>
            <b>Способ реализации:</b> {{$name->Способ_реализации}} <br>
            <b>Используемые уязвимости:</b> {{$name->Используемые_уязвимости}} <br>
            <b>Вид информационных ресурсов потенциально подверженных угрозе:</b> {{$name->Вид_информационных_ресурсов_потенциально_подверженных_угрозе}} <br>
            <b>Нарушаемые свойства безопасности информационных ресурсов:</b> {{$name->Нарушаемые_свойства_безопасности_информационных_ресурсов}} <br>
            <b>Возможные последствия реализации:</b> {{$name->Возможные_последствия_реализации}} <br>
            <b>Рубрика:</b> {{$name->Рубрика}} <br>
            <b>Мера:</b> {{$name->Мера}} <br>
            <b>Среда:</b> {{$name->Среда}} <br>
            <b>Тип:</b> {{$name->Тип}}                
          </div>
        </div>
      </div>
      @endforeach
      @endforeach
    </div>
    @endif
    @if(!empty($other_threats))
    <div class="accordion" id="accordionExample3">
      @foreach ($other_threats as $name2)
      @foreach ($name2 as $name)
      <div class="card">
        <div class="card-header row" id="heading{{$name->id}}ugroza">
          <div class="col-sm">
            <input class="bigcheckmodal" name="ugroza[]" value="{{$name->id}}" type="checkbox"/>
          </div>
          <div class="col-sm-11">
            <h5 class="mb-0">
              <button class="btn btn-link accordtext" type="button" data-toggle="collapse" data-target="#collapse{{$name->id}}ugroza" aria-expanded="true" aria-controls="collapse{{$name->id}}ugroza">
                {{$name->Аннотация}}
              </button>
            </h5>
          </div>
        </div>
        <div id="collapse{{$name->id}}ugroza" class="collapse" aria-labelledby="heading{{$name->id}}ugroza" data-parent="#accordionExample3">
          <div class="card-body">
            <b>Аннотация:</b> {{$name->Аннотация}} <br>
            <b>Источники:</b> {{$name->Источники}} <br>
            <b>Способ реализации:</b> {{$name->Способ_реализации}} <br>
            <b>Используемые уязвимости:</b> {{$name->Используемые_уязвимости}} <br>
            <b>Вид информационных ресурсов потенциально подверженных угрозе:</b> {{$name->Вид_информационных_ресурсов_потенциально_подверженных_угрозе}} <br>
            <b>Нарушаемые свойства безопасности информационных ресурсов:</b> {{$name->Нарушаемые_свойства_безопасности_информационных_ресурсов}} <br>
            <b>Возможные последствия реализации:</b> {{$name->Возможные_последствия_реализации}} <br>
            <b>Рубрика:</b> {{$name->Рубрика}} <br>
            <b>Мера:</b> {{$name->Мера}} <br>
            <b>Среда:</b> {{$name->Среда}} <br>
            <b>Тип:</b> {{$name->Тип}}                
          </div>
        </div>
      </div>
      @endforeach
      @endforeach
    </div>
    @endif
  </div>
  <br>
  <b>Политики:</b>
  <div class="prokrutka shadow">
    @if(!empty($id_un_uses_policy))
    <div class="accordion" id="accordionExample4">
      @foreach ($id_un_uses_policy as $name2)
      @foreach ($name2 as $name)
      <div class="card">
        <div class="card-header row" id="heading{{$name->id}}policy">
          <div class="col-sm">
            <input class="bigcheckmodal" name="policy[]" value="{{$name->id}}" type="checkbox"/>
          </div>
          <div class="col-sm-11">
            <h5 class="mb-0">
              <button class="btn btn-link accordtext" type="button" data-toggle="collapse" data-target="#collapse{{$name->id}}policy" aria-expanded="true" aria-controls="collapse{{$name->id}}policy">
                Нераспределенна: {{$name->Политика}}
              </button>
            </h5>
          </div>
        </div>
        <div id="collapse{{$name->id}}policy" class="collapse" aria-labelledby="heading{{$name->id}}policy" data-parent="#accordionExample4">
          <div class="card-body">
            <b>Политика:</b> {{$name->Политика}} <br>
            <b>Рубрика:</b> {{$name->Рубрика}} <br>
            <b>Мера:</b> {{$name->Мера}} <br>
            <b>Тип:</b> {{$name->Тип}} <br>
            <b>Среда:</b> {{$name->Среда}} <br>                
          </div>
        </div>
      </div>   
      @endforeach
      @endforeach
    </div>
    @endif
    @if(!empty($other_policy))
    <div class="accordion" id="accordionExample4">
      @foreach ($other_policy as $name2)
      @foreach ($name2 as $name)
      <div class="card">
        <div class="card-header row" id="heading{{$name->id}}policy">
          <div class="col-sm">
            <input class="bigcheckmodal" name="policy[]" value="{{$name->id}}" type="checkbox"/>
          </div>
          <div class="col-sm-11">
            <h5 class="mb-0">
              <button class="btn btn-link accordtext" type="button" data-toggle="collapse" data-target="#collapse{{$name->id}}policy" aria-expanded="true" aria-controls="collapse{{$name->id}}policy">
                {{$name->Политика}}
              </button>
            </h5>
          </div>
        </div>
        <div id="collapse{{$name->id}}policy" class="collapse" aria-labelledby="heading{{$name->id}}policy" data-parent="#accordionExample4">
          <div class="card-body">
            <b>Политика:</b> {{$name->Политика}} <br>
            <b>Рубрика:</b> {{$name->Рубрика}} <br>
            <b>Мера:</b> {{$name->Мера}} <br>
            <b>Тип:</b> {{$name->Тип}} <br>
            <b>Среда:</b> {{$name->Среда}} <br>                
          </div>
        </div>
      </div>  
      @endforeach
      @endforeach
    </div>
    @endif
  </div>
  <br>
  <div class="row">
    <div class="col-md-12 ml-auto">
      <b>Название цели:</b>
      <input class="form-control" name="Название"></input>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-6 ml-auto">
      <b>Описание цели:</b>
      <textarea class="form-control" name="Описание" rows="4"></textarea>
    </div>
    <div class="col-md-6 ml-auto">
      <b>Обоснование цели:</b>
      <textarea class="form-control" name="Обоснование" rows="4"></textarea>
    </div>
  </div>
  @endif



  @if($target == "СФОРМИРОВАТЬ ЦЕЛЬ СРЕДЫ")
  <b>Угрозы среды:</b>
  <div class="prokrutka shadow">
    <?php $i=0; $fck=0; if(empty($id_un_uses_threats_envi) || empty($other_threats_envi)) $i=1;   ?>
    @for($i; $i<2; $i++)
    <div class="accordion" id="accordionExample3">
      <?php if($i==0) {$s=$id_un_uses_threats_envi; $id_un_uses_threats_envi="";} else 
      if (!empty($other_threats_envi)) {
        $s=$other_threats_envi; 
        $other_threats_envi = "";
      }
      else{
        $s=$id_un_uses_threats_envi; $id_un_uses_threats_envi=""; $fck=1;
      }
      ?>      
      @foreach ($s as $name2)
      @foreach ($name2 as $name)        
      <div class="card">
        <div class="card-header row" id="heading{{$name->id}}ugroza2">
          <div class="col-sm">
            <input class="bigcheckmodal" name="ugroza[]" value="{{$name->id}}" type="checkbox"/>
          </div>
          <div class="col-sm-11">
            <h5 class="mb-0">
              <button class="btn btn-link accordtext" type="button" data-toggle="collapse" data-target="#collapse{{$name->id}}ugroza2" aria-expanded="true" aria-controls="collapse{{$name->id}}ugroza2">
                @if($i==0 || $fck==1)Нераспределенна: @endif{{$name->Аннотация}}
              </button>
            </h5>
          </div>
        </div>
        <div id="collapse{{$name->id}}ugroza2" class="collapse" aria-labelledby="heading{{$name->id}}ugroza2" data-parent="#accordionExample3">
          <div class="card-body">
            <b>Аннотация:</b> {{$name->Аннотация}} <br>
            <b>Источники:</b> {{$name->Источники}} <br>
            <b>Способ реализации:</b> {{$name->Способ_реализации}} <br>
            <b>Используемые уязвимости:</b> {{$name->Используемые_уязвимости}} <br>
            <b>Вид информационных ресурсов потенциально подверженных угрозе:</b> {{$name->Вид_информационных_ресурсов_потенциально_подверженных_угрозе}} <br>
            <b>Нарушаемые свойства безопасности информационных ресурсов:</b> {{$name->Нарушаемые_свойства_безопасности_информационных_ресурсов}} <br>
            <b>Возможные последствия реализации:</b> {{$name->Возможные_последствия_реализации}} <br>
            <b>Рубрика:</b> {{$name->Рубрика}} <br>
            <b>Мера:</b> {{$name->Мера}} <br>
            <b>Среда:</b> {{$name->Среда}} <br>
            <b>Тип:</b> {{$name->Тип}}                
          </div>
        </div>
      </div>
      @endforeach
      @endforeach
    </div>
    @endfor
  </div>
  <br>
  <b>Предположения:</b>
  <div class="prokrutka shadow">
    <?php $z=0; $fck=0; if(empty($id_un_uses_hypothesis) || empty($other_hypothesis)) $z=1;   ?>
    @for($z; $z<2; $z++)
    <div class="accordion" id="accordionExample4">
      <?php if($z==0) {$s2=$id_un_uses_hypothesis; $id_un_uses_hypothesis="";} else 
      if(!empty($other_hypothesis)){
        $s2=$other_hypothesis; 
        $other_hypothesis = "";
      }
      else{
        $s2=$id_un_uses_hypothesis; $id_un_uses_hypothesis=""; $fck=1;
      }
      ?>      
      @foreach ($s2 as $name2)
      @foreach ($name2 as $name)
      <div class="card">
        <div class="card-header row" id="heading{{$name->id}}hypothesis2">
          <div class="col-sm">
            <input class="bigcheckmodal" name="hypothesis[]" value="{{$name->id}}" type="checkbox"/>
          </div>
          <div class="col-sm-11">
            <h5 class="mb-0">
              <button class="btn btn-link accordtext" type="button" data-toggle="collapse" data-target="#collapse{{$name->id}}hypothesis2" aria-expanded="true" aria-controls="collapse{{$name->id}}hypothesis2">
                @if($z==0 || $fck==1)Нераспределенно: @endif{{$name->Предположение}}
              </button>
            </h5>
          </div>
        </div>
        <div id="collapse{{$name->id}}hypothesis2" class="collapse" aria-labelledby="heading{{$name->id}}hypothesis2" data-parent="#accordionExample4">
          <div class="card-body">
            <b>Предположение:</b> {{$name->Предположение}} <br>               
            <b>Категория:</b> {{$name->Категория}} <br>
            <b>Рубрика:</b> {{$name->Рубрика}} <br>
            <b>Мера:</b> {{$name->Мера}} <br>
            <b>Тип:</b> {{$name->Тип}} <br>
            <b>Среда:</b> {{$name->Среда}} <br>                 
          </div>
        </div>
      </div>   
      @endforeach
      @endforeach
    </div>
    @endfor
  </div>
  <br>
  <div class="row">
    <div class="col-md-12 ml-auto">
      <b>Название цели:</b>
      <input class="form-control" name="Название"></input>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-6 ml-auto">
      <b>Описание цели:</b>
      <textarea class="form-control" name="Описание" rows="4"></textarea>
    </div>
    <div class="col-md-6 ml-auto">
      <b>Обоснование цели:</b>
      <textarea class="form-control" name="Обоснование" rows="4"></textarea>
    </div>
  </div>
  @endif




  @if ($target == "ДОБАВИТЬ КОМПОНЕНТ ДОВЕРИЯ")

  <?PHP 
  $uncomponent2 = Array();
  foreach ($uncomponent as $name){
    if ($name['userid'] == 0 || $name['userid'] == Auth::user()->id){
      $array2[] = $name['id_класса'];
      $uncomponent2[] = $name;
    }
  }

  $class2 = array_count_values($array2);

  $id_class2 = array_values(array_unique($array2));

  ?> 
<div class="prokrutka shadow" style="height: 700px; width: 100%;">
  <table class="table table-hover table-bordered">
    <thead>
      <tr>
        <th scope="col">Класс доверия</th>
        <th scope="col">Компонент доверия</th>
        <th scope="col"> </th>
      </tr>
    </thead>
    <tbody>
      @foreach ($class2 as $index)
      <?PHP 
      $count2 = 0;
      ?>
      @foreach ($uncomponent2 as $name)
      @if ($name['id_класса'] == $id_class2[$count3])
      <?PHP $count2++; ?>
      @if ($count2 == 1)

      <tr>
        <td rowspan="{{$index}}"><?PHP echo $classtab[$name['id_класса']-1]['Класс']; ?></td>
        @if ($name['userid'] == 0)
        <td>{{$name->Компонент}}</td>
        @else
        <td><b>{{$name->Компонент}}</b></td>
        @endif
        <td><input name="newcomponent[]" value="{{$name->id}}" type="checkbox" style="transform:scale(2.0);" /></td>
      </tr>
      @else
      <tr>
        @if ($name['userid'] == 0)
        <td>{{$name->Компонент}}</td>
        @else
        <td><b>{{$name->Компонент}}</b></td>
        @endif
        <td><input name="newcomponent[]" value="{{$name->id}}" type="checkbox" style="transform:scale(2.0);" /></td>
      </tr>
      @endif
      @endif
      @endforeach
      <?PHP $count3++; ?>
      @endforeach
    </tbody>
  </table>
 </div>
 <br>
  @endif

  @if ($target == "СОЗДАТЬ ПОДКОМПОНЕНТ ДОВЕРИЯ" && $step == 9)
  <div class="row">
    <div class="col-md-12 ml-auto">
      <b>Описание подкомпонента:</b>
      <textarea class="form-control" name="Описание" rows="4"></textarea>
    </div>
  </div>
  <br>
  <div class="row">
   <div class="col-md-6 ml-auto"><b>Тип элемента:</b>
     <div class="form-group">
      <select class="form-control" name="Тип">
        <option value='0'>Выберете</option>
        <option value='Элементы действий разработчика'>Элементы действий разработчика</option>
        <option value='Элементы действий оценщика'>Элементы действий оценщика</option>
        <option value='Элементы содержания и представления свидетельств'>Элементы содержания и представления свидетельств</option>
      </select>
    </div>
  </div>
  <div class="col-md-6 ml-auto"><b>Компонент:</b>
   <div class="form-group">
    <select class="form-control" name="Комп">
     <option value='0'>Выберете</option>
     @foreach($us as $name)
     <option value='{{$name}}'>{{$name}}</option>
     @endforeach
   </select>
 </div>
</div>
</div>
<div class="row">
  <div class="col-md-12 ml-auto">
    <b>Название подкомпонента:</b>
    <textarea class="form-control" name="Название" rows="1"></textarea>
  </div>
</div>
<br>
@endif

@if ($target == "СОЗДАТЬ ПОДКОМПОНЕНТ ДОВЕРИЯ" && $step == 11)
<div class="row">
  <div class="col-md-12 ml-auto">
    <b>Описание подкомпонента:</b>
    <textarea class="form-control" name="Описание" rows="4"></textarea>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-6 ml-auto"><b>Компонент:</b>
   <div class="form-group">
    <select class="form-control" name="Комп">
     <option value='0'>Выберете</option>
     @foreach($us as $name)
     <option value='{{$name}}'>{{$name}}</option>
     @endforeach
   </select>
 </div>
</div>
<div class="col-md-6 ml-auto"><b>Название подкомпонента:</b>
  <textarea class="form-control" name="Название" rows="1"></textarea>
</div>
</div>
<br>
@endif

@if ($target == "СВЯЗАТЬ ЦЕЛЬ С КОМПОНЕНТАМИ")
<div class="row">
  <div class="col-md-4 ml-auto">
    <div class="row">
      <div class="col-md-12 ml-auto">
        <div class="form-group">
           <select class="form-control" name="picked_aim" id="0" onchange="sdf(this);">
              <option value='0'>Выберите</option>
              @foreach ($nameAims as $value)
                @foreach ($value as $value2)
                  @if($value2['userid'] != 0)
                    <option value="{{$value2['id']}}">{{$value2['Название']}}</option>
                  @endif
                @endforeach
              @endforeach
          </select>
        </div>
      </div>
      @foreach ($nameAims as $value)
        @foreach ($value as $value2)
           @if($value2['userid'] != 0)
              <div class="col-md-12 ml-auto" style="display: none;" id="{{$value2['id']}}div">
                <b>Название: </b>{{$value2['Название']}}
                <br><b>Описание: </b>{{$value2['Описание']}}
                <br><b>Среда: </b>{{$value2['Среда']}}
                <br><b>Тип: </b>{{$value2['Тип']}}
                <br><b>Обоснование: </b>{{$value2['Обоснование']}}
              </div>
            @endif
          @endforeach
        @endforeach
    </div>
    <script type="text/javascript">
      function sdf(el){
        if(el.value != "0"){
          document.getElementById('buttttt').removeAttribute('disabled');
        }
        else {
          document.getElementById('buttttt').disabled = "true";
        }
          try{
            document.getElementById(el.id + "div").style.display = "none";
          }catch {}
          el.id = el.value;
          try{
            document.getElementById(el.id + "div").style.display = "inline";
          }catch{}

      }
    </script>
  </div>
  <div class="col-md-8 ml-auto">
    <div class="prokrutka shadow" style="height: 500px;">
      <table class="table table-hover table-bordered">
        <thead>
          <tr>
            <th scope="col">Компонент доверия</th>
              <th scope="col"></th>
            </tr>
        </thead>
          <tbody>
            @foreach ($allcomponent as $name)
              <tr>
                <td>{{$name->Компонент}}</td>
                <td><input name="Фки[]" value="{{$name->id}}" type="checkbox" style="transform:scale(2.0);" /></td>
              </tr>
            @endforeach
          </tbody>
      </table>
    </div>
  </div>
</div>
<br>
@endif

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
  <button type="submit" name="add" class="btn btn-primary" id="buttttt" <?php if($target == 'СВЯЗАТЬ ЦЕЛЬ С КОМПОНЕНТАМИ') echo 'disabled="true"'; ?>>@if($target == "СОЗДАТЬ УГРОЗУ ОБЪЕКТА ОЦЕНКИ" || $target == "СОЗДАТЬ УГРОЗУ СРЕДЫ" || $target == "СОЗДАТЬ ПОЛИТИКУ" || $target == "СОЗДАТЬ ПРЕДПОЛОЖЕНИЕ" || $target == "СОЗДАТЬ ПОДКОМПОНЕНТ ДОВЕРИЯ")Создать@elseif($target == "ДОБАВИТЬ КОМПОНЕНТ ДОВЕРИЯ")Добавить@elseif($target == "СВЯЗАТЬ ЦЕЛЬ С КОМПОНЕНТАМИ")Связать@elseСформировать@endif</button>
</div>
</div>
</div>
</div>
</div>