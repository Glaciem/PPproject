<?php

use App\Hypothesis;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Hypothesis::class, function (Faker $faker) {

    return [
        'Категория' => $faker->sentence(10),
        'Предположение' => $faker->text(300),
        'Рубрика' => $faker->sentence(10),
        'Мера' => $faker->sentence(10),
        'Среда_разработки' => $faker->sentence(10),
        'Среда' => $faker->randomElement(['ОС', 'МЭ']),
    ];
});
