<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Reports extends Model
{
    protected $table = 'reports';

    public static function add($array) {
		static::insert([
			'userid' => Auth::user()->id, 
  			'Характеристика' => $array['Характеристика'],
  		  	'Описание' => $array['Описание']
  		  ]);
	}
}
