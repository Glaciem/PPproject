<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Component_of_trust extends Model
{
	protected $table = 'component_of_trust';

	public static function idcomponent($array) {
		$rezult = array();
		if (isset($array['Зависимость'])){
			foreach ($array['Зависимость'] as $value) {
				$rezult[] = static::where('id', $value)->get();
			}
		}elseif (isset($array['component'])){
			foreach ($array['component'] as $value) {
				$rezult[] = static::where('id', $value)->get();
				}
			if (isset($array['addcomponent'])){
				foreach ($array['addcomponent'] as $value) {
					$rezult[] = static::where('id', $value)->get();
				}
			}
		}elseif (isset($array['addcomponent'])){
			foreach ($array['addcomponent'] as $value) {
				$rezult[] = static::where('id', $value)->get();
				}
			if (isset($array['component'])){
				foreach ($array['component'] as $value) {
					$rezult[] = static::where('id', $value)->get();
				}
			}
		}
		else{
			foreach ($array as $value) {
				$rezult[] = static::where('id', $value['id_компонента'])->get();
			}
		}		
		return $rezult;
	}

	public static function uncomponent($allcomponent, $component_template) {
		$rezult = array();

		foreach ($component_template as $name){
			foreach ($name as $value) {
				$component[] = $value;
			}
		}	

		foreach ($allcomponent as $name1) {
			$bool = true;
			foreach ($component as $name2) {
				if ($name1['Компонент'] == $name2['Компонент']){
					$bool = false;
				}
			}
			if ($bool == true) {
				$rezult[] = $name1;
			}
		}
		return $rezult;
	}

	public static function add($req, $zavisimost, $id_new_class) {
		$string = "Зависимости: ";
		if ($zavisimost == 0){
			$string .= "отсутствуют.";
		}else{
			foreach ($zavisimost as $value) {
				foreach ($value as $name) {
					$string .= $name['Компонент'];
					$string .= " ";
				}
			}
		}
		if ($id_new_class == "none"){
			$id = static::insertGetId(['userid' => Auth::user()->id,
				'id_класса' => $req['Класс'],
				'Компонент' => $req['Компонент'],
				'Зависимость' => $string]);
		}
		else {
			$id = static::insertGetId(['userid' => Auth::user()->id,
				'id_класса' => $id_new_class,
				'Компонент' => $req['Компонент'],
				'Зависимость' => $string]);
		}
		return $id;
	}

	public static function pointed($req) {
		$rezult = array();
		foreach ($req['newcomponent'] as $value) {
			$rezult[] = static::where('id', $value)->get();
		}
		return $rezult;
	}

	public static function name_comp($array) {
		$rezult = static::where('Компонент', $array['Комп'])->get('id');
		return $rezult;
	}

	public static function id_comp($id) {
		$rezult = array();
		foreach ($id as $value) {
			$rezult[] = static::where('id', $value)->get();
		}
		return $rezult;
	}

	public static function get_almost_all(){
		$rezult = static::where('userid', 0)
		->orwhere('userid', Auth::user()->id)
		->get();
		return $rezult;
	}
}
