<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Aims_envi extends Model
{
    protected $table = 'aims_envi';

    public static function aims_envi($array) {
        $rezult = static::where('Среда', $array['select1'])
        ->where('Тип', $array['select2'])
        ->where('userid', 0)
        ->orwhere(function ($query) use ($array){
           $query->where('Среда', $array['select1'])
           ->where('Тип', $array['select2'])
           ->where('userid', Auth::user()->id);
       })
        ->get();
        return $rezult;
    }

    public static function add($array, $coockie){
      $s = static::insert(['userid' => Auth::user()->id,
         'Название' => $array['Название'],
         'Описание' => $array['Описание'],
         'Среда' => $coockie['select1'],
         'Тип' => $coockie['select2'],
         'Обоснование' => $array['Обоснование']]);
      $id = static::where('userid', Auth::user()->id)
      ->where('Название', $array['Название'])
      ->where('Описание', $array['Описание'])
      ->where('Среда', $coockie['select1'])
      ->where('Тип', $coockie['select2'])
      ->where('Обоснование', $array['Обоснование'])
      ->get('id');   		
      return $id;
    }

    public static function id_aims($array) {
      $aims = array();
      foreach ($array as $id) {
        $aims[] = static::where('id', $id)->get();
      }
      return $aims;
    }

    public static function all_for_user(){
      $aims = static::where('userid', Auth::user()->id)->get();
      return $aims;
    }

    public static function del($id){
      $id = mb_substr($id, 0, -1);
      static::where('id', $id)->delete();
      return "Great";
    }
}
