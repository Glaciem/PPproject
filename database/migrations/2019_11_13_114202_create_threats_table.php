<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThreatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('threats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userid');
            $table->longText('Аннотация');
            $table->longText('Источники');
            $table->longText('Способ_реализации');
            $table->longText('Используемые_уязвимости');
            $table->longText('Вид_информационных_ресурсов_потенциально_подверженных_угрозе');
            $table->longText('Нарушаемые_свойства_безопасности_информационных_ресурсов');
            $table->longText('Возможные_последствия_реализации');
            $table->text('Рубрика');
            $table->text('Мера');
            $table->text('Среда');
            $table->text('Тип');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('threats');
    }
}
