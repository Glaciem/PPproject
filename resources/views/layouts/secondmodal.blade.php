 <div class="modal fade bd-example-modal-lg" id="secondmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">

    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{$target2}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
     <div class="modal-body">
       <div class="container-fluid">



        @if ($target2 == "СОЗДАТЬ КОМПОНЕНТ ДОВЕРИЯ")
        <div class="col">
          <div class="row">
            <b>Зависимости:</b>
            <div class="prokrutka shadow" style="height: 500px;">
              <table class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <th scope="col">Компонент доверия</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($allcomponent as $name)
                  <tr>
                    <td>{{$name->Компонент}}</td>
                    <td><input name="Зависимость[]" value="{{$name->id}}" type="checkbox" style="transform:scale(2.0);" /></td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <br>
          <div class="row">
          	<div class="col-sm-6" style="padding-left: 0px;">
            	<b>Класс доверия:</b>
            </div>
            <div class="col-sm-6">
            	<b>Название класса:  </b><font style="color: grey;"> пример (ATE название)</font>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6 form-group" style="padding-left: 0px;">
              <select class="form-control" name="Класс" onchange="change_input(this);">
               <option value='0'>Создать новый класс</option>
               @foreach ($almost_all_class as $class)
               <option value='{{$class["id"]}}' id='{{$class["id"]}}'>{{$class['Класс']}}</option>
               @endforeach
              </select>
           </div>
           <div class="col-sm-6" style="padding-right: 0px;">           		
           		<input type="text" class="form-control" id="name_classss" name="Новый_класс" onkeyup="write_input_class(this);">
           </div>
         </div>
         <br>
         <div class="row">
          <b>Название компонента: </b><font style="color: grey;"> пример (APE_SPD_EXT.1 название)</font>
          <input type="text" class="form-control" name="Компонент" rows="1" disabled="true" id="name_commmmm" onkeyup="write_input(this);">
        </div>
      </div>
      <br>
      @endif
      @if ($target2 == "СОЗДАТЬ КОМПОНЕНТ ДОВЕРИЯ ДЛЯ ЦЕЛИ")
      <div class="col">
        <div class="row">
          <b>Зависимости:</b>
          <div class="prokrutka shadow" style="height: 500px;">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th scope="col">Компонент доверия</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($allcomponent as $name)
                <tr>
                  <td>{{$name->Компонент}}</td>
                  <td><input name="Зависимость[]" value="{{$name->id}}" type="checkbox" style="transform:scale(2.0);" /></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-sm-6" style="padding-left: 0px;">
              <b>Класс доверия:</b>
            </div>
            <div class="col-sm-6">
              <b>Название класса:  </b><font style="color: grey;"> пример (FAU название)</font>
            </div>
        </div>
        <div class="row">
          <div class="col-sm-6 form-group" style="padding-left: 0px;">
            <select class="form-control" name="Класс" onchange="change_input(this);">
               <option value='0'>Создать новый класс</option>
               @foreach ($almost_all_class as $class)
               <option value='{{$class["id"]}}' id='{{$class["id"]}}'>{{$class['Класс']}}</option>
               @endforeach
              </select>
         </div>
         <div class="col-sm-6" style="padding-right: 0px;">               
              <input type="text" class="form-control" id="name_classss" name="Новый_класс" onkeyup="write_input_class(this);">
          </div>
       </div>
       <div class="row">
        <div class="form-group"><b>Рубрика:</b>
          <br><input type="checkbox" name="Рубрика[]" value="Аудит"> Аудит
          <br><input type="checkbox" name="Рубрика[]" value="Требования к архитектуре"> Требования к архитектуре
          <br><input type="checkbox" name="Рубрика[]" value="Защита каналов связи"> Защита каналов связи
          <br><input type="checkbox" name="Рубрика[]" value="Управление доступом"> Управление доступом
          <br><input type="checkbox" name="Рубрика[]" value="Собственная защита ОО"> Собственная защита ОО
          <br><input type="checkbox" name="Рубрика[]" value="Управление пользователями"> Управление пользователями
        </div>
      </div>
      <div class="row">
        <b>Цель объекта оценки:</b>
      </div>
      <div class="row">
        <div class="form-group">
          <select class="form-control" name="Aim">
           <option value='0'>Выберите</option>
           @foreach ($nameAims as $value)
           @foreach ($value as $value2)
           <option value="{{$value2['id']}}">{{$value2['Название']}}</option>
           @endforeach
           @endforeach
         </select>
       </div>
     </div>
     <div class="row">
          <b>Название компонента: </b><font style="color: grey;"> пример (FAU_IFF_EXT.1 название)</font>
          <input type="text" class="form-control" name="Компонент" rows="1" disabled="true" id="name_commmmm" onkeyup="write_input(this);">
        </div>
  </div>
  <br>
  @endif
      <script type="text/javascript">
        var cl = "";
        var was = false;
        var alf = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        function change_input(el){
          switch(el.value){
            case '0': cl = "";
            document.getElementById("name_classss").removeAttribute("disabled");
            break;
            default:
            x = document.getElementById(el.value).innerHTML;
            cl = x[0] + x[1] + x[2] + "_";
            document.getElementById("name_classss").disabled = "true";
            break;
          }
          document.getElementById("name_commmmm").value = cl;
          if (cl != "")            
            document.getElementById("name_commmmm").removeAttribute('disabled');
          else
            document.getElementById("name_commmmm").disabled = "true";
        }
        function write_input_class(el){
          if (alf.indexOf(el.value[0]) + 1 && alf.indexOf(el.value[1]) + 1 && alf.indexOf(el.value[2]) + 1 && el.value[3] == " " && el.value[4] != undefined && el.value[4] != " "){
            document.getElementById("name_commmmm").removeAttribute('disabled');
            document.getElementById("name_commmmm").value = el.value[0] + el.value[1] + el.value[2] + "_";
          }
          else
            document.getElementById("name_commmmm").disabled = "true";
          was = false;
        }
        s = "";
        function write_input(el){
          nums = "123456789";
          if (el.value.indexOf(cl) + 1){
            if (alf.indexOf(el.value[4]) + 1 && alf.indexOf(el.value[5]) + 1 && alf.indexOf(el.value[6]) + 1 && was == false){
              el.value += "_EXT.";
              s = el.value;
              was = true;
            }
            if (el.value.indexOf(s) + 1){
              if (nums.indexOf(el.value[12]) + 1 && el.value[13] == " " && el.value[14] != undefined && el.value[14] != " ")
                document.getElementById("create_buttttton").removeAttribute('disabled');
              else
                document.getElementById("create_buttttton").disabled = "true";
            }
            else
              el.value = s;
          }
          else {
            el.value = cl;
            s = "";
            was = false;
          }
        }
      </script>


</div>
<div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
  <button type="submit" name="add" class="btn btn-primary" id="create_buttttton" disabled="true">Создать
</div>
</div>
</div>
</div>
</div>