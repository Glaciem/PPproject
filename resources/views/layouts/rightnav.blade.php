<nav class="col-md-2 d-none d-md-block bg-light sidebar" style="right: 0; height:100%; position: fixed; padding-top:10px;">
	@if ($step != 0)
	<div class="sidebar-sticky">
		<nav class="nav nav-pills flex-column prokrutka2" style="padding-bottom: 10px;">
			<div style="padding:0; margin:0;">
			<span <?php if ($step == 1) {echo 'class="nav-link active"';} else { echo 'class="nav-link"';} ?>><a href="/index.php/step1" <?php if ($step == 1) {echo 'style="color:#fff;"';}?>><b>Этап 1.</b> <font class="rs">"Начало"</font></a></span>
			<span <?php if ($step == 2) {echo 'class="nav-link active"';} else { echo 'class="nav-link"';} ?>><a href="/index.php/step2" <?php if ($step == 2) {echo 'style="color:#fff;"';}?>><b>Этап 2.</b> <font class="rs">"Угрозы ОО"</font></a></span>
			<span <?php if ($step == 3) {echo 'class="nav-link active"';} else { echo 'class="nav-link"';} ?>><a href="/index.php/step3" <?php if ($step == 3) {echo 'style="color:#fff;"';}?>><b>Этап 3.</b> <font class="rs">"Угрозы среды"</font></a></span>
			<span <?php if ($step == 4) {echo 'class="nav-link active"';} else { echo 'class="nav-link"';} ?>><a href="/index.php/step4" <?php if ($step == 4) {echo 'style="color:#fff;"';}?>><b>Этап 4.</b> <font class="rs">"Политики"</font></a></span>
			<span <?php if ($step == 5) {echo 'class="nav-link active"';} else { echo 'class="nav-link"';} ?>><a href="/index.php/step5" <?php if ($step == 5) {echo 'style="color:#fff;"';}?>><b>Этап 5.</b> <font class="rs">"Предположения"</font></a></span>
			<span <?php if ($step == 6) {echo 'class="nav-link active"';} else { echo 'class="nav-link"';} ?>><a href="/index.php/step6" <?php if ($step == 6) {echo 'style="color:#fff;"';}?>><b>Этап 6.</b> <font class="rs">"Цели ОО"</font></a></span>
			<span <?php if ($step == 7) {echo 'class="nav-link active"';} else { echo 'class="nav-link"';} ?>><a href="/index.php/step7" <?php if ($step == 7) {echo 'style="color:#fff;"';}?>><b>Этап 7.</b> <font class="rs">"Цели среды"</font></a></span>
			<span <?php if ($step == 8) {echo 'class="nav-link active"';} else { echo 'class="nav-link"';} ?>><a href="/index.php/step8" <?php if ($step == 8) {echo 'style="color:#fff;"';}?>><b>Этап 8.</b> <font class="rs">"Компоненты доверия"</font></a></span>
			<span <?php if ($step == 9) {echo 'class="nav-link active"';} else { echo 'class="nav-link"';} ?>><a href="/index.php/step9" <?php if ($step == 9) {echo 'style="color:#fff;"';}?>><b>Этап 9.</b> <font class="rs">"Подкомпоненты"</font></a></span>
			<span <?php if ($step == 10) {echo 'class="nav-link active"';} else { echo 'class="nav-link"';} ?>><a href="/index.php/step10" <?php if ($step == 10) {echo 'style="color:#fff;"';}?>><b>Этап 10.</b> <font class="rs">"Компоненты требований"</font></a></span>
			<span <?php if ($step == 11) {echo 'class="nav-link active"';} else { echo 'class="nav-link"';} ?>><a href="/index.php/step11" <?php if ($step == 11) {echo 'style="color:#fff;"';}?>><b>Этап 11.</b> <font class="rs">"Подкомпоненты требований"</font></a></span>
			<span <?php if ($step == 12) {echo 'class="nav-link active"';} else { echo 'class="nav-link"';} ?>><a href="/index.php/step12" <?php if ($step == 12) {echo 'style="color:#fff;"';}?>><b>Этап 12.</b> <font class="rs">"Формирование"</font></a></span>
			</div>			
		</nav>		
		<div class="progress">
			<div class="progress-bar" role="progressbar" style="width: <?PHP echo ($step/12)*100;?>%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<input class=" btn-primary" type="button" onclick="back(this);" id={{$step}} style="width:100%; padding: 10px; font-weight: bold; border-radius: 3px; font-size: 12pt;" value="НАЗАД"/>
			</div>
			<?php if($step != 12){ ?>
				<div class="col-md-6">
					<button <?php if($step == '1' || $step == '2' || $step == '3' || $step == '4' || $step == '5') echo 'hidden=true'; if (($step == '6' || $step == '7') && empty($uses_aims)) echo 'hidden=true';  if ($step == '10' && $arrayComps == "<(0)") echo 'hidden=true';?> class=" btn-primary send" type="submit" style="width:100%; padding: 10px; font-weight: bold; border-radius: 3px;" id="NextButton"><?php if($step == 11) echo 'ЗАВЕРШИТЬ'; else echo 'ДАЛЕЕ' ?></button> <!-- когда уберем включенные всегда галочки, сделать эту кнопку по умолчанию скрытой hidden=true -->
				</div>
			<?php } ?>
		</div>
	</div>
</form>
@if(isset($target))
<!--	<form method="POST">
	{{ csrf_field() }} -->
	<br>
	<div class="row">
		<div class="col-md-12 ml-auto">
			<button <?php if(($step == '9' && empty($us)) || ($step == '11' && empty($us))) echo 'hidden=true'; ?> style="width:100%; padding: 10px; font-weight: bold; border-radius: 3px; font-size: 10pt;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#firstmodal" id="modalcreatebutton">{{$target}}</button>			
		</div>
	</div>
	<!--	</form> -->
	@endif
	@if(isset($target2))
	<br>
<!--	<form method="POST">
	{{ csrf_field() }} -->
	<div class="row">
		<div class="col-md-12 ml-auto">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#secondmodal" style="width:100%; padding: 10px; font-weight: bold; border-radius: 3px; font-size: 10pt;">{{$target2}}</button>			
		</div>
	</div>
	<!--	</form> -->
	@endif
	<br>
	<div class="row">
	</div>

	<style type="text/css">
	
	</style>
	@endif
</nav>
<script type="text/javascript">
	function back(el){
		var step = el.id.indexOf("step");
		step = Number(el.id.slice(step - 1));
		if (step > 1)
			step = "/index.php/step" + (step - 1);
		else
			step = "/index.php/step" + step;
		window.location.href = step;
	}
</script>