@extends('Main.layout')

@section('content')

@guest
<main role="main" class="col">
	<div class="jumbotron shadow">
		<div class="col">
			<h1 style="text-align: center;"><b><?php header('Refresh: 0; URL=/index.php'); ?></b></h1>
		</div>
	</div>
</main>
@else
<?php
	//if (isset($ret))
	//	header("Refresh:0");
	//echo '<pre>', print_r($req), '<pre>';
?>

<main role="main" class="col">
	<div class="jumbotron shadow">
		<?php $s = "u"; $i=0;?>
		<h2 class="headers_text">Список созданных вами параметров</h2>
		@for($i; $i < 6; $i++)

		@if($i == 0)
		<?php 
			if (empty($threats[0])) 
				$i++; 
			else {
				echo '<br><h5>Ваши угрозы объекта оценки:</h5>';
				$tabs = $threats; //u
				$threats = "";
			}
		?>
		@endif

		@if($i == 1)
		<?php 
			if (empty($threats_envi[0])) 
				$i++; 
			else {
				echo '<br><h5>Ваши угрозы среды:</h5>';
				$tabs = $threats_envi;
				$threats_envi = "";
				$s = "e"; //e
			}
		?>
		@endif
		
		@if($i == 2)
		<?php 
			if (empty($policy[0])) 
				$i++; 
			else {
				echo '<br><h5>Ваши политики:</h5>';
				$tabs = $policy;
				$policy = "";
				$s = "p"; //p
			}
		?>
		@endif

		@if($i == 3)
		<?php 
			if (empty($hypotheses[0])) 
				$i++; 
			else {
				echo '<br><h5>Ваши предположения:</h5>';
				$tabs = $hypotheses;
				$hypotheses = "";
				$s = "h"; //h
			}
		?>
		@endif

		@if($i == 4)
		<?php 
			if (empty($aims[0])) 
				$i++; 
			else {
				echo '<br><h5>Ваши цели объекта оценки:</h5>';
				$tabs = $aims;
				$aims = "";
				$s = "a"; //a
			}
		?>
		@endif

		@if($i == 5)
		<?php 
			if (empty($aims_threats[0])) 
				continue; 
			else {
				echo '<br><h5>Ваши цели среды:</h5>';
				$tabs = $aims_threats;
				$aims_threats = "";
				$s = "t"; //t
			}
		?>
		@endif


		<div class="accordion" id="accordionExample{{$i}}">
			@foreach($tabs as $name)
			<div class="card">
				<div class="card-header row" id="heading{{$name->id}}{{$s}}">
					<div class="col-sm-12">
						<h5 class="mb-0">
							<button class="btn btn-link accordtext" type="button" data-toggle="collapse" data-target="#collapse{{$name->id}}{{$s}}" aria-expanded="true" aria-controls="collapse{{$name->id}}{{$s}}">
								@if($i == 0 || $i == 1)
								{{$name->Аннотация}}
								@elseif ($i == 2)
								{{$name->Политика}}
								@elseif ($i == 3)
								{{$name->Предположение}}
								@elseif ($i == 4 || $i == 5)
								{{$name->Название}}
								@endif
							</button>
						</h5>
					</div>
				</div>
				<div id="collapse{{$name->id}}{{$s}}" class="collapse" aria-labelledby="heading{{$name->id}}{{$s}}" data-parent="#accordionExample{{$i}}">
					<div class="card-body">
						@if($i == 0 || $i == 1)
							<b>Аннотация:</b> {{$name->Аннотация}} <br>
							<b>Источники:</b> {{$name->Источники}} <br>
							<b>Способ реализации:</b> {{$name->Способ_реализации}} <br>
							<b>Используемые уязвимости:</b> {{$name->Используемые_уязвимости}} <br>
							<b>Вид информационных ресурсов потенциально подверженных угрозе:</b> {{$name->Вид_информационных_ресурсов_потенциально_подверженных_угрозе}} <br>
							<b>Нарушаемые свойства безопасности информационных ресурсов:</b> {{$name->Нарушаемые_свойства_безопасности_информационных_ресурсов}} <br>
							<b>Возможные последствия реализации:</b> {{$name->Возможные_последствия_реализации}} <br>
							<b>Рубрика:</b> {{$name->Рубрика}} <br>
							<b>Мера:</b> {{$name->Мера}} <br>
							<b>Среда:</b> {{$name->Среда}} <br>
							<b>Тип:</b> {{$name->Тип}}<br>
						@elseif ($i == 2)
							<b>Политика:</b> {{$name->Политика}} <br>
							<b>Рубрика:</b> {{$name->Рубрика}} <br>
							<b>Мера:</b> {{$name->Мера}} <br>
							<b>Тип:</b> {{$name->Тип}} <br>
							<b>Среда:</b> {{$name->Среда}} <br>	
						@elseif ($i == 3)
							<b>Предположение:</b> {{$name->Предположение}} <br>								
							<b>Категория:</b> {{$name->Категория}} <br>
							<b>Рубрика:</b> {{$name->Рубрика}} <br>
							<b>Мера:</b> {{$name->Мера}} <br>
							<b>Тип:</b> {{$name->Тип}} <br>
							<b>Среда:</b> {{$name->Среда}} <br>	
						@elseif ($i == 4 || $i == 5)
							<b>Название:</b> {{$name->Название}} <br>
							<b>Описание:</b> {{$name->Описание}} <br>
							<b>Среда:</b> {{$name->Среда}} <br>
							<b>Тип:</b> {{$name->Тип}} <br>
							<b>Обоснование:</b> {{$name->Обоснование}} <br>	
						@endif
						<form method="POST">
						<input name="{{$name->id}}{{$s}}" id="{{$name->id}}{{$s}}" hidden=true type="checkbox"/>
						{{ csrf_field() }}
						<button onclick="placeArrow(this);" id="{{$name->id}}{{$s}}" class=" btn-primary send" type="submit" style="width:100%; padding: 10px; font-weight: bold; border-radius: 3px;">УДАЛИТЬ</button></form>			
					</div>
				</div>
			</div>
			@endforeach
			</div>
			@endfor
		</div>

		<script type="text/javascript">
			function placeArrow(el){
				document.getElementById(el.id).checked="checked";
			}
		</script>

	</main>

	@endguest
	@endsection
