<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reductions extends Model
{
    protected $table = 'reductions';

    public static function get_def($arr) {
    	$rezult = static::where('Среда', 'def')
    	->where('Тип', 'def') 
        ->get();
    	return $rezult;
    }

    public static function get_selected($arr) {
    	$rezult = static::where('Среда', $arr['select1'])
    	->where('Тип', $arr['select2']) 
        ->get();
    	return $rezult;
    }
}
