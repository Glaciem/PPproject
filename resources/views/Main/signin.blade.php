@extends('Main.layout')
@section('content')
<br>
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col-sm">
		</div>
		<div class="col-sm">
			<h3 class="thin text-center"><b>Авторизация</b></h3>
			<p class="text-center text-muted"><a href="http://project.std-322.ist.mospolytech.ru/index.php/signup">Регистрация</a></p>
			<hr>
			<form method="POST" action="{{ route('login') }}">
				@csrf
				<div class="top-margin">
					<label>Email<span class="text-danger">*</span></label>
					 <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
				</div>
				<div class="top-margin">
					<label>Пароль<span class="text-danger">*</span></label>
					 <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
				</div>

				<div class="top-margin">
					<div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Запомнить меня') }}
                                    </label>
                                </div>
				</div>
				<hr>
				<div class="row">
					<div class="col-sm-12 text-center">
						<button class="btn btn-primary" style="width:100%;" type="submit">Войти</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-sm">
		</div>
	</div>
</div>
@endsection