<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Sub_component_of_trust extends Model
{
    protected $table = 'sub_component_of_trust';

    public static function all_sub_comp($array){
    	$rezult = array();
    	foreach ($array as $value)
    		foreach ($value as $id)
				$rezult[] = static::where('id_компонента', $id['id'])->get();		
    	return $rezult;
    }

    public static function add_sub($array, $comp){
    	static::insert(['id_компонента' => $comp[0]['id'],
			'Подкомпонент' => $array['Название'],
			'Элемент' => $array['Тип'],
			'Описание' => $array['Описание'],
            'userid' => Auth::user()->id]);
    	return "Great";
    }

    public static function id_sub($id){
      $rezult = array();
        foreach ($id as $value) {
          $rezult[] = static::where('id', $value)->get();
        } 
      return $rezult;
    }
}
