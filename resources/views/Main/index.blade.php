@extends('Main.layout')

@section('content')

<div class="container-fluid"  id="head">
	<div class="row align-items-start">
		<div class="col-md-5 mx-auto ">
			<h1 class="lead">Добро пожаловать!</h1>
			<p class="tagline">Сайт почти готов</p>
			@guest
			<p><a class="bbtn btn-primary btn-lg" role="button" href="http://project.std-322.ist.mospolytech.ru/index.php/signin">АВТОРИЗАЦИЯ</a></p>
			@else
			<p><a class="bbtn btn-primary btn-lg" role="button" href="/index.php/step1" onclick="pechenki();">НАЧАТЬ РАБОТУ</a>
			<a class="bbtn btn-primary btn-lg" role="button" href="http://project.std-322.ist.mospolytech.ru/index.php/usersaves">ПРОДОЛЖИТЬ</a></p>
			@endguest			
		</div>
	</div>
</div>


<script type="text/javascript">
	function CookiesDelete() {
		var cookies = document.cookie.split(";");
		for (var i = 0; i < cookies.length; i++) {
			var cookie = cookies[i];
			var eqPos = cookie.indexOf("=");
			var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
			document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT;";
			document.cookie = name + '=; path=/index.php; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		}
	}
	function pechenki(){
		CookiesDelete();		
		document.cookie = "WorkWith=0";
	}
</script>

<!--
<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
        <img src="/assets/images/bg_header.jpg" class="d-block w-100" alt="ОГОГОГОГО" width="40px">
        <div class="carousel-caption d-none d-md-block">
          <h5>Second slide label</h5>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
    </div>
  </div>
</div>
-->

<div class="container text-center">
	<br> <br>
	<h2 class="thin">Проект по созданию конструктора профилей защиты банковского ПО</h2>
	<p class="text-muted"> Просто ждем, когда все само заработает..	</p>
</div>

<section id="social">
	<div class="container">
		<div class="wrapper clearfix">
			<div class="addthis_toolbox addthis_default_style">
				<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
				<a class="addthis_button_tweet"></a>
				<a class="addthis_button_linkedin_counter"></a>
				<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
			</div>
		</div>
	</div>
</section>
@endsection