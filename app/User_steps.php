<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class User_steps extends Model
{
    protected $table = 'user_steps';

    public static function create($cell, $step) {	
		$id = static::insertGetId(['userid' => Auth::user()->id,
			$step => $cell]);
		return $id;
	}

	public static function add($cell, $step, $id_old) {
		$all = User_steps::get($id_old);
		$arr = [];
		foreach ($all as $key) {
			$key[$step] = $cell;
			$arr = $key;			
		}
		$id = static::insertGetId(['userid' => Auth::user()->id,
			'name' => $arr['name'],
			'step1' => $arr['step1'],
			'step2' => $arr['step2'],
			'step3' => $arr['step3'],
			'step4' => $arr['step4'],
			'step5' => $arr['step5'],
			'step6' => $arr['step6'],
			'step7' => $arr['step7'],
			'step8' => $arr['step8'],
			'step9' => $arr['step9'],
			'step10' => $arr['step10'],
			'step11' => $arr['step11'],
			'step12' => $arr['step12'],
			'FirstSelect' => $arr['FirstSelect'],
			'Threats' => $arr['Threats'],
			'ThreatsEnvi' => $arr['ThreatsEnvi'],
			'Policy' => $arr['Policy'],
			'Hypothesis' => $arr['Hypothesis'],
			'Aims' => $arr['Aims'],
			'AimsThreats' => $arr['AimsThreats'],
			'Components' => $arr['Components'],
			'AelemetsADD' => $arr['AelemetsADD'],
			'Aelemets' => $arr['Aelemets'],
			'Asubelements' => $arr['Asubelements'],
			'Components_f_oo' => $arr['Components_f_oo'],
			'FelemetsADD' => $arr['FelemetsADD'],
			'Felemets' => $arr['Felemets'],
			'Fsubelements' => $arr['Fsubelements']
		]);
		static::where('id', $id_old)->delete();
		return $id;
	}

	public static function get($id){
		$rezult = static::where('userid', Auth::user()->id)
		->where('id', $id)
		->get();
		return $rezult;
	}

	public static function try_get(){
		$rezult = static::where('userid', Auth::user()->id)->get();
		return $rezult;
	}
	public static function del($id){
		static::where('id', $id)->delete();
	}
}
