<!--<div class=" flex-column flex-md-row align-items-center p-3 px-md-4  bg-dark shadow-sm navbar-fixed-top">-->
<div class="navbar fixed-top flex-column flex-md-row align-items-center p-2 px-md-4  bg-dark shadow-sm">
	<a class="navbar-brand my-0 mr-md-auto font-weight-normal" href="/index.php"><img src="/assets/images/logo.png" alt="Progressus HTML5 template"></a>
	<nav class="my-2 my-md-0 mr-md-3">
		<a class="p-2 text-white" href="/">Главная</a>
	</nav>


	@guest
		<a class="btn btn-outline-primary" href="http://project.std-322.ist.mospolytech.ru/index.php/signin">Авторизация</a>
	@else
		<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
			{{ Auth::user()->name }} <span class="caret"></span>
		</a>

		<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
			<a class="dropdown-item" href="{{ route('logout') }}"
			onclick="event.preventDefault();
			document.getElementById('logout-form').submit();">
			{{ __('Logout') }}
		</a>

		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			@csrf
		</form>
	</div>
@endguest




</div>



<!--
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href="/"><img src="/assets/images/logo.png" alt="Progressus HTML5 template"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li class="active"><a href="/">Home</a></li>
					
					<li><a class="btn" href="http://project.std-322.ist.mospolytech.ru/index.php/signin">SIGN IN / SIGN UP</a></li>
				</ul>
			</div>
		</div>
	</div> -->