-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Хост: std-mysql
-- Время создания: Ноя 13 2019 г., 14:16
-- Версия сервера: 5.7.26-0ubuntu0.16.04.1
-- Версия PHP: 7.2.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `std_322`
--

-- --------------------------------------------------------

--
-- Структура таблицы `aims`
--

CREATE TABLE `aims` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Название` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Описание` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Среда` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Категория` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Обоснование` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `aims_envi`
--

CREATE TABLE `aims_envi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Название` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Описание` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Среда` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Категория` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Обоснование` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `aims_threats`
--

CREATE TABLE `aims_threats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_цели` int(11) NOT NULL,
  `id_угроз` int(11) NOT NULL,
  `id_политик` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `aims_threats_envi`
--

CREATE TABLE `aims_threats_envi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_цели_среды` int(11) NOT NULL,
  `id_угроз` int(11) NOT NULL,
  `id_политик` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `class_of_trust`
--

CREATE TABLE `class_of_trust` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Класс` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `class_of_trust_f`
--

CREATE TABLE `class_of_trust_f` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Класс` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `component_of_trust`
--

CREATE TABLE `component_of_trust` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_класса` int(11) NOT NULL,
  `Компонент` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Зависимость` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `component_of_trust_f`
--

CREATE TABLE `component_of_trust_f` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_класса` int(11) NOT NULL,
  `Компонент` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Зависимость` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `hypotheses`
--

CREATE TABLE `hypotheses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Категория` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Предположение` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Рубрика` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Мера` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Среда_разработки` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Среда` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_11_06_182543_create_tasks_table', 1),
(5, '2019_11_13_114202_create_threats_table', 1),
(6, '2019_11_13_114325_create_policy_table', 1),
(7, '2019_11_13_114725_create_aims_table', 1),
(8, '2019_11_13_114849_create_aims_threats_table', 1),
(9, '2019_11_13_115008_create_hypotheses_table', 1),
(10, '2019_11_13_115326_create_threats_envi_table', 1),
(11, '2019_11_13_115501_create_aims_envi_table', 1),
(12, '2019_11_13_115550_create_aims_threats_envi_table', 1),
(13, '2019_11_13_120451_create_component_of_trust_table', 1),
(14, '2019_11_13_120529_create_class_of_trust_table', 1),
(15, '2019_11_13_120714_create_sub_component_of_trust_table', 1),
(16, '2019_11_13_120839_create_template_f_table', 1),
(17, '2019_11_13_121451_create_template_level_3_table', 1),
(18, '2019_11_13_121535_create_component_of_trust_f_table', 1),
(19, '2019_11_13_121555_create_class_of_trust_f_table', 1),
(20, '2019_11_13_121628_create_sub_component_of_trust_f_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `policy`
--

CREATE TABLE `policy` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Политика` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Рубрика` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Мера` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Среда_разработки` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Среда` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sub_component_of_trust`
--

CREATE TABLE `sub_component_of_trust` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_компонента` int(11) NOT NULL,
  `Подкомпонент` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Элемент` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Описание` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sub_component_of_trust_f`
--

CREATE TABLE `sub_component_of_trust_f` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_компонента` int(11) NOT NULL,
  `Подкомпонент` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Элемент` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Описание` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE `tasks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lol` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tasks`
--

INSERT INTO `tasks` (`id`, `body`, `completed`, `created_at`, `updated_at`, `lol`) VALUES
(1, '1', 0, NULL, NULL, '2'),
(2, '3', 0, NULL, NULL, '4'),
(3, 'Белеберда', 0, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Структура таблицы `template_f`
--

CREATE TABLE `template_f` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_компонента` int(11) NOT NULL,
  `id_цели` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `template_level_3`
--

CREATE TABLE `template_level_3` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_класса` int(11) NOT NULL,
  `id_компонента` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `threats`
--

CREATE TABLE `threats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Аннотация` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Источники` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Способ_реализации` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Используемые_уязвимости` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Вид_информационных_ресурсов_потенциально_подверженных_угрозе` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Нарушаемые_свойства_безопасности_информационных_ресурсов` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Возможные_последствия_реализации` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Рубрика` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Мера` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Среда` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Тип` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `threats_envi`
--

CREATE TABLE `threats_envi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Аннотация` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Источники` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Способ_реализации` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Используемые_уязвимости` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Вид_информационных_ресурсов_потенциально_подверженных_угрозе` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Нарушаемые_свойства_безопасности_информационных_ресурсов` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Возможные_последствия_реализации` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Рубрика` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Мера` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Среда` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Среда_работы` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `aims`
--
ALTER TABLE `aims`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aims_envi`
--
ALTER TABLE `aims_envi`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aims_threats`
--
ALTER TABLE `aims_threats`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aims_threats_envi`
--
ALTER TABLE `aims_threats_envi`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `class_of_trust`
--
ALTER TABLE `class_of_trust`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `class_of_trust_f`
--
ALTER TABLE `class_of_trust_f`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `component_of_trust`
--
ALTER TABLE `component_of_trust`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `component_of_trust_f`
--
ALTER TABLE `component_of_trust_f`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hypotheses`
--
ALTER TABLE `hypotheses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `policy`
--
ALTER TABLE `policy`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sub_component_of_trust`
--
ALTER TABLE `sub_component_of_trust`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sub_component_of_trust_f`
--
ALTER TABLE `sub_component_of_trust_f`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `template_f`
--
ALTER TABLE `template_f`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `template_level_3`
--
ALTER TABLE `template_level_3`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `threats`
--
ALTER TABLE `threats`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `threats_envi`
--
ALTER TABLE `threats_envi`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `aims`
--
ALTER TABLE `aims`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `aims_envi`
--
ALTER TABLE `aims_envi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `aims_threats`
--
ALTER TABLE `aims_threats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `aims_threats_envi`
--
ALTER TABLE `aims_threats_envi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `class_of_trust`
--
ALTER TABLE `class_of_trust`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `class_of_trust_f`
--
ALTER TABLE `class_of_trust_f`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `component_of_trust`
--
ALTER TABLE `component_of_trust`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `component_of_trust_f`
--
ALTER TABLE `component_of_trust_f`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `hypotheses`
--
ALTER TABLE `hypotheses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `policy`
--
ALTER TABLE `policy`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `sub_component_of_trust`
--
ALTER TABLE `sub_component_of_trust`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `sub_component_of_trust_f`
--
ALTER TABLE `sub_component_of_trust_f`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `template_f`
--
ALTER TABLE `template_f`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `template_level_3`
--
ALTER TABLE `template_level_3`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `threats`
--
ALTER TABLE `threats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `threats_envi`
--
ALTER TABLE `threats_envi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
