@extends('Main.layout')

@section('content')

@guest
<main role="main" class="col">
	<div class="jumbotron shadow">
		<div class="col">
			<h1 style="text-align: center;"><b>УДИ АТСЮДА, РАЗБИЙНИК!!!</b></h1>
		</div>
	</div>
</main>

@else
<?php
	//echo '<pre>', print_r($_COOKIE), '<pre>';
?>



<main role="main" class="col">
	<div class="jumbotron shadow">
		<form method="POST" action="fake">
			{{ csrf_field() }}
			<div class="col">
				<h2 class="headers_text">Этап 1. Выбор первоначальных параметров</h2><br>
				<div class="input-group mb-3 ">
					<div class="input-group-prepend">
						<label class="input-group-text" for="inputGroupSelect1">Название профиля защиты:</label>
					</div>
					<input type="text" class="form-control" name="name_pz" id="name_pz" onkeyup="sel2();" placeholder="Это название не будет использоваться в документе">
				</div>
				<div class="input-group mb-3 ">
					<div class="input-group-prepend">
						<label class="input-group-text" for="inputGroupSelect1">Тип профиля защиты:</label>
					</div>
					<select name="select1" class="custom-select" id="inputGroupSelect1" onchange="sel2();">
						<option selected>Выберите</option>
						<option value="ОС">ОС</option>
						<option value="МЭ">МЭ</option>
						<option value="САЗ">САЗ</option>
						<option value="СДЗ">СДЗ</option>
						<option value="СКН">СКН</option>
						<option value="СОВ">СОВ</option>
					</select>
				</div>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<label class="input-group-text" for="inputGroupSelect2">Тип:</label>
					</div>
					<select name="select2" class="custom-select" id="inputGroupSelect2" onchange="check();">
						<option selected>Выберите тип профиля защиты</option>
					</select>
				</div>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<label class="input-group-text" for="inputGroupSelect3">Класс защиты:</label>
					</div>
					<select name="select3" class="custom-select" id="inputGroupSelect3" onchange="check();">
						<option selected>Выберите</option>
						<option value="ПЕРВОГО">Первый</option>
						<option value="ВТОРОГО">Второй</option>
						<option value="ТРЕТЬЕГО">Третий</option>
						<option value="ЧЕТВЕРТОГО">Четвертый</option>
					</select>
				</div>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<label class="input-group-text" for="inputGroupSelect4">Уровень доверия:</label>
					</div>
					<select name="select4" class="custom-select" id="inputGroupSelect4" onchange="check();">
						<option selected>Выберите</option>
						<option value="1">Первый</option>
						<option value="2">Второй</option>
						<option value="3">Третий</option>
						<option value="4">Четвертый</option>
						<option value="5">Пятый</option>
						<option value="6">Шестой</option>
						<option value="7">Седьмой</option>
						<option value="8">Восьмой</option>
						<option value="9">Девятый</option>
						<option value="10">Десятый</option>
					</select>
				</div>
			</div>					
		</div>
	</main>
	<script type="text/javascript">
		var os = new Array("А","Б","В");
		var me = new Array("А","Б","В","Г","Д");
		var saz = new Array("А","Б","В","Г");
		var sdz = new Array("ПР","УБ");
		var skn = new Array("Н","П");
		var sov = new Array("С","У");
		function sel2(){
			var Sreda = document.getElementById("inputGroupSelect1");
			var Type = document.getElementById("inputGroupSelect2");
			var selind = Sreda.options.selectedIndex;
			switch (selind){
				case 0:
				Type.options.length = 0;
				Type[0] = new Option("Выберите тип профиля защиты");
				break;
				case 1:
				Type.options.length = 0;
				for (var n = 0; n < os.length; n++){
					Type[n] = new Option(os[n], ('"' + os[n] + '"'));
				}
				break;
				case 2:
				Type.options.length = 0;
				for (var n = 0; n < me.length; n++){
					Type[n] = new Option(me[n],('"' + me[n] + '"'));
				}
				break;
				case 3:
				Type.options.length = 0;
				for (var n = 0; n < saz.length; n++){
					Type[n] = new Option(saz[n], ('"' + saz[n] + '"'));
				}
				break;
				case 4:
				Type.options.length = 0;
				for (var n = 0; n < sdz.length; n++){
					Type[n] = new Option(sdz[n], ('"' + sdz[n] + '"'));
				}
				break;
				case 5:
				Type.options.length = 0;
				for (var n = 0; n < skn.length; n++){
					Type[n] = new Option(skn[n], ('"' + skn[n] + '"'));
				}
				break;
				case 6:
				Type.options.length = 0;
				for (var n = 0; n < sov.length; n++){
					Type[n] = new Option(sov[n], ('"' + sov[n] + '"'));
				}
				break;
			}
			check();
		}
		function check(){			
			if( document.getElementById("inputGroupSelect1").options.selectedIndex != 0 && document.getElementById("inputGroupSelect3").options.selectedIndex != 0 && document.getElementById("inputGroupSelect4").options.selectedIndex != 0 && document.getElementById("name_pz").value != "" && document.getElementById("name_pz").value[0] != " ")
				document.getElementById("NextButton").hidden = false;
			else
				document.getElementById("NextButton").hidden = true;
		}
	</script>

	@endguest
	@endsection
