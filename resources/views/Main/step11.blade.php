@if(isset($req[0]))
	<?php echo '<meta http-equiv="refresh" content="0; http://project.std-322.ist.mospolytech.ru/index.php/'.$req[1].'">'; ?>
@else

@extends('Main.layout')

@section('content')

<?PHP  


//echo '<pre>', print_r($req), '<pre>'; 
$mass = array();
$mass2 = array();
if (isset($req['component'])){
	foreach ($req['component'] as $name){
		$mass[] = $name;
	}
	if (isset($req['addcomponent'])){
		foreach ($req['addcomponent'] as $name){
			$mass2[] = $name;
		}
		$json2 = json_encode($mass2);
		setcookie('FelemetsADD', $json2);
	}
	$json = json_encode($mass);
	setcookie('Felemets', $json);
}

if(!isset($req['addcomponent']) && !isset($req['Описание'])){
	SetCookie("FelemetsADD","");
}


?>

<main role="main" class="col">
	<div class="jumbotron shadow">
		<h2 class="headers_text">Этап 11. Дерево подкомпонентов доверия целей объекта оценки</h2>
			<?php 
			//if(isset($ret))
			//	header("Refresh:0");
			?>

		<form method="POST" action= "step12">
			{{ csrf_field() }}

		<?php
			echo '<pre>', print_r($req),  print_r($_COOKIE), '</pre>';
		?>
		<div class="row">
			<div class="col-6" >
				<div class="prokrutka shadow" style="height: 750px;  background-color: #f8f8ff;">
				<div class="list-group" id="list-tab" role="tablist">
					@foreach ($components as $name)
					@foreach ($name as $value)
					<?php $s = str_replace(" ", "", $value['Компонент']); $n = str_replace(".", "", $s); $count = -1;?>
					<a style="position:left; width:50%;" class="list-group-item list-group-item-action" id="{{$n}}" data-toggle="list" href="#list-{{$n}}" role="tab" aria-controls="{{$n}}"
					onclick="openB(this);">{{$value->Компонент}}</a>
					@foreach($sub_comps as $name2)
					@foreach($name2 as $value2)
					@if($value2['id_компонента'] == $value['id'])
					<?php $v = str_replace(" ", "", $value2['Подкомпонент']); $z = str_replace(".", "", $v); $count++;?>
					<a style="margin-left: auto;  margin-right: 0em; width:50%;" class="list-group-item list-group-item-action" id="{{$n}}SUB{{$count}}[]" data-toggle="list" href="#list-{{$z}}" role="tab" aria-controls="{{$z}}" hidden=true>{{$value2->Подкомпонент}}<input name="{{$value2['id']}}" hidden=true checked=true type="checkbox"/></a>
					@endif
					@endforeach
					@endforeach
					@endforeach
					@endforeach					
				</div>
			</div>
			</div>
			<div class="col-6 shadow" style="border: 1px solid #e0e0e0; background:white; border-radius:10px; padding: 10px;">
				<div class="tab-content" id="nav-tabContent">
					@foreach ($components as $name)
					@foreach ($name as $value) 
					<?php $s = str_replace(" ", "", $value['Компонент']); $n = str_replace(".", "", $s);?>
					<div class="tab-pane fade" id="list-{{$n}}" role="tabpanel" aria-labelledby="list-{{$n}}-list"><p style="text-align: justify;"><b>{{$value->Компонент}}</b><br>{{$value->Зависимость}}</p></div>
					@foreach($sub_comps as $name2)
					@foreach($name2 as $value2)
					@if($value2['id_компонента'] == $value['id'])
					<?php $v = str_replace(" ", "", $value2['Подкомпонент']); $z = str_replace(".", "", $v);?>
					<div class="tab-pane fade" id="list-{{$z}}" role="tabpanel" aria-labelledby="list-{{$z}}-list"><p style="text-align: justify;"><b>{{$value2->Подкомпонент}}</b><br><b>Описание: </b>{{$value2->Описание}}</p></div>
					@endif
					@endforeach
					@endforeach
					@endforeach
					@endforeach
				</div>
			</div>
		</div>
		<script type="text/javascript">
			var vis = [];
			function openB(el){
				i = 0;
				for (z = 0; z < vis.length; z++)
					document.getElementById(vis[z]).hidden = true;
				if(vis[0] != el.id + "SUB0[]"){
					vis = [];
					while(document.getElementById(el.id + "SUB" + i + "[]") != null){
						document.getElementById(el.id + "SUB" + i + "[]").hidden = false;
						vis.push(el.id + "SUB" + i + "[]");
						i++;
					}
				}
				else
					vis = [];
			}
		</script>

</div>
</main>
@endsection
@endif