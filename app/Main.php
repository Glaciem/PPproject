<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Main extends Model
{
	protected $table = 'threats';

    public static function threats($array) {
    	$threats = static::where('Среда', $array['select1'])
    		->where('Тип', $array['select2'])
    		->where('userid', 'select1')
    		->orwhere(function ($query) use ($array){
              $query->where('Среда', $array['select1'])
                ->where('Тип', $array['select2'])
                ->where('userid', Auth::user()->id);
            })  
          	->get();
		return $threats;
	}

	public static function add($array, $coockie) {
		$co = array();
		foreach ($array['Источники'] as $name) {
			$co[] = $name;
		}
		$is = implode(", ", $co);
		$co = array();
		foreach ($array['Рубрика'] as $name) {
			$co[] = $name;
		}
		$rub = implode(", ", $co);
		$co = array();
		foreach ($array['Свойства'] as $name) {
			$co[] = $name;
		}
		$sv = implode(", ", $co);
		$s = static::insert(['userid' => Auth::user()->id, 
  			'Аннотация' => $array['Аннотация'],
  		  	'Источники' => $is,
  		  	'Способ_реализации' => $array['Способ_реализации'],
  		  	'Используемые_уязвимости' => $array['Используемые_уязвимости'],
  		    'Вид_информационных_ресурсов_потенциально_подверженных_угрозе' => $array['Вид_ресурсов'],
  		    'Нарушаемые_свойства_безопасности_информационных_ресурсов' => $sv,
  		    'Возможные_последствия_реализации' => $array['Возможные_последствия'],
  			'Рубрика' => $rub,
  			'Мера' => $array['Мера'],
  			'Среда' => $coockie['select1'],
  		  	'Тип' => $coockie['select2']]);
	}

	public static function id_threats($array) {
		$threats = array();
		foreach ($array as $id) {
			$threats[] = static::where('id', $id)->get();
		}
		return $threats;
	}

	public static function all_for_user(){
		$threats = static::where('userid', Auth::user()->id)->get();
		return $threats;
	}

	public static function del($id){
		$id = mb_substr($id, 0, -1);
		static::where('id', $id)->delete();
		return "Great";
	}
}
