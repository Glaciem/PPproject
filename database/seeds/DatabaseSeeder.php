<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Main::class, 100)->create();
        factory(App\Threats_envi::class, 100)->create();
        factory(App\Policy::class, 100)->create();
        factory(App\Hypothesis::class, 100)->create();
    }
}
