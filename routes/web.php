<?php



Route::get('/', 'MainController@index');
Route::get('start', 'MainController@start');
Route::get('signin', 'MainController@showsignin');
Route::get('signup', 'MainController@showsignup');
Route::post('signup', 'MainController@signup');
Route::get('report', 'MainController@report');
Route::post('report', 'MainController@report');
Route::get('/step1', 'MainController@step1');
//Route::get('/fake', 'MainController@fake');
Route::post('/fake', 'MainController@fake');
Route::get('/step2', 'MainController@step2');
Route::post('/step2', 'MainController@step2');
Route::get('/step3', 'MainController@step3');
Route::post('/step3', 'MainController@step3');
Route::get('/step4', 'MainController@step4');
Route::post('/step4', 'MainController@step4');
Route::get('/step5', 'MainController@step5');
Route::post('/step5', 'MainController@step5');
Route::get('/step6', 'MainController@step6');
Route::post('/step6', 'MainController@step6');
Route::get('/step7', 'MainController@step7');
Route::post('/step7', 'MainController@step7');
Route::get('/step8', 'MainController@step8');
Route::post('/step8', 'MainController@step8');
Route::get('/step9', 'MainController@step9');
Route::post('/step9', 'MainController@step9');
Route::get('/step10', 'MainController@step10');
Route::post('/step10', 'MainController@step10');
Route::get('/step11', 'MainController@step11');
Route::post('/step11', 'MainController@step11');
Route::get('/step12', 'MainController@step12');
Route::post('/step12', 'MainController@step12');
Route::get('/step13', 'MainController@step13');
Route::post('/step13', 'MainController@step13');
Route::get('/usersparams', 'MainController@usersparams');
Route::post('/usersparams', 'MainController@usersparams');
Route::get('/usersaves', 'MainController@usersaves');
Route::post('/usersaves', 'MainController@usersaves');
Route::get('/create', 'Create@create');
Route::post('/create', 'Create@create');
Route::get('/download/{id}', 'Create@show')->name('downloadfile');




/*
Route::post('/step2', function () {
	$step = "Step2";
	$req = Request::all();
	//$threats = App\Main::threats();
	$threats = DB::table('threats')->get();
	//$threats = 1;
	return view('Main.step2', compact('step', 'req', 'threats'));
});*/

//Route::get('/tasks', 'TasksController@index');
//Route::get('/tasks/{task}', 'TasksController@show');

/*
Route::get('/', function () {
    return view('welcome');
});
*/


Route::get('/tasks', function () {
	//$tasks = DB::table('threats')->get();
	return view('tasks.index');
});
/*
Route::get('/tasks/{task}', function ($id) {
	//$task = DB::table('tasks')->find($id);
	$task = App\Task::find($id);
	//dd($task);
    return view('tasks.show', compact('task'));
});*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
