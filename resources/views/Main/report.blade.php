@extends('Main.layout')

@section('content')



<main role="main" class="col">
	<div class="jumbotron shadow" style="height: 1430px;">
		<form method="POST">
			{{ csrf_field() }}
			<img src="/img/Сердце.png" style="position: absolute;  width: 90%; /* height: 50%; */ z-index: 5;">
			<div class="col" style="position: absolute; z-index: 6;">
				<h2 class="headers_text">Мы будем рады вашим замечаниям!</h2><br>
				<b> Дайте краткую характеристику ошибки:</b>
				<textarea class="form-control" name="Характеристика" rows="1" style="width: 80%;"></textarea>
				<br>
				<b> Подробно опишите в чем заключается ошибка, как и в какой момент она произошла:</b>
				<textarea class="form-control" name="Описание" rows="6" style="width: 90%;"></textarea>
				<br>
				<button class=" btn-primary send" type="submit" style="width:90%; padding: 10px; font-weight: bold; border-radius: 3px;" id="report">ОТПРАВИТЬ</button>
			</div>
		</form>				
	</div>
</main>


<?php
	if (isset($req['Характеристика']) || isset($req['Описание'])){
		if (empty($req['Описание'])){ 
			?>
			<script>
				window.onload = function() {
					alert("Заполните описание");
				}
			</script>
			<?PHP
		}elseif (empty($req['Характеристика'])) {
			?>
			<script>
				window.onload = function() {
					alert("Заполните характеристику");
				}
			</script>
			<?PHP
		}
		if (isset($req['Характеристика']) && isset($req['Описание'])){
			?>
			<script>
				window.onload = function() {
					alert("Сообщение было отправлено");
				}
			</script>
			<?PHP
		}
	}
?>

@endsection