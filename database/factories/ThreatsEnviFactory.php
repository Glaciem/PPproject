<?php

use App\Threats_envi;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Threats_envi::class, function (Faker $faker) {

    return [
        'Аннотация' => $faker->sentence(10),
        'Источники' => $faker->sentence(3),
        'Способ_реализации' => $faker->text(300),
        'Используемые_уязвимости' => $faker->text(300),
        'Вид_информационных_ресурсов_потенциально_подверженных_угрозе' => $faker->text(300),
        'Нарушаемые_свойства_безопасности_информационных_ресурсов' => $faker->text(300),
        'Возможные_последствия_реализации' => $faker->text(300),
        'Рубрика' => $faker->sentence(10),
        'Мера' => $faker->sentence(10),
        'Среда' => $faker->randomElement(['ОС', 'МЭ']),
        'Среда_работы' => $faker->sentence(10),
    ];
});
