<footer id="footer" class="top-space">  <!--  fixed-bottom -->
		<div class="footer1"> 
			<div class="container">
				<div class="row">
					<div class="col-md-3 widget">
						<h3 class="widget-title">Контакты</h3>
						<div class="widget-body">
							<p>+12345678900<br>
								<a href="mailto:#">email@email.ru</a>
							</p>	
						</div>
					</div>
					<div class="col-md-3 widget">
						<h3 class="widget-title">Блог проекта</h3>
						<div class="widget-body">
							<p class="follow-me-icons">
								<a href="http://mysite.std-324.ist.mospolytech.ru/index.php" target="_blank"><img src="/img/blog.png"></a> <!--  height=50px> -->
							</p>	
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<div class="footer2">
			<div class="container">
				<div class="row">					
					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="simplenav">
								<a href="/">Главная</a> | 
								@guest
								<a href="http://project.std-322.ist.mospolytech.ru/index.php/signin">Войти</a>
								@else
								<a href="#">Наверх</a>
								@endguest								
							</p>
						</div>
					</div>					
				</div>
			</div>
		</div>
</footer>