@extends('Main.layout')

@section('content')
<?php 
require_once 'PHPWord/vendor/autoload.php';

//echo '<pre>', print_r($_COOKIE), '<pre>';
//echo '<pre>', print_r($type_opr), print_r($envi), '<pre>';

echo '<pre>';
echo print_r($_COOKIE);
echo '<pre>';


$threatsN = array();
foreach ($threats as $value) {
	foreach ($value as $value2) {
		$threatsN[] = $value2;
	}
}

$policyN = array();
foreach ($policy as $value) {
	foreach ($value as $value2) {
		$policyN[] = $value2;
	}
}


$ThreatsEnviN = array();
foreach ($ThreatsEnvi as $value) {
	foreach ($value as $value2) {
		$ThreatsEnviN[] = $value2;
	}
}


$HypothesisN = array();
foreach ($hypothesis as $value) {
	foreach ($value as $value2) {
		$HypothesisN[] = $value2;
	}
}

$AimsN = array();
foreach ($Aims as $value) {
	foreach ($value as $value2) {
		$AimsN[] = $value2;
	}
}

$AimsThreatsN = array();
foreach ($AimsThreats as $value) {
	foreach ($value as $value2) {
		$AimsThreatsN[] = $value2;
	}
}

$ComponentOfTrustN = array();
foreach ($ComponentOfTrust as $value) {
	foreach ($value as $value2) {
		$ComponentOfTrustN[] = $value2;
	}
}

if (isset($ComponentOfTrustADD)){
	$ComponentOfTrustADDN = array();
	foreach ($ComponentOfTrustADD as $value) {
		foreach ($value as $value2) {
			$ComponentOfTrustADDN[] = $value2;
		}
	}
}


$ComponentOfTrustFN = array();
foreach ($ComponentOfTrustF as $value) {
	foreach ($value as $value2) {
		$ComponentOfTrustFN[] = $value2;
	}
}

if (isset($ComponentOfTrustFADD)){
	$ComponentOfTrustFADDN = array();
	foreach ($ComponentOfTrustFADD as $value) {
		foreach ($value as $value2) {
			$ComponentOfTrustFADDN[] = $value2;
		}
	}
}

$ClassOfTrustFN = array();
foreach ($ClassOfTrustF as $value) {
	$ClassOfTrustFN[] = $value['Класс'];
}

$ClassOfTrustN = array();
foreach ($ClassOfTrust as $value) {
	$ClassOfTrustN[] = $value['Класс'];
}


$SubComponentOfTrustFN = array();
foreach ($SubComponentOfTrustF as $value) {
	foreach ($value as $value2) {
		$SubComponentOfTrustFN[] = $value2;
	}
}

$SubComponentOfTrustN = array();
foreach ($SubComponentOfTrust as $value) {
	foreach ($value as $value2) {
		$SubComponentOfTrustN[] = $value2;
	}
}


//echo '<pre>', print_r($ClassOfTrustFN), '<pre>';

$default1 = "текст из будущего";

$phpWord = new \PhpOffice\PhpWord\PhpWord();

$phpWord->setDefaultFontName('Times New Roman');
$phpWord->setDefaultFontSize(14);



$properties = $phpWord->getDocInfo();

$properties->setCreator('My name');
$properties->setCompany('My factory');
$properties->setTitle('My title');
$properties->setDescription('My description');
$properties->setCategory('My category');
$properties->setLastModifiedBy('My name');
$properties->setCreated(mktime(0, 0, 0, 3, 12, 2020));
$properties->setModified(mktime(0, 0, 0, 3, 14, 2020));
$properties->setSubject('My subject');
$properties->setKeywords('my, key, word');

$sectionStyle = array('pageNumberingStart' => 1);
$paragraphStyleCentr = array(
	'alignment' => 'center',
	'spaceAfter' => 0,
	'spaceBefore' => 0
);
$paragraphStyleBoth = array(
	'alignment' => 'both'
);
$fontStyleBold = array(
	'bold' => true
);
 
 $fontStyleItalicUnder = array(
	'italic' => true,
	'underline' => 'single'
);
$fontStyleItalic = array(
	'italic' => true
);

$section = $phpWord->createSection($sectionStyle);

$footer = $section->createFooter();
$footer->addPreserveText('{PAGE}', array(),
array(
    'align' => 'right',
));


// Титульник
$text = 'ФЕДЕРАЛЬНАЯ СЛУЖБА
ПО ТЕХНИЧЕСКОМУ И ЭКСПОРТНОМУ КОНТРОЛЮ
(ФСТЭК РОССИИ)';
$section->addText($text, array(), $paragraphStyleCentr);
$section->addTextBreak(13);
$text = 'МЕТОДИЧЕСКИЙ ДОКУМЕНТ';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleCentr);
$section->addTextBreak(1);
$text = 'ПРОФИЛЬ ЗАЩИТЫ';
$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);

if($FirstSelect['select1'] == 'ОС'){
	//$textrun = $section->createTextRun(array('alignment' => 'center', 'spaceAfter' => 0, 'spaceBefore' => 0)); //array('indent'=> 1)
	//$textrun->addText('ОПЕРАЦИОННЫХ СИСТЕМ ТИПА ', array('bold' => true));
	$text = 'ОПЕРАЦИОННЫХ СИСТЕМ ТИПА '.$FirstSelect['select2'];
	$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);
	$text = $FirstSelect['select3'].' КЛАССА ЗАЩИТЫ';
	$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);
}elseif($FirstSelect['select1'] == 'МЭ'){
	$text = 'МЕЖСЕТЕВЫХ ЭКРАНОВ ТИПА '.$FirstSelect['select2'];
	$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);
	$text = $FirstSelect['select3'].' КЛАССА ЗАЩИТЫ';
	$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);
}elseif($FirstSelect['select1'] == 'САЗ'){
	$text = 'СРЕДСТВ АНТИВИРУСНОЙ ЗАЩИТЫ '.$FirstSelect['select2'];
	$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);
	$text = $FirstSelect['select3'].' КЛАССА ЗАЩИТЫ';
	$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);
}elseif($FirstSelect['select1'] == 'СДЗ'){
	$text = 'СРЕДСТВА ДОВЕРЕННОЙ ЗАГРУЗКИ УРОВНЯ ПЛАТЫ РАСШИРЕНИЯ '.$FirstSelect['select2'];
	$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);
	$text = $FirstSelect['select3'].' КЛАССА ЗАЩИТЫ';
	$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);
}elseif($FirstSelect['select1'] == 'СКН'){
	$text = 'СРЕДСТВ КОНТРОЛЯ ПОДКЛЮЧЕНИЯ СЪЕМНЫХ МАШИННЫХ НОСИТЕЛЕЙ ИНФОРМАЦИИ '.$FirstSelect['select2'];
	$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);
	$text = $FirstSelect['select3'].' КЛАССА ЗАЩИТЫ';
	$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);
}elseif($FirstSelect['select1'] == 'СОВ'){
	$text = 'СИСТЕМ ОБНАРУЖЕНИЯ ВТОРЖЕНИЙ '.$FirstSelect['select2'];
	$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);
	$text = $FirstSelect['select3'].' КЛАССА ЗАЩИТЫ';
	$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);
}

$type = str_replace('"', '', $FirstSelect['select2']);

if ($FirstSelect['select3'] == 'ПЕРВОГО'){
	$classNumber = '1';
	$classSmall = 'Первый';
	$classSmallDEF = 'первого';
}elseif ($FirstSelect['select3'] == 'ВТОРОГО') {
	$classNumber = '2';
	$classSmall = 'Второй';
	$classSmallDEF = 'второго';
}elseif ($FirstSelect['select3'] == 'ТРЕТЬЕГО') {
	$classNumber = '3';
	$classSmall = 'Третий';
	$classSmallDEF = 'третьего';
}elseif ($FirstSelect['select3'] == 'ЧЕТВЕРТОГО') {
	$classNumber = '4';
	$classSmall = 'Четвертый';
	$classSmallDEF = 'четвертого';
}

$textrun = $section->createTextRun($paragraphStyleCentr);
$textrun->addText('ИТ.'.$FirstSelect['select1'].'.'.$type.$classNumber.'.'.'ПЗ', $fontStyleBold);


$section->addPageBreak();

// СОДЕРЖАНИЕ
$text = 'СОДЕРЖАНИЕ';
$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);
$section->addTextBreak(1);
$section->addTOC();
$section->addPageBreak();


// Сокращения
$phpWord->addTitleStyle(1, $fontStyleBold, $paragraphStyleCentr);
$text = "Перечень сокращений";
$section->addTitle($text, 1);
$section->addTextBreak(1);

foreach ($DefSocrs as $value) {
	$textrun = $section->createTextRun(array('indent'=> 1, 'alignment' => 'both'));
	$textrun->addText($value['Сокращение'], $fontStyleBold);
	$textrun->addText(' - '.$value['Расшифровка']);
}
foreach ($SelectedSocrs as $value) {
	$textrun = $section->createTextRun(array('indent'=> 1, 'alignment' => 'both'));
	$textrun->addText($value['Сокращение'], $fontStyleBold);
	$textrun->addText(' - '.$value['Расшифровка']);
}
$section->addPageBreak(1);


// ПУНКТ 1
$phpWord->addTitleStyle(1, $fontStyleBold, $paragraphStyleCentr);
$text = '1. Общие положения';
$section->addTitle($text, 1);
$section->addTextBreak(1);
$textrun = $section->createTextRun($paragraphStyleBoth);
$text = "Настоящий методический документ ФСТЭК России разработан и утвержден в соответствии с подпунктом 4 пункта 8 Положения о Федеральной службе по техническому и экспортному контролю, утвержденного Указом Президента Российской Федерации от 16 августа 2004 г. № 1085, и предназначен для организаций, осуществляющих в соответствии с законодательством Российской Федерации работы по созданию средств защиты информации (далее – разработчики, производители), заявителей на осуществление сертификации продукции (далее – заявители), а также для испытательных лабораторий и органов по сертификации, выполняющих работы по сертификации средств защиты информации на соответствие обязательным требованиям по безопасности информации при проведении ими работ";
$textrun->addText(htmlspecialchars($text));
$textrun->addText(htmlspecialchars($envi[0]['Общие_положения']));
$textrun->addText(htmlspecialchars(', утвержденным приказом '));
$textrun->addText(htmlspecialchars($envi[0]['Приказ_фстэк'].'.')); //ЗАВИСИМОСТЬ
//$section->addText(, array(), $paragraphStyleBoth);
$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText(htmlspecialchars('Настоящий методический документ ФСТЭК России детализирует и определяет взаимосвязи требований и функций безопасности '));
$textrun->addText(htmlspecialchars($FirstSelect['select1'].', '));
$textrun->addText(htmlspecialchars('установленных Требованиями безопасности информации к '));
$textrun->addText(htmlspecialchars($FirstSelect['select1'].', '));
$textrun->addText(htmlspecialchars('утвержденными приказом '));
$textrun->addText(htmlspecialchars($envi[0]['Приказ_фстэк'].'.'));//ЗАВИСИМОСТЬ
$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText(htmlspecialchars('Профиль защиты учитывает положения национальных стандартов Российской Федерации ГОСТ Р ИСО/МЭК 15408 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий».'));
$section->addPageBreak(1);

// ПУНКТ 2
$phpWord->addTitleStyle(1, $fontStyleBold, $paragraphStyleCentr);
$text = '2. Введение профиля защиты';
$section->addTitle($text, 1);
$section->addTextBreak(1);
$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText(htmlspecialchars('Данный раздел содержит информацию общего характера. Подраздел «Ссылка на профиль защиты» включает идентификационные сведения о профиле защиты (далее – ПЗ), которые предоставляют маркировку и описательную информацию, необходимую для контроля и идентификации ПЗ и объекта оценки (далее – ОО), к которому он относится. '));
// Тут может быть пункт, который есть в более старых версиях
$textrun->addText(htmlspecialchars('Подраздел «Аннотация профиля защиты» содержит общую характеристику ПЗ, позволяющую определить применимость ОО, к которому относится настоящий ПЗ, в конкретной ситуации. В подразделе «Соглашения» дается описание операций конкретизации компонентов требований безопасности '));
$textrun->addText(htmlspecialchars($FirstSelect['select1'].'. '));
$textrun->addText(htmlspecialchars('В подразделе «Ключевые термины, используемые в профиле защиты» представлены определения основных терминов, специфичных для данного ПЗ.'));
$section->addTextBreak(1);

//ПУНКТ 2.1. 
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = '2.1. Ссылка на профиль защиты';
$section->addTitle($text, 2);
$section->addTextBreak(1);


//Таблица
$styleTable = array('borderColor'=>'#000000', 'borderSize'=> 0, 'cellMargin'=>0, 'valign'=>'center', 'alignment'=>'center');
$styleFirstRow = array('bold'=>true, 'valign'=>'center');
$styleCell = array('valign'=>'top');
$styleFirsCell = array('valign'=>'top', 'bold'=>true);
$fontStyle = array('align'=>'center', 'color'=>'ccc');
$fontStyleBold = array('align'=>'center', 'color'=>'ccc', 'bold'=>true);
$table = $section->addTable($styleTable);
//$table->addRow(2);
//$table->addCell(3000, $styleCell)->addText('Зависимости: ', $fontStyle, array('spaceAfter' => 0));
//$table->addCell(13000, $styleCell)->addText($value2['Зависимость'], $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));

$table->addRow(2);
$table->addCell(5000, $styleFirsCell)->addText('Наименование ПЗ:', $fontStyleBold, array('spaceAfter' => 0, 'alignment' => 'both'));

if($FirstSelect['select1'] == 'ОС'){
	$text = 'Профиль защиты операционных систем типа '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты.';
	$table->addCell(13000, $styleCell)->addText($text, $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));
}elseif($FirstSelect['select1'] == 'МЭ'){
	$text = 'Профиль защиты межсетевых экранов типа '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты.';
	$table->addCell(13000, $styleCell)->addText($text, $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));
}elseif($FirstSelect['select1'] == 'САЗ'){
	$text = 'Профиль защиты средств антивирусной защиты '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты.';
	$table->addCell(13000, $styleCell)->addText($text, $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));
}elseif($FirstSelect['select1'] == 'СДЗ'){
	$text = 'Профиль защиты средства доверенной загрузки уровня платы расширения '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты.';
	$table->addCell(13000, $styleCell)->addText($text, $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));
}elseif($FirstSelect['select1'] == 'СКН'){
	$text = 'Профиль защиты средств контроля подключения съемных машинных носителей информации '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты.';
	$table->addCell(13000, $styleCell)->addText($text, $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));
}elseif($FirstSelect['select1'] == 'СОВ'){
	$text = 'Профиль защиты систем обнаружения вторжений '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты.';
	$table->addCell(13000, $styleCell)->addText($text, $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));
}

$table->addRow(2);
$table->addCell(5000, $styleFirsCell)->addText('Тип '.$FirstSelect['select1'].':', $fontStyleBold, array('spaceAfter' => 0, 'alignment' => 'both'));
$table->addCell(13000, $styleCell)->addText($FirstSelect['select1'].' типа '.$FirstSelect['select2'].'.', $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));

$table->addRow(2);
$table->addCell(5000, $styleFirsCell)->addText('Класс защиты:', $fontStyleBold, array('spaceAfter' => 0, 'alignment' => 'both'));
$table->addCell(13000, $styleCell)->addText($classSmall.'.', $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));

$table->addRow(2);
$table->addCell(5000, $styleFirsCell)->addText('Версия ПЗ:', $fontStyleBold, array('spaceAfter' => 0, 'alignment' => 'both'));
$table->addCell(13000, $styleCell)->addText('Версия 1.0.', $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));

$table->addRow(2);
$table->addCell(5000, $styleFirsCell)->addText('Обозначение ПЗ:', $fontStyleBold, array('spaceAfter' => 0, 'alignment' => 'both'));
$table->addCell(13000, $styleCell)->addText('ИТ.'.$FirstSelect['select1'].'.'.$type.$classNumber.'.'.'ПЗ'.'.', $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));

$table->addRow(2);
$table->addCell(5000, $styleFirsCell)->addText('Идентификация ОО:', $fontStyleBold, array('spaceAfter' => 0, 'alignment' => 'both'));
$table->addCell(13000, $styleCell)->addText($FirstSelect['select1'].' типа '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты.', $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));


$text = 'Оценочный уровень доверия '.$FirstSelect['select4'].' (ОУД'.$FirstSelect['select4'].')';

if (isset($ComponentOfTrustADDN)){
	$text .= ', ';
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == 0){
			$text .= 'усиленный компонентами ';
			//$textrun->addText(htmlspecialchars($text));
			break;
		}
	}
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == 0){
			$text .= $value['Компонент'].', ';
			//$textrun->addText(htmlspecialchars($text));
		}
	}
	
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == Auth::user()->id){
			$text .= 'расширенного компонентами ';
			//$textrun->addText(htmlspecialchars($text));
			break;
		}
	}
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == Auth::user()->id){
			$text .= $value['Компонент'].', ';
			//$textrun->addText(htmlspecialchars($text));
		}
	}
}


$table->addRow(2);
$table->addCell(5000, $styleFirsCell)->addText('Уровень доверия:', $fontStyleBold, array('spaceAfter' => 0, 'alignment' => 'both'));
$table->addCell(13000, $styleCell)->addText($text, $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));


$table->addRow(2);
$table->addCell(5000, $styleFirsCell)->addText('Идентификация:', $fontStyleBold, array('spaceAfter' => 0, 'alignment' => 'both'));
$table->addCell(13000, $styleCell)->addText('Требования ГОСТ Р ИСО/МЭК 15408 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий».', $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));

$table->addRow(2);
$table->addCell(5000, $styleFirsCell)->addText('Ключевые слова:', $fontStyleBold, array('spaceAfter' => 0, 'alignment' => 'both'));
$table->addCell(13000, $styleCell)->addText($envi[0]['Ключевые_слова'].'.', $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));



/*
$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Наименование ПЗ:  ', $fontStyleBold);
$textrun->addText('Профиль защиты ');
if($FirstSelect['select1'] == 'ОС'){
	//$textrun = $section->createTextRun(array('alignment' => 'center', 'spaceAfter' => 0, 'spaceBefore' => 0)); //array('indent'=> 1)
	//$textrun->addText('ОПЕРАЦИОННЫХ СИСТЕМ ТИПА ', array('bold' => true));
	$text = 'операционных систем типа '.$FirstSelect['select2'];
	$textrun->addText(htmlspecialchars($text.' '));
	$text = $classSmallDEF.' класса защиты';
	$textrun->addText(htmlspecialchars($text));
	//$text = 'Профиль защиты операционных систем типа '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты';
}elseif($FirstSelect['select1'] == 'МЭ'){
	$text = 'межсетевых экранов типа '.$FirstSelect['select2'];
	$textrun->addText(htmlspecialchars($text.' '));
	$text = $classSmallDEF.'  класса защиты';
	$textrun->addText(htmlspecialchars($text));
	//$text = 'Профиль защиты межсетевых экранов типа '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты';
}elseif($FirstSelect['select1'] == 'САЗ'){
	$text = 'средств антивирусной защиты '.$FirstSelect['select2'];
	$textrun->addText(htmlspecialchars($text.' '));
	$text = $classSmallDEF.'  класса защиты';
	$textrun->addText(htmlspecialchars($text));
	//$text = 'Профиль защиты средств антивирусной защиты '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты';
}elseif($FirstSelect['select1'] == 'СДЗ'){
	$text = 'средства доверенной загрузки уровня платы расширения '.$FirstSelect['select2'];
	$textrun->addText(htmlspecialchars($text.' '));
	$text = $classSmallDEF.'  класса защиты';
	$textrun->addText(htmlspecialchars($text));
	//$text = 'Профиль защиты средства доверенной загрузки уровня платы расширения '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты';
}elseif($FirstSelect['select1'] == 'СКН'){
	$text = 'средств контроля подключения съемных машинных носителей информации '.$FirstSelect['select2'];
	$textrun->addText(htmlspecialchars($text.' '));
	$text = $classSmallDEF.'  класса защиты';
	$textrun->addText(htmlspecialchars($text));
	//$text = 'Профиль защиты средств контроля подключения съемных машинных носителей информации '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты';
}elseif($FirstSelect['select1'] == 'СОВ'){
	$text = 'систем обнаружения вторжений '.$FirstSelect['select2'];
	$textrun->addText(htmlspecialchars($text.' '));
	$text = $classSmallDEF.'  класса защиты';
	$textrun->addText(htmlspecialchars($text));
	//$text = 'Профиль защиты систем обнаружения вторжений '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты';
}

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('ТИП ', $fontStyleBold);
$textrun->addText($FirstSelect['select1'].':  ', $fontStyleBold);
$textrun->addText($FirstSelect['select1'].' типа ');
$textrun->addText($FirstSelect['select2'].'.');

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Класс защиты:  ', $fontStyleBold);
$textrun->addText($classSmall.'.');

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Версия ПЗ:  ', $fontStyleBold);
$textrun->addText('Версия 1.0.');

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Обозначение ПЗ:  ', $fontStyleBold);
$textrun->addText('ИТ.'.$FirstSelect['select1'].'.'.$type.$classNumber.'.'.'ПЗ'.'.');

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Идентификация ОО:  ', $fontStyleBold);
$textrun->addText($FirstSelect['select1'].' типа '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты.');

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Уровень доверия:  ', $fontStyleBold);
$textrun->addText('Оценочный уровень доверия '.$FirstSelect['select4'].' (ОУД'.$FirstSelect['select4'].'), ');

if (isset($ComponentOfTrustADDN)){
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == 0){
			$text = 'усиленный компонентами ';
			$textrun->addText(htmlspecialchars($text));
			break;
		}
	}
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == 0){
			$text = $value['Компонент'].', ';
			$textrun->addText(htmlspecialchars($text));
		}
	}
	
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == Auth::user()->id){
			$text = 'расширенного компонентами ';
			$textrun->addText(htmlspecialchars($text));
			break;
		}
	}
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == Auth::user()->id){
			$text = $value['Компонент'].', ';
			$textrun->addText(htmlspecialchars($text));
		}
	}
}

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Идентификация:  ', $fontStyleBold);
$textrun->addText('Требования ГОСТ Р ИСО/МЭК 15408 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий».'); //зависимость

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Ключевые слова:  ', $fontStyleBold);
$textrun->addText($envi[0]['Ключевые_слова'].'.'); //зависимость
*/
$section->addTextBreak(1);

//ПУНКТ 2.2. Аннотация профиля защиты 
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "2.2. Аннотация профиля защиты";
$section->addTitle($text, 2);

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Настоящий ПЗ определяет требования безопасности к '.$FirstSelect['select1'].' (тип '.$FirstSelect['select1'].') '.$classSmallDEF.' класса защиты.');
//зависимость
$section->addTextBreak(1);

//2.2.1. Ключевые термины, используемые в профиле защиты
$phpWord->addTitleStyle(3, $fontStyleBold, $paragraphStyleCentr);
$text = "2.2.1. Ключевые термины, используемые в профиле защиты";
$section->addTitle($text, 3);

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Ниже приведены ключевые термины, используемые в ПЗ при задании требований безопасности ');
$textrun->addText($FirstSelect['select1']);
$textrun->addText(' и относящиеся к различным категориям пользователей ');
$textrun->addText($FirstSelect['select1']);
$textrun->addText(' и субъектов доступа, а также их определения.');
//зависимость

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Администратор безопасности: ', $fontStyleBold);
$textrun->addText('уполномоченный пользователь, ответственный за установку, администрирование и эксплуатацию ОО.');

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Задание по безопасности: ', $fontStyleBold);
$textrun->addText('совокупность требований безопасности и спецификаций, предназначенная для использования в качестве основы для оценки (сертификации) конкретного ОО.');

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Объект оценки: ', $fontStyleBold);
$textrun->addText('подлежащее сертификации (оценке) '.$FirstSelect['select1'].' с руководствами по эксплуатации.');

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Политика безопасности ОО: ', $fontStyleBold);
$textrun->addText('совокупность правил, регулирующих управление, защиту и распределение информационных ресурсов, контролируемых ОО.');

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Профиль защиты: ', $fontStyleBold);
$textrun->addText('совокупность требований безопасности для '.$FirstSelect['select1'].' типа '.$FirstSelect['select2'].' '.$classSmallDEF.' класса защиты.');

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Угроза безопасности информации: ', $fontStyleBold);
$textrun->addText('совокупность условий и факторов, определяющих потенциальную или реально существующую опасность нарушения безопасности информации.');

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Функции безопасности ОО: ', $fontStyleBold);
$textrun->addText('совокупность всех функций безопасности ОО, направленных на осуществление политики безопасности объекта оценки.');


$section->addTextBreak(1);

// ПУНКТ 2.2.2
$phpWord->addTitleStyle(3, $fontStyleBold, $paragraphStyleCentr);
$text = "2.2.2. Использование и основные характеристики безопасности объекта оценки";
$section->addTitle($text, 3);

$text = "ОО представляет собой:";
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
$text = $default1;
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
$text = "ОО должен обеспечивать нейтрализацию следующих угроз безопасности информации:";
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
$text = $default1;
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
$section->addTextBreak(1);


// ПУНКТ 2.2.3. Тип объекта оценки
$phpWord->addTitleStyle(3, $fontStyleBold, $paragraphStyleCentr);
$text = "2.2.3. Тип объекта оценки";
$section->addTitle($text, 3);

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('ОО является ');
$textrun->addText($FirstSelect['select1']);
$textrun->addText(' типа ');
$textrun->addText($FirstSelect['select2']);
$textrun->addText(' '.$classSmallDEF.' класса защиты. ');
$textrun->addText($FirstSelect['select1']);
$textrun->addText(' типа ');
$textrun->addText($FirstSelect['select2'].' - ');
$textrun->addText(htmlspecialchars($type_opr[0]['Определение'])); //зависимость
$section->addTextBreak(1);

// ПУНКТ 2.2.4.
$phpWord->addTitleStyle(3, $fontStyleBold, $paragraphStyleCentr);
$text = "2.2.4. Доступные аппаратные средства, программное обеспечение, программно-аппаратные средства, не входящие в объект оценки";
$section->addTitle($text, 3);
$text = "В рамках настоящего ПЗ аппаратные средства, программное обеспечение, программно-аппаратные средства, не входящие в ОО, не рассматриваются.";
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
$section->addTextBreak(1);

// ПУНКТ 2.3. Соглашения
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "2.3. Соглашения";
$section->addTitle($text, 2);

$text = 'Национальные стандарты Российской Федерации ГОСТ Р ИСО/МЭК 15408 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий» допускают выполнение определенных операций над компонентами требований безопасности. Соответственно в настоящем ПЗ используются операции «уточнение», «выбор», «назначение» и «итерация».';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Операция ');
$textrun->addText('«уточнение» ', $fontStyleBold);
$textrun->addText('используется для добавления в компонент требований некоторых подробностей (деталей) и, таким образом, ограничивает диапазон возможностей по удовлетворению требований. Результат операции ');
$textrun->addText('«уточнение» ', $fontStyleBold);
$textrun->addText('в настоящем ПЗ обозначается ');
$textrun->addText('полужирным текстом.', $fontStyleBold);

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Операция ');
$textrun->addText('«выбор» ', $fontStyleBold);
$textrun->addText('используется для выбора одного или нескольких элементов из перечня в формулировке компонента требований. Результат операции ');
$textrun->addText('«выбор» ', $fontStyleBold);
$textrun->addText('в настоящем ПЗ обозначается ');
$textrun->addText('подчеркнутым курсивным текстом.', $fontStyleItalicUnder);

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Операция ');
$textrun->addText('«назначение» ', $fontStyleBold);
$textrun->addText('используется для присвоения конкретного значения ранее неконкретизированному параметру в компоненте требований. Операция ');
$textrun->addText('«назначение» ', $fontStyleBold);
$textrun->addText('обозначается заключением присвоенного значения параметра в квадратные скобки, [назначаемое (присвоенное) значение параметра].');

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('В настоящем ПЗ используются компоненты требований безопасности, включающие частично выполненные операции ');
$textrun->addText('«назначение» ', $fontStyleBold);
$textrun->addText('и предполагающие завершение операций в задании по безопасности (ЗБ). В данных компонентах незавершенная часть операции ');
$textrun->addText('«назначение» ', $fontStyleBold);
$textrun->addText('обозначается как [назначение: ');
$textrun->addText('область предполагаемых значений', $fontStyleItalic);
$textrun->addText('].');

$text = 'В настоящий ПЗ включен ряд требований безопасности, сформулированных в явном виде (расширенные (специальные) требования безопасности). Краткая форма имен компонентов требований, сформулированных в явном виде, содержит текст (EXT).';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Операция ');
$textrun->addText('«итерация» ', $fontStyleBold);
$textrun->addText('используется для выражения двух или более требований безопасности на основе одного компонента требований безопасности; при этом осуществляется различное выполнение других операций («уточнение», «выбор» и (или) «назначение») над этим компонентом.');

$text = 'Настоящий ПЗ содержит ряд незавершенных операций над компонентами функциональных требований безопасности. Эти операции должны быть завершены в задании по безопасности для конкретной реализации ОС.';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

// ПУНКТ 3
$section->addPageBreak(1);
$phpWord->addTitleStyle(1, $fontStyleBold, $paragraphStyleCentr);
$text = "3. Утверждение о соответствии";
$section->addTitle($text, 1);

// ПУНКТ 3.1
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "3.1. Утверждение о соответствии ГОСТ Р ИСО/МЭК 15408";
$section->addTitle($text, 2);
$text = "Настоящий ПЗ разработан с учетом положений национальных стандартов Российской Федерации ГОСТ Р ИСО/МЭК 15408-2-2013 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий. Часть 2. Функциональные компоненты безопасности» и ГОСТ Р ИСО/МЭК 15408-3-2013 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий. Часть 3. Компоненты доверия к безопасности».";
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);


if (isset($ComponentOfTrustADDN) || isset($ComponentOfTrustFADDN)){
	$tumbler = false;
	if (isset($ComponentOfTrustADDN)){
		foreach ($ComponentOfTrustADDN as $value) {
			if($value['userid'] == Auth::user()->id){
				$tumbler = true;
				break;
			}
		}
	}
	if (isset($ComponentOfTrustFADDN)){
		foreach ($ComponentOfTrustFADDN as $value) {
			if($value['userid'] == Auth::user()->id){
				$tumbler = true;
				break;
			}
		}
	}
	if ($tumbler == true) {
		$textrun = $section->createTextRun($paragraphStyleBoth);
		$textrun->addText('Настоящий ПЗ содержит расширенные (специальные) требования безопасности, разработанные в соответствии с правилами, установленными национальными стандартами Российской Федерации ГОСТ Р ИСО/МЭК 15408 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий» (');
		$count = 0;
		if (isset($ComponentOfTrustADDN)){
			foreach ($ComponentOfTrustADDN as $value) {
				if($value['userid'] == Auth::user()->id){
					$count = $count + 1;
					if ($count == 1){
						$text = $value['Компонент'];
						$textrun->addText(htmlspecialchars($text));
					}else{
						$text = ', '.$value['Компонент'];
						$textrun->addText(htmlspecialchars($text));
					}
				}
			}
		}
		if (isset($ComponentOfTrustFADDN)){
			foreach ($ComponentOfTrustFADDN as $value) {
				if($value['userid'] == Auth::user()->id){
					$count = $count + 1;
					if ($count == 1){
						$text = $value['Компонент'];
						$textrun->addText(htmlspecialchars($text));
					}else{
						$text = ', '.$value['Компонент'];
						$textrun->addText(htmlspecialchars($text));
					}
				}
			}
		}
		$textrun->addText(')');
	}
}


// ПУНКТ 3.2
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "3.2. Утверждение о соответствии профилям защиты";
$section->addTitle($text, 2);
$text = 'Соответствие другим ПЗ не требуется.';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

// ПУНКТ 3.3
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "3.3. Утверждение о соответствии пакетам";
$section->addTitle($text, 2);

$textrun = $section->createTextRun($paragraphStyleBoth);
$textrun->addText('Заявлено о соответствии настоящего ПЗ следующему пакету: пакет требований доверия : оценочный уровень доверия '.$FirstSelect['select4'].' (ОУД'.$FirstSelect['select4'].')');


if (isset($ComponentOfTrustADDN)){
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == 0){
			$text = ', усиленный компонентами ';
			$textrun->addText(htmlspecialchars($text));
			break;
		}
	}
	$count = 0;
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == 0){
			$count = $count + 1;
			if ($count == 1){
				$text = $value['Компонент'];
				$textrun->addText(htmlspecialchars($text));
			}else{
				$text = ', '.$value['Компонент'];
				$textrun->addText(htmlspecialchars($text));
			}
		}
	}
	
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == Auth::user()->id){
			$text = ', расширенного компонентами ';
			$textrun->addText(htmlspecialchars($text));
			break;
		}
	}
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == Auth::user()->id){
			$count = $count + 1;
			if ($count == 1){
				$text = $value['Компонент'];
				$textrun->addText(htmlspecialchars($text));
			}else{
				$text = ', '.$value['Компонент'];
				$textrun->addText(htmlspecialchars($text));
			}
		}
	}
	$textrun->addText('.');
}else{
	$textrun->addText('.');
}

// ПУНКТ 3.4
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "3.4. Обоснование соответствия";
$section->addTitle($text, 2);

$textrun = $section->createTextRun($paragraphStyleBoth);
$text = 'Включение функциональных требований и требований доверия к '.$FirstSelect['select1'].' в настоящий ПЗ определяется Требованиями безопасности информации к '.$FirstSelect['select1'];
$textrun->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
$text = ', утвержденными приказом '.$envi[0]['Приказ_фстэк'].'.';
$textrun->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

// ПУНКТ 3.5
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "3.5. Изложение соответствия";
$section->addTitle($text, 2);
$text = 'При разработке ЗБ и (или) других ПЗ на основе настоящего профиля защиты устанавливаются следующие типы соответствия:';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
$text = '«строгое» соответствие – если настоящий ПЗ является единственным ПЗ, утверждение о соответствии которому включено в ЗБ;';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
$text = '«демонстрируемое» соответствие – если ОО является комплексным продуктом (изделием), и в ЗБ включено утверждение о соответствии настоящему ПЗ и другому (другим) ПЗ.';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

// ПУНКТ 4
$section->addPageBreak(1);
$phpWord->addTitleStyle(1, $fontStyleBold, $paragraphStyleCentr);
$text = "4. Определение проблемы безопасности";
$section->addTitle($text, 1);
$text = 'Данный раздел содержит описание следующих аспектов решаемой с использованием '.$FirstSelect['select1'].' проблемы безопасности:';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

$textrun = $section->createTextRun($paragraphStyleBoth);
$text = 'угроз безопасности, которым должны противостоять ОО и среда функционирования ОО;';
$textrun->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

$textrun = $section->createTextRun($paragraphStyleBoth);
$text = 'политик безопасности, которые должен выполнять ОО;';
$textrun->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

$textrun = $section->createTextRun($paragraphStyleBoth);
$text = 'предположений безопасности (обязательных условий безопасного использования ОО).';
$textrun->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

// ПУНКТ 4.1
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "4.1. Угрозы";
$section->addTitle($text, 2);

// ПУНКТ 4.1.1
$section->addTextBreak(1);
$phpWord->addTitleStyle(3, $fontStyleBold, $paragraphStyleCentr);
$text = "4.1.1. Угрозы, которым должен противостоять объект оценки";
$section->addTitle($text, 3);
$text = 'В настоящем ПЗ определены следующие угрозы, которым необходимо противостоять средствами ОО.';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

//Вывод угроз
$count = 0;
$index = 0;
$NewThreats = array();
//$textrun->addText('Слово');
foreach ($threatsN as $value) {
	$count++;
	$index++;
	$NewThreats[] = $value['id'];
	$text = 'Угроза-'.$index;
	$section->addText(htmlspecialchars($text), array('bold' => true));

	$listFormat = $phpWord->addNumberingStyle(
		'multilevel-'.$count,
		array('type' => 'multilevel', 'levels' => array(
			array('format' => 'decimal', 'text' => '%1.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720)
		)
	)
	);

	$section->addListItem('Аннотация угрозы - '.$value['Аннотация'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	$section->addListItem('Источники угрозы - '.$value['Источники'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	$section->addListItem('Способ реализации угрозы - '.$value['Способ_реализации'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	$section->addListItem('Используемые уязвимости - '.$value['Используемые_уязвимости'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	$section->addListItem('Вид информационных ресурсов, потенциально подверженных угрозе - '.$value['Вид_информационных_ресурсов_потенциально_подверженных_угрозе'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	$section->addListItem('Нарушаемые свойства безопасности информационных ресурсов - '.$value['Нарушаемые_свойства_безопасности_информационных_ресурсов'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	$section->addListItem('Возможные последствия реализации угрозы - '.$value['Возможные_последствия_реализации'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	//$section->addTextBreak(1);
}


// ПУНКТ 4.1.2
$section->addTextBreak(1);
$phpWord->addTitleStyle(3, $fontStyleBold, $paragraphStyleCentr);
$text = "4.1.2. Угрозы, которым противостоит среда";
$section->addTitle($text, 3);
$text = 'В настоящем ПЗ определены следующие угрозы, которым должна противостоять среда функционирования ОО:';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);


//Вывод угроз
$index = 0;
foreach ($ThreatsEnviN as $value) {
	$count++;
	$index++;
	
	$text = 'Угроза-'.$index;
	$section->addText(htmlspecialchars($text), array('bold' => true));

	$listFormat = $phpWord->addNumberingStyle(
		'multilevel-'.$count,
		array('type' => 'multilevel', 'levels' => array(
			array('format' => 'decimal', 'text' => '%1.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720)
		)
	)
	);

	$section->addListItem('Аннотация угрозы - '.$value['Аннотация'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	$section->addListItem('Источники угрозы - '.$value['Источники'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	$section->addListItem('Способ реализации угрозы - '.$value['Способ_реализации'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	$section->addListItem('Используемые уязвимости - '.$value['Используемые_уязвимости'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	$section->addListItem('Вид информационных ресурсов, потенциально подверженных угрозе - '.$value['Вид_информационных_ресурсов_потенциально_подверженных_угрозе'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	$section->addListItem('Нарушаемые свойства безопасности информационных ресурсов - '.$value['Нарушаемые_свойства_безопасности_информационных_ресурсов'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	$section->addListItem('Возможные последствия реализации угрозы - '.$value['Возможные_последствия_реализации'], 0, array(), 'multilevel-'.$count, $paragraphStyleBoth);
	//$section->addTextBreak(1);
}

// ПУНКТ 4.2
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "4.2. Политика безопасности";
$section->addTitle($text, 2);
$text = 'ОО должен выполнять приведенные ниже политики безопасности.';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

//Вывод политик
$index = 0;
$NewPolicy = array();
foreach ($policyN as $value) {
	$index++;
	$NewPolicy[] = $value['id'];
	$text = 'Политика безопасности-'.$index;
	$section->addText(htmlspecialchars($text), array('bold' => true));
	$text = $value['Политика'];
	$section->addText(htmlspecialchars($text), array(), array(
		'indent'=> 1, 'alignment' => 'both'
	));
	
	//$section->addTextBreak(1);
}

// ПУНКТ 4.3
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "4.3. Предположения безопасности";
$section->addTitle($text, 2);

//Вывод предположений
$index = 0;
foreach ($HypothesisN as $value) {
	$index++;
	$text = 'Предположение-'.$index;
	$section->addText(htmlspecialchars($text), array('bold' => true));
	$text = $value['Категория'];
	$section->addText(htmlspecialchars($text), array('bold' => true));
	$text = $value['Предположение'];
	$section->addText(htmlspecialchars($text), array(), array(
		'indent'=> 1, 'alignment' => 'both'
	));
	
	//$section->addTextBreak(1);
}



// ПУНКТ 5
$section->addPageBreak(1);
$phpWord->addTitleStyle(1, $fontStyleBold, $paragraphStyleCentr);
$text = "5. Цели безопасности";
$section->addTitle($text, 1);

// ПУНКТ 5.1
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "5.1. Цели безопасности для объекта оценки";
$section->addTitle($text , 2);
$text = 'В данном разделе дается описание целей безопасности для ОО.';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

// Вывод целей
$index = 0;
foreach ($AimsN as $value) {
	$index++;
	$text = 'Цель безопасности-'.$index;
	$section->addText(htmlspecialchars($text), array('bold' => true));
	$text = $value['Название'];
	$section->addText(htmlspecialchars($text), array('bold' => true, 'underline'=>'single'));
	$text = $value['Описание'];
	$section->addText(htmlspecialchars($text), array(), array(
		'indent'=> 1, 'alignment' => 'both'
	));
}

// ПУНКТ 5.2
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "5.2. Цели безопасности для среды функционирования";
$section->addTitle($text , 2);
$text = 'В данном разделе дается описание целей безопасности для среды функционирования ОО.';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

// Вывод целей
$index = 0;
foreach ($AimsThreatsN as $value) {
	$index++;
	$text = 'Цель безопасности для среды функционирования ОО-'.$index;
	$section->addText(htmlspecialchars($text), array('bold' => true));
	$text = $value['Название'];
	$section->addText(htmlspecialchars($text), array('bold' => true, 'underline'=>'single'));
	$text = $value['Описание'];
	$section->addText(htmlspecialchars($text), array(), array(
		'indent'=> 1, 'alignment' => 'both'
	));
}

// ПУНКТ 5.3
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "5.3. Обоснование целей безопасности";
$section->addTitle($text, 2);
$text = 'В таблице 5.1 приведено отображение целей безопасности для ОО на угрозы и политику безопасности.';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
$text = 'Таблица 5.1 – Отображение целей безопасности для ОО на угрозы и политику безопасности';
$section->addText(htmlspecialchars($text), $fontStyleBold, $paragraphStyleCentr);

//Таблица
$styleTable = array('borderColor'=>'#000000', 'borderSize'=> 5, 'cellMargin'=>0, 'valign'=>'center', 'alignment'=>'center');
$styleFirstRow = array('bgColor'=>'#C0C0C0', 'bold'=>true, 'valign'=>'center');
$styleCell = array('valign'=>'center');
$styleFirsCell = array('valign'=>'center' , 'bgColor'=>'#C0C0C0', 'bold'=>true);
$fontStyle = array('bold'=>false, 'valign'=>'center', 'color'=>'ccc');
$boldFontStyle = array('bold'=>true, 'valign'=>'center', 'color'=>'ccc');
//$phpWord->addTableStyle('myTable', $styleTable, $styleFirstRow);
$table = $section->addTable($styleTable);
$table->addRow(count($AimsN) + 1);
$table->addCell(3600, $styleFirsCell)->addText('Цели: ', $styleFirstRow, array('spaceAfter' => 0));
$count = 0;
foreach ($AimsN as $value) {
	$count++;
	$table->addCell(200, $styleFirsCell)->addText($count, $styleFirstRow, array('spaceAfter' => 0));
}
$count_threats = 0;

// угрозы: 17,118,119,120,121,122,123,124,125,126,127,128,187,188,1
// Угрозы целлейЖ 23,124,117,10,1|117,122,125,1|118,121,126,127,10,1|120,122,10,1|119,128,1|10,1|125,1|10,1|10,10,1|117,118,10,1|119,1|187,188,1|117,118,1|117,118,1|1
//echo '<pre>';
//echo '<pre>', print_r($threatsN), '<pre>';
/*
foreach ($threatsN as $value) {
	$count_aims = 0;
	foreach ($AimsN as $value2) {
		foreach ($threatsForAims[$count_aims] as $value3) {
			if ($value3['id_угроз'] == $value['id']){

			}
		}
		echo print_r('|');
		$count_aims++;
	}
}
*/
//echo '<pre>';
foreach ($threatsN as $value) {
	$count_threats++;
	$table->addRow(count($AimsN) + 1);
	$table->addCell(3600, $styleCell)->addText('Угроза - '.$count_threats, $fontStyle, array('spaceAfter' => 0));
	$count_aims = 0;
	foreach ($AimsN as $value2) {
		$plus = false;
		foreach ($threatsForAims[$count_aims] as $value3) {
			if ($value3['id_угроз'] == $value['id']){
				$plus = true;
			}
		}
		if ($plus == true){
			$table->addCell(200, $styleCell)->addText('Х', $boldFontStyle, array('spaceAfter' => 0));
		}else{
			$table->addCell(200, $styleCell)->addText(' ', $boldFontStyle, array('spaceAfter' => 0));
		}
		$count_aims++;
	}
}
$count_policys = 0;
foreach ($policyN as $value) {
	$count_policys++;
	$table->addRow(count($AimsN) + 1);
	$table->addCell(3600, $styleCell)->addText('Политика безопасности-'.$count_policys, $fontStyle, array('spaceAfter' => 0));
	$count_aims = 0;
	foreach ($AimsN as $value2) {
		$plus = false;
		foreach ($PolicyForAims[$count_aims] as $value3) {
			if ($value3['id_политик'] == $value['id']){
				$plus = true;
			}
		}
		if ($plus == true){
			$table->addCell(200, $styleCell)->addText('Х', $boldFontStyle, array('spaceAfter' => 0));
		}else{
			$table->addCell(200, $styleCell)->addText(' ', $boldFontStyle, array('spaceAfter' => 0));
		}
		$count_aims++;
	}
}
/*
foreach ($threatsForAims[$index-1] as $value2){
		$index2=0;
		foreach ($NewThreats as $value3){
			$index2++;
			if ($value2['id_угроз'] == $value3){
				$textrun->addText('Угроза-'.$index2.', ', $fontStyleBold);
			}
		}
	} */
/*
foreach ($ComponentOfTrustFN as $value){
	$table->addRow(2);
	$table->addCell(8000, $styleCell)->addText(stristr($value['Компонент'], ' ', true), $fontStyle, array('spaceAfter' => 0));
	$table->addCell(8000, $styleCell)->addText(stristr($value['Компонент'], ' '), $fontStyle, array('spaceAfter' => 0));
}*/
$section->addTextBreak(1);

//Вывод обоснования целей
$index = 0;
foreach ($AimsN as $value) {
	$index++;
	$text = 'Цель безопасности-'.$index;
	$section->addText(htmlspecialchars($text), array('bold' => true));
	$textrun = $section->createTextRun(array('indent'=> 1, 'alignment' => 'both'));
	$textrun->addText('Достижение этой цели безопасности необходимо для противостояния угрозам ');
	//ugrozi
	foreach ($threatsForAims[$index-1] as $value2){
		$index2=0;
		foreach ($NewThreats as $value3){
			$index2++;
			if ($value2['id_угроз'] == $value3){
				$textrun->addText('Угроза-'.$index2.', ', $fontStyleBold);
			}
		}
	}
	$textrun->addText('и реализации политик безопасности ');
	//politics
	foreach ($PolicyForAims[$index-1] as $value2){
		$index2=0;
		foreach ($NewPolicy as $value3){
			$index2++;
			if ($value2['id_политик'] == $value3){
				$textrun->addText('Политика-'.$index2.', ', $fontStyleBold);
			}
		}
	}
	$textrun->addText('так как обеспечивает: ');
	$textrun->addText($value['Обоснование']);
}


// ПУНКТ 6
$section->addPageBreak(1);
$phpWord->addTitleStyle(1, $fontStyleBold, $paragraphStyleCentr);
$text = "6. Определение расширенных компонентов";
$section->addTitle($text, 1);
if (isset($ComponentOfTrustADDN) || isset($ComponentOfTrustFADDN)){
	$textrun = $section->createTextRun($paragraphStyleBoth);
	$text = 'В данном разделе ПЗ представлены расширенные компоненты для '.$FirstSelect['select1'].'.';
	$textrun->addText(htmlspecialchars($text));
	
	// ПУНКТ 6.1
	$section->addTextBreak(1);
	$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
	$text = "6.1. Определение расширенных компонентов функциональных требований безопасности объекта оценки";
	$section->addTitle($text , 2);
	if (isset($ComponentOfTrustFADDN)){
		$text = 'Для ОО определены компоненты функциональных требований безопасности, сформулированные в явном виде в стиле компонентов из национального стандарта Российской Федерации ГОСТ Р ИСО/МЭК 15408-2-2013 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий. Часть 2. Функциональные компоненты безопасности» (расширенные (специальные) компоненты). Компоненты функциональных требований безопасности, сформулированные в явном виде, представлены в приложении А к настоящему профилю защиты.';
		$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
	}else{
		$text = 'Для ОО не определены расширенные компоненты функциональных требований безопасности.';
		$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
	}

	// ПУНКТ 6.2
	$section->addTextBreak(1);
	$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
	$text = "6.2. Определение расширенных (специальных) компонентов требований доверия к безопасности объекта оценки";
	$section->addTitle($text , 2);
	if (isset($ComponentOfTrustADDN)){
		$textrun = $section->createTextRun($paragraphStyleBoth);
		$text = 'Для ОО определены следующие расширенные (специальные) компоненты требований доверия к безопасности: ';
		$textrun->addText(htmlspecialchars($text));
		foreach ($ComponentOfTrustADDN as $value) {
			if($value['userid'] == Auth::user()->id){
				$text = $value['Компонент'].', ';
				$textrun->addText(htmlspecialchars($text));
			}
		}
		$text = 'сформулированные в явном виде в стиле компонентов из национального стандарта Российской Федерации ГОСТ Р ИСО/МЭК 15408-3-2013 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий. Часть 3. Компоненты доверия к безопасности». Компоненты требований доверия к безопасности, сформулированные в явном виде, представлены в приложении Б к настоящему профилю защиты.';
		$textrun->addText(htmlspecialchars($text));
	}else{
		$text = 'Для ОО не определены расширенные компоненты требований доверия к безопасности.';
		$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
	}
}else{
	$text = 'Для ОО не определены расширенные компоненты для '.$FirstSelect['select1'].'.';
	$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
}



// ПУНКТ 7
$section->addPageBreak(1);
$phpWord->addTitleStyle(1, $fontStyleBold, $paragraphStyleCentr);
$text = "7. Требования безопасности";
$section->addTitle($text, 1);
$textrun = $section->createTextRun($paragraphStyleBoth);
$text = 'В данном разделе ПЗ представлены функциональные требования и требования доверия, которым должен удовлетворять ОО.Функциональные требования, представленные в настоящем ПЗ, основаны на функциональных компонентах из национального стандарта Российской Федерации ГОСТ Р ИСО / МЭК 15408 - 2 - 2013 «Информационная технология.Методы и средства обеспечения безопасности.Критерии оценки безопасности информационных технологий.Часть 2. Функциональные компоненты безопасности».Кроме того, в настоящий ПЗ включен ряд требований безопасности, сформулированных в явном виде(расширение национального стандарта Российской Федерации ГОСТ Р ИСО / МЭК 15408 - 2 - 2013 «Информационная технология.Методы и средства обеспечения безопасности.Критерии оценки безопасности информационных технологий. Часть 2. Функциональные компоненты безопасности»).Требования доверия основаны на компонентах требований доверия из национального стандарта Российской Федерации ГОСТ Р ИСО / МЭК 15408 - 3 - 2013 «Информационная технология.Методы и средства обеспечения безопасности.Критерии оценки безопасности информационных технологий.Часть 3. Компоненты доверия к безопасности» и представлены в настоящем ПЗ в виде оценочного уровня доверия ОУД'.$FirstSelect['select4'].', '; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
$textrun->addText(htmlspecialchars($text));

if (isset($ComponentOfTrustADDN)){
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == 0){
			$text = 'усиленный компонентами ';
			$textrun->addText(htmlspecialchars($text));
			break;
		}
	}
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == 0){
			$text = $value['Компонент'].', ';
			$textrun->addText(htmlspecialchars($text));
		}
	}
	
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == Auth::user()->id){
			$text = 'расширенного компонентами ';
			$textrun->addText(htmlspecialchars($text));
			break;
		}
	}
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == Auth::user()->id){
			$text = $value['Компонент'].', ';
			$textrun->addText(htmlspecialchars($text));
		}
	}
}

$textrun->addText('сформулированы в явном виде (расширение национального стандарта Российской Федерации ГОСТ Р ИСО/МЭК 15408-3-2013 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий. Часть 3. Компоненты доверия к безопасности»).');

// ПУНКТ 7.1
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "7.1. Функциональные требования безопасности объекта оценки";
$section->addTitle($text, 2);
$text = 'Функциональные компоненты из национального стандарта Российской Федерации ГОСТ Р ИСО / МЭК 15408 - 2 - 2013 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий.Часть 2. Функциональные компоненты безопасности», на которых основаны функциональные требования безопасности ОО, а также компоненты сформулированных в явном виде расширенных(специальных) требований приведены в таблице 7.1.';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
$section->addTextBreak(1);
$text = 'Таблица 7.1 – Функциональные компоненты, на которых основаны ФТБ';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleCentr);

//Таблица
$styleTable = array('borderColor'=>'#000000', 'borderSize'=> 5, 'cellMargin'=>0, 'valign'=>'center', 'alignment'=>'center');
$styleFirstRow = array('bgColor'=>'#C0C0C0', 'bold'=>true, 'valign'=>'center');
$styleCell = array('valign'=>'center');
$styleFirsCell = array('valign'=>'center' , 'bgColor'=>'#C0C0C0', 'bold'=>true);
$fontStyle = array('bold'=>false, 'align'=>'center', 'color'=>'ccc');
//$phpWord->addTableStyle('myTable', $styleTable, $styleFirstRow);
$table = $section->addTable($styleTable);
$table->addRow(2);
$table->addCell(8000, $styleFirsCell)->addText('Идентификатор компонента требований', $styleFirstRow, array('spaceAfter' => 0));
$table->addCell(8000, $styleFirsCell)->addText('Название компонента требований', $styleFirstRow, array('spaceAfter' => 0));
foreach ($ComponentOfTrustFN as $value){
	$table->addRow(2);
	$table->addCell(8000, $styleCell)->addText(stristr($value['Компонент'], ' ', true), $fontStyle, array('spaceAfter' => 0));
	$table->addCell(8000, $styleCell)->addText(stristr($value['Компонент'], ' '), $fontStyle, array('spaceAfter' => 0));
}

// ПУНКТ 7.1.N
$IdClassBuff = array();
foreach ($ComponentOfTrustFN as $value){
	$IdClassBuff[] = $value['id_класса'];
}
$class = array_count_values ($IdClassBuff);
$id_class = array_values(array_unique($IdClassBuff));

//echo '<pre>', print_r($class),  print_r($id_class), print_r($ComponentOfTrustFN),  print_r($ClassOfTrustF), '<pre>';

$index = 0;
foreach ($class as $value){
	$index++;
	$section->addTextBreak(1);
	$phpWord->addTitleStyle(3, $fontStyleBold, $paragraphStyleCentr);
	$text = "7.1.".$index." ".stristr($ClassOfTrustFN[$id_class[$index-1]-1], ' ')." (".stristr($ClassOfTrustFN[$id_class[$index-1]-1], ' ', true).")";
	$section->addTitle($text, 2);
	foreach ($ComponentOfTrustFN as $value2) {
		if ($value2['id_класса'] == $id_class[$index-1]){
			//Таблица
			$styleTable = array('borderColor'=>'#000000', 'borderSize'=> 0, 'cellMargin'=>0, 'valign'=>'center', 'alignment'=>'center');
			$styleFirstRow = array('bold'=>true, 'valign'=>'center');
			$styleCell = array('valign'=>'top');
			$styleFirsCell = array('valign'=>'top', 'bold'=>true);
			$fontStyle = array('bold'=>false, 'align'=>'center', 'color'=>'ccc');
			$table = $section->addTable($styleTable);
			$table->addRow(2);
			$table->addCell(3000, $styleFirsCell)->addText(stristr($value2['Компонент'], ' ', true), $styleFirstRow, array('spaceAfter' => 0));
			$table->addCell(13000, $styleFirsCell)->addText(stristr($value2['Компонент'], ' '), $styleFirstRow, array('spaceAfter' => 0, 'alignment' => 'both'));
			$table->addRow(2);
			$table->addCell(3000, $styleCell)->addText('Зависимости: ', $fontStyle, array('spaceAfter' => 0));
			$table->addCell(13000, $styleCell)->addText($value2['Зависимость'], $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));
			foreach ($SubComponentOfTrustFN as $value3) {
				if ($value3['id_компонента'] == $value2['id']){
					$table->addRow(2);
					$table->addCell(3000, $styleCell)->addText($value3['Подкомпонент'], $fontStyle, array('spaceAfter' => 0));
					$table->addCell(13000, $styleCell)->addText($value3['Описание'], $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));
				}
			}
		}
	}
}


// ПУНКТ 7.2
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "7.2. Требования доверия к безопасности объекта оценки";
$section->addTitle($text, 2);
$textrun = $section->createTextRun($paragraphStyleBoth);
$text = 'Требования доверия к безопасности ОО взяты из национального стандарта Российской Федерации ГОСТ Р ИСО/МЭК 15408-3-2013 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий. Часть 3. Компоненты доверия к безопасности» и образуют ОУД'.$FirstSelect['select4'].', ';
$textrun->addText(htmlspecialchars($text));

if (isset($ComponentOfTrustADDN)){
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == 0){
			$text = 'усиленный компонентами ';
			$textrun->addText(htmlspecialchars($text));
			break;
		}
	}
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == 0){
			$text = $value['Компонент'].', ';
			$textrun->addText(htmlspecialchars($text));
		}
	}
	
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == Auth::user()->id){
			$text = 'расширенного компонентами ';
			$textrun->addText(htmlspecialchars($text));
			break;
		}
	}
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == Auth::user()->id){
			$text = $value['Компонент'].', ';
			$textrun->addText(htmlspecialchars($text));
		}
	}
}
$text = '(см. таблицу 7.4).';
$textrun->addText(htmlspecialchars($text));

$section->addTextBreak(1);
$text = 'Таблица 7.4 – Требования доверия к безопасности ОО';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleCentr);
//Таблица
$styleTable = array('borderColor'=>'#000000', 'borderSize'=> 5, 'cellMargin'=>0, 'valign'=>'center', 'alignment'=>'center');
$styleFirstRow = array('bgColor'=>'#C0C0C0', 'bold'=>true, 'valign'=>'center');
$styleCell = array('valign'=>'center');
$styleFirsCell = array('valign'=>'center' , 'bgColor'=>'#C0C0C0', 'bold'=>true);
$fontStyle = array('bold'=>false, 'align'=>'center', 'color'=>'ccc');
//$phpWord->addTableStyle('myTable', $styleTable, $styleFirstRow);
$table = $section->addTable($styleTable);
$table->addRow(2);
$table->addCell(3000, $styleFirsCell)->addText('Класс доверия', $styleFirstRow, array('spaceAfter' => 0));
$table->addCell(3000, $styleFirsCell)->addText('Идентификатор компонента требований', $styleFirstRow, array('spaceAfter' => 0));
$table->addCell(10000, $styleFirsCell)->addText('Название компонента требований', $styleFirstRow, array('spaceAfter' => 0));

$IdClassBuff = array();
foreach ($ComponentOfTrustN as $value){
	$IdClassBuff[] = $value['id_класса'];
}
$class = array_count_values ($IdClassBuff);
$id_class = array_values(array_unique($IdClassBuff));

$index = 0;
foreach ($class as $name) {
	foreach ($ComponentOfTrustN as $value){
		if($value['id_класса'] == $id_class[$index]){
			$table->addRow(2);
			$table->addCell(3000, $styleCell)->addText(stristr($ClassOfTrustN[$id_class[$index]-1], ' '), $fontStyle, array('spaceAfter' => 0));
			$table->addCell(3000, $styleCell)->addText(stristr($value['Компонент'], ' ', true), $fontStyle, array('spaceAfter' => 0));
			$table->addCell(10000, $styleCell)->addText(stristr($value['Компонент'], ' '), $fontStyle, array('spaceAfter' => 0));
		}
	}
	$index++;
}



// ПУНКТ 7.2.N
$IdClassBuff = array();
foreach ($ComponentOfTrustN as $value){
	$IdClassBuff[] = $value['id_класса'];
}
$class = array_count_values ($IdClassBuff);
$id_class = array_values(array_unique($IdClassBuff));

//echo '<pre>', print_r($class),  print_r($id_class), print_r($ComponentOfTrustFN),  print_r($ClassOfTrustF), '<pre>';

$index = 0;
foreach ($class as $value){
	$index++;
	$section->addTextBreak(1);
	$phpWord->addTitleStyle(3, $fontStyleBold, $paragraphStyleCentr);
	$text = "7.2.".$index." ".stristr($ClassOfTrustN[$id_class[$index-1]-1], ' ')." (".stristr($ClassOfTrustN[$id_class[$index-1]-1], ' ', true).")";
	$section->addTitle($text, 3);
	foreach ($ComponentOfTrustN as $value2) {
		if ($value2['id_класса'] == $id_class[$index-1]){
			//Таблица
			$styleTable = array('borderColor'=>'#000000', 'borderSize'=> 0, 'cellMargin'=>0, 'valign'=>'center', 'alignment'=>'center');
			$styleFirstRow = array('bold'=>true, 'valign'=>'center');
			$styleCell = array('valign'=>'top');
			$styleFirsCell = array('valign'=>'top', 'bold'=>true);
			$fontStyle = array('bold'=>false, 'align'=>'center', 'color'=>'ccc');
			$table = $section->addTable($styleTable);
			$table->addRow(2);
			$table->addCell(3000, $styleFirsCell)->addText(stristr($value2['Компонент'], ' ', true), $styleFirstRow, array('spaceAfter' => 0));
			$table->addCell(13000, $styleFirsCell)->addText(stristr($value2['Компонент'], ' '), $styleFirstRow, array('spaceAfter' => 0, 'alignment' => 'both'));
			$table->addRow(2);
			$table->addCell(3000, $styleCell)->addText('Зависимости: ', $fontStyle, array('spaceAfter' => 0));
			$table->addCell(13000, $styleCell)->addText($value2['Зависимость'], $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));
			foreach ($SubComponentOfTrustN as $value3) {
				if ($value3['id_компонента'] == $value2['id']){
					$table->addRow(2);
					$table->addCell(3000, $styleCell)->addText($value3['Подкомпонент'], $fontStyle, array('spaceAfter' => 0));
					$table->addCell(13000, $styleCell)->addText($value3['Описание'], $fontStyle, array('spaceAfter' => 0, 'alignment' => 'both'));
				}
			}
		}
	}
}

// ПУНКТ 7.3
$section->addTextBreak(1);
$phpWord->addTitleStyle(2, $fontStyleBold, $paragraphStyleCentr);
$text = "7.3. Обоснование требований безопасности";
$section->addTitle($text, 2);

// ПУНКТ 7.3.1
$phpWord->addTitleStyle(3, $fontStyleBold, $paragraphStyleCentr);
$text = "7.3.1. Обоснование требований безопасности для объекта оценки";
$section->addTitle($text, 3);

// ПУНКТ 7.3.1.1
$phpWord->addTitleStyle(4, $fontStyleBold, $paragraphStyleCentr);
$text = "7.3.1.1. Обоснование функциональных требований безопасности объекта оценки";
$section->addTitle($text, 4);

$text = "В таблице 7.5 представлено отображение функциональных требований безопасности на цели безопасности для ОО.";
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);

$section->addTextBreak(1);
$text = 'Таблица 7.5 – Отображение функциональных требований безопасности на цели безопасности';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleCentr);



//Таблица
$styleTable = array('borderColor'=>'#000000', 'borderSize'=> 5, 'cellMargin'=>0, 'valign'=>'center', 'alignment'=>'center');
$styleFirstRow = array('bgColor'=>'#C0C0C0', 'bold'=>true, 'valign'=>'center');
$styleCell = array('valign'=>'center');
$styleFirsCell = array('valign'=>'center', 'bgColor'=>'#C0C0C0', 'bold'=>true);
$fontStyle = array('bold'=>false, 'valign'=>'center', 'color'=>'ccc');
$boldFontStyle = array('bold'=>true, 'valign'=>'center', 'color'=>'ccc');
//$phpWord->addTableStyle('myTable', $styleTable, $styleFirstRow);
$table = $section->addTable($styleTable);
$table->addRow(count($AimsN) + 1);
$table->addCell(1600, $styleFirsCell)->addText('Цели: ', $styleFirstRow, array('spaceAfter' => 0));
$count = 0;
foreach ($AimsN as $value) {
	$count++;
	$table->addCell(200, $styleFirsCell)->addText($count, $styleFirstRow, array('spaceAfter' => 0));
}

foreach ($ComponentOfTrustFN as $value) {
	$comp = false;
	foreach ($FelemetsForAims as $valueOO) {
		foreach ($valueOO as $valueLOL) {
			if ($valueLOL['id_компонента'] == $value['id']){
				$comp = true;
			}
		}
	}
	if ($comp == true) {
		$table->addRow(count($AimsN) + 1);
		$table->addCell(1600, $styleCell)->addText(stristr($value['Компонент'], ' ', true), $fontStyle, array('spaceAfter' => 0));
		$count_aims = 0;
	
		foreach ($AimsN as $value2) {
			$plus = false;
			foreach ($FelemetsForAims[$count_aims] as $value3) {
				if ($value3['id_компонента'] == $value['id']){
					$plus = true;
				}
			}
			if ($plus == true){
				$table->addCell(200, $styleCell)->addText('Х', $boldFontStyle, array('spaceAfter' => 0));
			}else{
				$table->addCell(200, $styleCell)->addText(' ', $boldFontStyle, array('spaceAfter' => 0));
			}
			$count_aims++;
		}
	}
}

$section->addTextBreak(1);


$text = "Включение указанных в таблице 7.5 функциональных требований безопасности ОО в ПЗ определяется Требованиями безопасности информации к операционным системам, утвержденными приказом ФСТЭК России от 19 августа 2016 г. № 119.";
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
$section->addTextBreak(1);


//Вывод обоснования F компонентов
foreach ($ComponentOfTrustFN as $value) {
	$text = $value['Компонент'];
	$section->addText(htmlspecialchars($text), array('bold' => true));
	$textrun = $section->createTextRun(array('indent'=> 1, 'alignment' => 'both'));
	$textrun->addText('Обоснование. '); //ОБОСНОВАНИЕ
	$textrun->addText('Рассматриваемый компонент сопоставлен с целью (ями) ');
	$count_aims = 0;
	//echo '<pre>', print_r($FelemetsForAims), '<pre>';
	foreach ($AimsN as $value2) {
		$count_aims++;
		foreach ($FelemetsForAims[$count_aims-1] as $value3) {
			if ($value3['id_цели'] == $value2['id'] && $value3['id_компонента'] == $value['id']){
				$textrun->addText('Цель безопасности-'.$count_aims.', ', $fontStyleBold);
			}
		}
	}
	$textrun->addText('и способствует ее (их) достижению.');
}
$section->addTextBreak(1);


// ПУНКТ 7.3.1.2
$phpWord->addTitleStyle(4, $fontStyleBold, $paragraphStyleCentr);
$text = "7.3.1.2. Обоснование удовлетворения зависимостей функциональных требований безопасности";
$section->addTitle($text, 4);

$text = "В таблице 7.6 представлены результаты удовлетворения зависимостей функциональных требований безопасности. Все зависимости компонентов требований удовлетворены в настоящем профиле защиты либо включением компонентов, определенных в национальном стандарте Российской Федерации ГОСТ Р ИСО/МЭК 15408-2-2013 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий. Часть 2. Функциональные компоненты безопасности» под рубрикой «Зависимости», либо включением компонентов, иерархичных по отношению к компонентам, определенным в национальном стандарте Российской Федерации ГОСТ Р ИСО/МЭК 15408-2-2013 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий. Часть 2. Функциональные компоненты безопасности» под рубрикой «Зависимости».";
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
$text = "Столбец 2 таблицы 7.6 является справочным и содержит компоненты, определенные в национальном стандарте Российской Федерации ГОСТ Р ИСО/МЭК 15408-2-2013 «Информационная технология. Методы и средства обеспечения безопасности. Критерии оценки безопасности информационных технологий. Часть 2. Функциональные компоненты безопасности» в описании компонентов требований, приведенных в столбце 1 таблицы 7.6, под рубрикой «Зависимости».";
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
/*
$text = "Столбец 3 таблицы 7.6 показывает, какие компоненты требований были включены в настоящий ПЗ для удовлетворения зависимостей компонентов, приведенных в первом столбце таблицы 7.6. Компоненты требований в столбце 3 таблицы 7.6 либо совпадают с компонентами в столбце 2 таблицы 7.6, либо иерархичны по отношению к ним";
$section->addText(htmlspecialchars($text), array(), $paragraphStyleBoth);
*/
$section->addTextBreak(1);
$text = 'Таблица 7.6 – Зависимости функциональных требований безопасности';
$section->addText(htmlspecialchars($text), array(), $paragraphStyleCentr);



//Таблица
$styleTable = array('borderColor'=>'#000000', 'borderSize'=> 5, 'cellMargin'=>0, 'valign'=>'center', 'alignment'=>'center');
$styleFirstRow = array('bgColor'=>'#C0C0C0', 'bold'=>true, 'valign'=>'center');
$styleCell = array('valign'=>'center');
$styleFirsCell = array('valign'=>'center', 'bgColor'=>'#C0C0C0', 'bold'=>true);
$fontStyle = array('bold'=>false, 'valign'=>'center', 'color'=>'ccc');
$boldFontStyle = array('bold'=>true, 'valign'=>'center', 'color'=>'ccc');
//$phpWord->addTableStyle('myTable', $styleTable, $styleFirstRow);
$table = $section->addTable($styleTable);
$table->addRow(2);
$table->addCell(2000, $styleFirsCell)->addText('Функциональные компоненты', $styleFirstRow, array('spaceAfter' => 0));
$table->addCell(7000, $styleFirsCell)->addText('Зависимости в соответствии с ГОСТ Р ИСО/МЭК 15408 и подразделом 7.1 настоящего ПЗ', $styleFirstRow, array('spaceAfter' => 0));

foreach ($ComponentOfTrustFN as $value) {
	$table->addRow(2);
	$zav = str_replace("\n", " ", $value['Зависимость']);
	$table->addCell(2000, $styleCell)->addText(stristr($value['Компонент'], ' ', true), $fontStyle, array('spaceAfter' => 0));
	$table->addCell(7000, $styleCell)->addText(stristr($zav, ' '), $fontStyle, array('spaceAfter' => 0));
}
/*
foreach ($AimsN as $value) {
	$count++;
	$table->addCell(200, $styleFirsCell)->addText($count, $styleFirstRow, array('spaceAfter' => 0));
}

foreach ($ComponentOfTrustFN as $value) {
	$comp = false;
	foreach ($FelemetsForAims as $valueOO) {
		foreach ($valueOO as $valueLOL) {
			if ($valueLOL['id_компонента'] == $value['id']){
				$comp = true;
			}
		}
	}
	if ($comp == true) {
		$table->addRow(count($AimsN) + 1);
		$table->addCell(1600, $styleCell)->addText(stristr($value['Компонент'], ' ', true), $fontStyle, array('spaceAfter' => 0));
		$count_aims = 0;
	
		foreach ($AimsN as $value2) {
			$plus = false;
			foreach ($FelemetsForAims[$count_aims] as $value3) {
				if ($value3['id_компонента'] == $value['id']){
					$plus = true;
				}
			}
			if ($plus == true){
				$table->addCell(200, $styleCell)->addText('Х', $boldFontStyle, array('spaceAfter' => 0));
			}else{
				$table->addCell(200, $styleCell)->addText(' ', $boldFontStyle, array('spaceAfter' => 0));
			}
			$count_aims++;
		}
	}
}
*/
$section->addTextBreak(1);



// ПУНКТ 7.3.2
$phpWord->addTitleStyle(3, $fontStyleBold, $paragraphStyleCentr);
$text = "7.3.2. Обоснование требований доверия к безопасности объекта оценки";
$section->addTitle($text, 3);

$textrun = $section->createTextRun($paragraphStyleBoth);
$text = 'Требования доверия настоящего ПЗ соответствуют ОУД'.$FirstSelect['select4'].', ';
$textrun->addText(htmlspecialchars($text));

if (isset($ComponentOfTrustADDN)){
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == 0){
			$text = 'усиленный компонентами ';
			$textrun->addText(htmlspecialchars($text));
			break;
		}
	}
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == 0){
			$text = $value['Компонент'].', ';
			$textrun->addText(htmlspecialchars($text));
		}
	}
	
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == Auth::user()->id){
			$text = 'расширенного компонентами ';
			$textrun->addText(htmlspecialchars($text));
			break;
		}
	}
	foreach ($ComponentOfTrustADDN as $value) {
		if($value['userid'] == Auth::user()->id){
			$text = $value['Компонент'].', ';
			$textrun->addText(htmlspecialchars($text));
		}
	}
}

$textrun->addText('Включение указанных требований доверия к безопасности ОО в ПЗ определяется Требованиями безопасности информации к операционным системам, утвержденными приказом ФСТЭК России от 19 августа 2016 г. № 119.');

//$section->addPageBreak(1);

//РАБОЧИЙ КОД
//$phpWord->getSettings()->setUpdateFields(true);

$file = Auth::user()->name.'.docx';
header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="' . $file . '"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');
$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$xmlWriter->save("php://output");



//$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
//$objWriter->save(Auth::user()->id.'.doc');



/*
ob_start();
header('Refresh: 0; URL=start');
ob_end_flush(); */

/*
$file = 'doc.doc';
header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="' . $file . '"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');
$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$xmlWriter->save("php://output");
*/

?>

@endsection

<!--
	<a href="{{ route('downloadfile', 'test.txt') }}" class="btn btn-primary" >ЗАГРУЗКАFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF</a> -->

