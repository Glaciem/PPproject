@extends('Main.layout')

@section('content')

<?PHP //echo '<pre>', print_r($cell), '<pre>'; ?>

<!--
	{{print_r($req)}}

	{{print_r(array_keys($req))}}
-->
<?PHP 
//echo print_r($req);


if(isset($req[0])){
	//echo $req[1];	
	echo '<meta http-equiv="refresh" content="0; http://project.std-322.ist.mospolytech.ru/index.php/'.$req[1].'">';
}

$array = array();?>
@foreach (array_keys($req) as $name)
@if ($name != '_token')
<?php $array[] = $name ?>
@endif				
@endforeach
<?php
if(!isset($req['Аннотация'])){
	$json = json_encode($array);
	setcookie('Threats', $json);
}
//	echo '<pre>', print_r($_COOKIE), '<pre>';
/*
	$array2 = json_decode($_COOKIE['SelectThreats'], true); //возвращаем массиву рабочее состояние
	echo "<pre>";
	print_r($array2); // выводим массив для  проверки
	echo "</pre>"; 

*/
	?>
	<main role="main" class="col">
		<div class="jumbotron shadow">
			<h2 class="headers_text">Этап 3. Выбор угроз среды</h2>
			@if(empty($threats[0]['Аннотация']))
			<h4>Угрозы среды не найдены!</h4>
			@else
			<?php $s=0; $v=0;?>
			@for($i = 0; $i < 2; $i++) 
			<form method="POST" action= "step4">
				{{ csrf_field() }}
				<div class="accordion" id="accordionExample">
					@foreach ($threats as $name)
					@if(($i == 0 && $name->userid == 0) || ($i == 1 && $name->userid == Auth::user()->id))
					@if($s == 0 && $i == 0 && $name->userid == 0)
					<?php $s = 1; ?>
					<h5>Стандартные угрозы среды:</h5>
					@endif
					@if($s == 0 && $i == 1)
					<?php $s = 1; ?>
					<h4>Стандартные угрозы среды не найдены!</h4>
					@endif
					@if($v == 0 && $i == 1 && $name->userid == Auth::user()->id)
					<?php $v = 1; ?>
					<br><h5>Ваши угрозы среды:</h5>
					@endif
					<div class="card">
						<div class="card-header row" id="heading{{$name->id}}">
							<div class="col-sm">
								<input class="bigcheck" name="{{$name->id}}" type="checkbox" id="{{$name->id}}" onclick="checkArrowNextButton();"/>
							</div>
							<div class="col-sm-11">
								<h5 class="mb-0">
									<button class="btn btn-link accordtext" type="button" data-toggle="collapse" data-target="#collapse{{$name->id}}" aria-expanded="true" aria-controls="collapse{{$name->id}}">
										{{$name->Аннотация}}
									</button>
								</h5>
							</div>
						</div>
						<div id="collapse{{$name->id}}" class="collapse" aria-labelledby="heading{{$name->id}}" data-parent="#accordionExample">
							<div class="card-body">
								<b>Аннотация:</b> {{$name->Аннотация}} <br>
								<b>Источники:</b> {{$name->Источники}} <br>
								<b>Способ реализации:</b> {{$name->Способ_реализации}} <br>
								<b>Используемые уязвимости:</b> {{$name->Используемые_уязвимости}} <br>
								<b>Вид информационных ресурсов потенциально подверженных угрозе:</b> {{$name->Вид_информационных_ресурсов_потенциально_подверженных_угрозе}} <br>
								<b>Нарушаемые свойства безопасности информационных ресурсов:</b> {{$name->Нарушаемые_свойства_безопасности_информационных_ресурсов}} <br>
								<b>Возможные последствия реализации:</b> {{$name->Возможные_последствия_реализации}} <br>
								<b>Рубрика:</b> {{$name->Рубрика}} <br>
								<b>Мера:</b> {{$name->Мера}} <br>
								<b>Среда:</b> {{$name->Среда}} <br>
								<b>Тип:</b> {{$name->Тип}}								
							</div>
						</div>
					</div>
					@endif
					@endforeach
				</div>
				@endfor
				@endif
			</div>
		</main>

		@endsection
