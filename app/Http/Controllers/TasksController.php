<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class TasksController extends Controller
{
    public function index(){
		$tasks = App\Task::incomplete();
		$name = 'Alex';
		$test = [
			'test1',
			'test2',
			'test3'
		];
		return view('tasks.index', compact('test','name','tasks'));
	}
	
	public function show($id){
		$task = App\Task::find($id);
		return view('tasks.show', compact('task'));
	}
}
