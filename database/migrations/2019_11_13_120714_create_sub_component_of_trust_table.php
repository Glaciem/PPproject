<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubComponentOfTrustTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_component_of_trust', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_компонента');
            $table->text('Подкомпонент');
            $table->text('Элемент');
            $table->text('Описание');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_component_of_trust');
    }
}
