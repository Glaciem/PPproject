<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aims_threats extends Model
{
    protected $table = 'aims_threats';

    public static function uses_aims($true_aims, $arrayThreats, $arrayPolicy){
        $uses_aims = array();
        foreach ($true_aims as $true_aims_id) {
            $id_ugroz = static::where('id_цели', $true_aims_id['id'])->get('id_угроз');
            $id_politic = static::where('id_цели', $true_aims_id['id'])->get('id_политик'); 
            $ok_u = true;
            $ok_p = true;
            foreach ($id_ugroz as $id_2) {
                if ($id_2['id_угроз'] == 0){}
                else {
                    if (in_array($id_2['id_угроз'], $arrayThreats)) {}
                        else {
                            $ok_u = false;
                            break;
                    }
                }
            }          
            foreach ($id_politic as $id_2) {
                    if ($id_2['id_политик'] == 0){}
                    else {
                        if (in_array($id_2['id_политик'], $arrayPolicy)) {}
                        else {
                            $ok_p = false;
                            break;
                        }
                    }
                }
            if ($ok_u == true && $ok_p == true)
                $uses_aims[] = $true_aims_id;
        }
        return $uses_aims;
    }

    public static function un_uses_threats($uses_aims, $arrayThreats){
        $un_uses_threats = array();
        $id_ugroz = array();
        $id_ugroz2 = array();
        foreach ($uses_aims as $uses_aims_id) {
            $id_ugroz[] = static::where('id_цели', $uses_aims_id['id'])->get('id_угроз');
        }
        foreach ($id_ugroz as $id) {
            foreach ($id as $id2) {
               $id_ugroz2[] = $id2['id_угроз'];
            }
        }
        foreach ($arrayThreats as $id_2) {
            if (in_array($id_2, $id_ugroz2)){}
            else
                $un_uses_threats[] = $id_2;
        }    
        return $un_uses_threats;
    }

    public static function un_uses_policy($uses_aims, $arrayPolicy){
        $un_uses_policy = array();
        $id_politic = array();
        $id_politic2 = array();
        foreach ($uses_aims as $uses_aims_id) {
            $id_politic[] = static::where('id_цели', $uses_aims_id['id'])->get('id_политик');
        }
        foreach ($id_politic as $id) {
            foreach ($id as $id2) {
               $id_politic2[] = $id2['id_политик'];
            }
        }
        foreach ($arrayPolicy as $id_2) {
            if (in_array($id_2, $id_politic2)){}
            else
                $un_uses_policy[] = $id_2;
        }    
        return $un_uses_policy;        
    }

    public static function other_threats($un_uses_threats, $arrayThreats){
        if(!empty($un_uses_threats)){
            $other_threats = array();
            foreach ($arrayThreats as $id) {
                if (in_array($id, $un_uses_threats)){}
                else
                    $other_threats[] = $id;
            }
        }
        else
            $other_threats = $arrayThreats;
        return $other_threats;
    }

    public static function other_policy($un_uses_policy, $arrayPolicy){
        if(!empty($un_uses_policy)){
            $other_policy = array();
            foreach ($arrayPolicy as $id) {
                if (in_array($id, $un_uses_policy)){}
                else
                    $other_policy[] = $id;
            }
        }
        else
            $other_policy = $arrayPolicy;
        return $other_policy;
    }

    public static function add_aim($id_last_added_aim, $array){
        $id_ugroz = array();
        $id_politic = array();
        $u = 0;
        $p = 0;
        if(!empty($array['ugroza'])){
            foreach ($array['ugroza'] as $name){
                $id_ugroz[] = $name;
                $u++;
            }
        }
        if(!empty($array['policy'])){
            foreach ($array['policy'] as $name){
                $id_politic[] = $name;
                $p++;
            }
        }
        if ($u > $p){ //если угроз > политик
            for ($i = 0; $i < $u; $i++){
                if($p > 0){
                    $s = static::insert(['id_цели' => $id_last_added_aim[0]['id'], 
                        'id_угроз' => $id_ugroz[$i],
                        'id_политик' => $id_politic[$i]]);
                }
                else {
                    $s = static::insert(['id_цели' => $id_last_added_aim[0]['id'], 
                        'id_угроз' => $id_ugroz[$i],
                        'id_политик' => 0]);
                }
                $p--;
            }            
        }
        else 
            if($u < $p){ //если угроз < политик
                for ($i = 0; $i < $p; $i++){
                    if($u > 0){
                        $s = static::insert(['id_цели' => $id_last_added_aim[0]['id'], 
                            'id_угроз' => $id_ugroz[$i],
                            'id_политик' => $id_politic[$i]]);
                    }
                    else {
                        $s = static::insert(['id_цели' => $id_last_added_aim[0]['id'], 
                            'id_угроз' => 0,
                            'id_политик' => $id_politic[$i]]);
                    }
                    $u--;
                }
            }
            else { //если равное количество   
               for ($i = 0; $i < $p; $i++){
                    $s = static::insert(['id_цели' => $id_last_added_aim[0]['id'], 
                        'id_угроз' => $id_ugroz[$i],
                        'id_политик' => $id_politic[$i]]);
                }      
            }
            return "wowitswork";
    }

     public static function id_ugr($id_aims){
        $id_ugr = array();
        foreach ($id_aims as $value) {
            $id_ugr[] = static::where('id_цели', $value)->get('id_угроз');
        }
        return $id_ugr;
    }
    public static function id_polit($id_aims){
        $id_polit = array();
        foreach ($id_aims as $value) {
            $id_polit[] = static::where('id_цели', $value)->get('id_политик');
        }
        return $id_polit;
    }

    public static function del($id){
        $id = mb_substr($id, 0, -1);
        static::where('id_цели', $id)->delete();
        return "Great";
    }
}