@extends('Main.layout')

@section('content')


<?PHP $array = array();

if(isset($req[0])){
	//echo $req[1];	
	echo '<meta http-equiv="refresh" content="0; http://project.std-322.ist.mospolytech.ru/index.php/'.$req[1].'">';
}
?>



@foreach (array_keys($req) as $name)
	@if ($name != '_token')
		<?php $array[] = $name ?>
	@endif				
@endforeach


<?php
if(!isset($req['Предположение'])){
	$json = json_encode($array);
	setcookie('Policy', $json);
}

?>

	<main role="main" class="col">
		<div class="jumbotron shadow">
			<h2 class="headers_text">Этап 5. Выбор предположений</h2>
			@if(empty($tabs[0]['Предположение']))
			<h4>Предположения не найдены!</h4>
			@else
			<?php $s=0; $v=0;?>
			@for($i = 0; $i < 2; $i++) 
			<form method="POST" action= "step6">
				{{ csrf_field() }}
				<div class="accordion" id="accordionExample">
					@foreach ($tabs as $name)
					@if(($i == 0 && $name->userid == 0) || ($i == 1 && $name->userid == Auth::user()->id))
					@if($s == 0 && $i == 0 && $name->userid == 0)
					<?php $s = 1; ?>
					<h5>Стандартные предположения:</h5>
					@endif
					@if($s == 0 && $i == 1)
					<?php $s = 1; ?>
					<h4>Стандартные предположения не найдены!</h4>
					@endif
					@if($v == 0 && $i == 1 && $name->userid == Auth::user()->id)
					<?php $v = 1; ?>
					<br><h5>Ваши предположения:</h5>
					@endif
					<div class="card">
						<div class="card-header row" id="heading{{$name->id}}">
						<div class="col-sm">
							<input class="bigcheck" name="{{$name->id}}" type="checkbox" id="{{$name->id}}" onclick="checkArrowNextButton();"/>
						</div>
						<div class="col-sm-11">
							<h5 class="mb-0">
								<button class="btn btn-link accordtext" type="button" data-toggle="collapse" data-target="#collapse{{$name->id}}" aria-expanded="true" aria-controls="collapse{{$name->id}}">
									{{$name->Предположение}}
								</button>
							</h5>
						</div>
						</div>
						<div id="collapse{{$name->id}}" class="collapse" aria-labelledby="heading{{$name->id}}" data-parent="#accordionExample">
							<div class="card-body">
								<b>Предположение:</b> {{$name->Предположение}} <br>								
								<b>Категория:</b> {{$name->Категория}} <br>
								<b>Рубрика:</b> {{$name->Рубрика}} <br>
								<b>Мера:</b> {{$name->Мера}} <br>
								<b>Тип:</b> {{$name->Тип}} <br>
								<b>Среда:</b> {{$name->Среда}} <br>								
							</div>
						</div>
					</div>
					@endif
					@endforeach
				</div>
				@endfor
				@endif			
		</div>
	</main>


@endsection
