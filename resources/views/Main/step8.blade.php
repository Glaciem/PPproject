@extends('Main.layout')

@section('content')

<main role="main" class="col">
	<div class="jumbotron shadow">
		<h2 class="headers_text">Этап 8. Выбор компонентов доверия</h2>


		<?PHP $mass = array();
		if(isset($req[0])){
	//echo $req[1];	
	echo '<meta http-equiv="refresh" content="0; http://project.std-322.ist.mospolytech.ru/index.php/'.$req[1].'">';
}
		?>

@foreach (array_keys($req) as $name)
@if ($name != '_token')
<?php $mass[] = $name ?>
@endif				
@endforeach

		<?PHP //echo '<pre>', print_r($req), '<pre>';

$json = json_encode($mass);
if($json != '["newcomponent","add"]'){
	setcookie('AimsThreats', $json);
}
		$array = array();
		$array2 = array();
		$newcomponent = array();
		$comparr = array();
		$comparr = array();
		$count3 = 0;
		/*
		if(isset($zavisimost))
			header("Refresh:0");
*/

		foreach ($component as $name){
			foreach ($name as $value) {
				$newcomponent[] = $value;
			}
		}

		foreach ($newcomponent as $name){
			$array[] = $name['id_класса'];
		}

		foreach ($newcomponent as $value) {
			$comparr[] = array("id" => $value['id'], 
				"userid" => $value['userid'],
				"id_класса" => $value['id_класса'],
				"Компонент" => $value['Компонент'],
				"Зависимость" => $value['Зависимость']
			);
		}

		if (isset($pointed)){
			$pointed2 = array();
			$pointed_array = array();

			foreach ($pointed as $name){
				foreach ($name as $value) {
					$pointed2[] = $value;
				}
			}

			foreach ($pointed2 as $name){
				$pointed_array[] = $name['id_класса'];
			}

			if (isset($req['newcomponent'])){
				foreach ($pointed2 as $name){
					$comparr[] = array("id" => $name['id'], 
						"userid" => $name['userid'],
						"id_класса" => $name['id_класса'],
						"Компонент" => $name['Компонент'],
						"Зависимость" => $name['Зависимость']
					);
				}
			}


			$pointed_class = array_count_values ($pointed_array);

			$pointed_id_class = array_values(array_unique($pointed_array));
		}
/*
		foreach ($comparr as $name){
				echo $name['id'], '\n';
		}

 echo '<pre>', print_r($comparr);
 echo '<pre>', print_r($newcomponent), '<pre>';
*/
 foreach ($comparr as $name){
			$arrayBLA[] = $name['id_класса'];
		}


		$class = array_count_values ($array);

		$id_class = array_values(array_unique($array));



		/*
				echo '<pre>', print_r($class);
				echo print_r($id_class), '<pre>';


		echo '<pre>', print_r($comparr), '<pre>';*/


// echo '<pre>', print_r($newcomponent), '<pre>';

//		echo '<pre>', print_r($pointed), '<pre>';


		?>

		<form method="POST" action= "step9">
			{{ csrf_field() }}

			<table class="table table-hover table-bordered" style="margin-bottom: 0rem;">
				<thead>
					<tr>
						<th scope="col" style="width:400px">Класс доверия</th>
						<th scope="col">Компонент доверия</th>
						<th scope="col" style="width:30px"> </th>
					</tr>
				</thead>
				<tbody>
					@foreach ($class as $index)
					<?PHP 
					$count2 = 0;
					?>
					@foreach ($newcomponent as $name)
					@if ($name['id_класса'] == $id_class[$count3])
					<?PHP $count2++; ?>
					@if ($count2 == 1)
					<tr>
						<td rowspan="{{$index}}" style="width:400px"><?PHP echo $classtab[$name['id_класса']-1]['Класс']; ?></td>
						<td>{{$name->Компонент}}</td>
						<td style="width:30px"><input name="component[]" value="{{$name->id}}" id="{{$name->id}}" type="checkbox" checked="checked" style="transform:scale(2.0);" onclick="checkArrowNextButton();"/></td>
					</tr>
					@else
					<tr>
						<td>{{$name->Компонент}}</td>
						<td style="width:30px"><input name="component[]" value="{{$name->id}}" id="{{$name->id}}" type="checkbox" checked="checked" style="transform:scale(2.0);" onclick="checkArrowNextButton();"/></td>
					</tr>
					@endif
					@endif
					@endforeach
					<?PHP $count3++; ?>
					@endforeach
					<?PHP
					$count2 = 0;
					$count3 = 0;
					?>
				</tbody>
			</table>

			@if(isset($pointed))

			<?PHP  


//			echo '<pre>';
//			echo print_r($pointed2);
//			echo print_r($pointed_class);
//			echo print_r($pointed_id_class);
//			echo print_r($pointed_array);
//			echo '<pre>';
			?>

			<table class="table table-hover table-bordered"  style="margin-bottom: 0rem;">
				<thead>
					<tr>
						<th scope="col" style="width:400px">Добавленные</th>
						<th scope="col"> </th>
						<th scope="col" style="width:30px"> </th>
					</tr>
				</thead>
				<tbody>
					@foreach ($pointed_class as $index)
					<?PHP 
					$count2 = 0;
					?>
					@foreach ($pointed2 as $name)
					@if ($name['id_класса'] == $pointed_id_class[$count3])
					<?PHP $count2++; ?>
					@if ($count2 == 1)

					<tr>
						<td rowspan="{{$index}}" style="width:400px"><?PHP echo $classtab[$name['id_класса']-1]['Класс']; ?></td>
						<td>{{$name->Компонент}}</td>
						<td style="width:30px"><input name="addcomponent[]" value="{{$name->id}}" id="{{$name->id}}" type="checkbox" checked="checked" style="transform:scale(2.0);" onclick="checkArrowNextButton();"/></td>
					</tr>
					@else
					<tr>
						<td>{{$name->Компонент}}</td>
						<td style="width:30px"><input name="addcomponent[]" value="{{$name->id}}" id="{{$name->id}}" type="checkbox" checked="checked" style="transform:scale(2.0);" onclick="checkArrowNextButton();"/></td>
					</tr>
					@endif
					@endif
					@endforeach
					<?PHP $count3++; ?>
					@endforeach
					<?PHP
					$count2 = 0;
					$count3 = 0;
					?>
				</tbody>
			</table>


			@endif

		</div>
	</main>
	@endsection