<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Template_f extends Model
{
    protected $table = 'template_f';

    public static function get_comps($array){
    	$rezult = array();
    	foreach ($array as $value) {
    		$rezult[] = static::where('id_цели', $value)
    		->where('userid', 0)
    		->orwhere(function ($query) use ($value){
              $query->where('id_цели', $value)
                ->where('userid', Auth::user()->id);
            }) 
    		->get();
    	}
    	return $rezult;
    }

    public static function add($id_aim, $id_comp) {
		$id = static::insert(['id_компонента' => $id_comp,
			'id_цели' => $id_aim,
			'id_цели_среды' => '0',
			'userid' => Auth::user()->id]);			
		return $id;
	}

    public static function already_exist($id_aim, $id_comp){
        $rezult = static::where('id_цели', $id_aim)
            ->where('userid', Auth::user()->id)
            ->where('id_компонента', $id_comp)
            ->get();
        if ($rezult == "[]")
            $id = Template_f::add($id_aim, $id_comp);
        return $rezult;
    }

    public static function id_elements($arr) {
        $rezult = array();
        foreach ($arr as $value) {
            $rezult[] = static::where('id_цели', $value)->get();
        }
        return $rezult;
    }

    public static function id_oo() {
        $rezult = static::where('id_цели_среды', 0)->get();
        return $rezult;
    }

}
