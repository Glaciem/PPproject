<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App;

class MainController extends Controller
{
	
	public function index(){
		return view('Main.index');
	}
	public function start(){
		return view('Main.start');
	}
	public function showsignin(){
		return view('Main.signin');
	}
	public function showsignup(){
		$name = 'NULL';
		return view('Main.signup', compact('name'));
	}
	public function signup(){
		//$name = Request::input('name', 'Нет имени');
		$name = Request::all();
		return view('Main.signup', compact('name'));
	}
	public function report(){
		$step = 0;
		$req = Request::all();
		if (isset($req['Описание']) && isset($req['Характеристика'])){
			App\Reports::add($req);
		}
		return view('Main.report', compact('step', 'req'));
	}
	public function step1(){
		$step = 1;
		$req = Request::all();
		return view('Main.step1', compact('step', 'req'));
	}
	public function fake(){
		$req = Request::all();
		$WorkWith = json_decode($_COOKIE['WorkWith'], true);
		if ($WorkWith == "0"){
			$cell = json_encode($req);
			$id = App\User_steps::create($req['name_pz'], "name");
			$id = App\User_steps::add($cell, 'step1', $id);
			$id = App\User_steps::add($cell, 'FirstSelect', $id);
		}		
		else {
			$cell = json_encode($req);
			$id = App\User_steps::add($req['name_pz'], "name");
			$id = App\User_steps::add($cell, 'step1', $id);
			$id = App\User_steps::add($cell, 'FirstSelect', $id);
		}
		setcookie('WorkWith', json_encode($id));
		return view('Main.fake', compact('req', 'WorkWith'));
	}
	public function step2(){
		$req = Request::all();
		$step = 2;
		$WorkWith = json_decode($_COOKIE['WorkWith'], true);
		if($WorkWith == 0){
			$req[] = "no";
			$req[1] = "step1";
			$threats = [];
		}
		else {
			$all = App\User_steps::get($WorkWith);
			$array = [];
			foreach ($all as $key) {
				$array = $key;
			}
			$array = json_decode($array['FirstSelect'], true);
			if(isset($req['Аннотация']) && isset($req['Способ_реализации']) && isset($req['Используемые_уязвимости']) && isset($req['Возможные_последствия']) && isset($req['Вид_ресурсов']) && !empty($req['Рубрика']) && !empty($req['Свойства']) && !empty($req['Источники'])){
				App\Main::add($req, $array);
			}
			$threats = App\Main::threats($array);
		}
		$target = "СОЗДАТЬ УГРОЗУ ОБЪЕКТА ОЦЕНКИ";
		return view('Main.step2', compact('step', 'target', 'threats', 'req'));
	}
	public function step3(){
		$step = 3;
		$req = Request::all();
		$WorkWith = json_decode($_COOKIE['WorkWith'], true);
		if($WorkWith == 0){
			$req[] = "no";
			$req[1] = "step1";
			$threats = [];
		}
		else {
			$all = App\User_steps::get($WorkWith);
			$zxc = [];
			foreach ($all as $key) {
				$zxc = $key;
			}			
			$array = json_decode($zxc['FirstSelect'], true);				
			if($req == []){
				if(json_decode($zxc['step2'], true) == NULL){
					$req[] = "no";
					for($i = 1; $i < 11; $i++){
						if(json_decode($zxc['step'.$i], true) != NULL){
							$req[1] = "step".($i + 1);						
						}
					}
				}
				else{					
					$req = json_decode($zxc['step2'], true);
				}
			}
			else{
				if(isset($req['Аннотация']) && isset($req['Способ_реализации']) && isset($req['Используемые_уязвимости']) && isset($req['Возможные_последствия']) && isset($req['Вид_ресурсов']) && !empty($req['Рубрика']) && !empty($req['Свойства']) && !empty($req['Источники'])){
					App\Threats_envi::add($req, $array);
				}
				else {
					$json = json_encode($req);
					$id = App\User_steps::add($json, 'step2', $WorkWith);
					$arr = [];
					foreach (array_keys($req) as $name)
						if ($name != '_token')
							$arr[] = $name;

					$json = json_encode($arr);
					$id = App\User_steps::add($json, 'Threats', $id);
					setcookie('WorkWith', json_encode($id));
				}
			}
			$threats = App\Threats_envi::threats($array);
		}
		$target = "СОЗДАТЬ УГРОЗУ СРЕДЫ";
		return view('Main.step3', compact('step', 'req', 'threats', 'target'));
	}
	public function step4(){
		$step = 4;
		$req = Request::all();
		$WorkWith = json_decode($_COOKIE['WorkWith'], true);
		if($WorkWith == 0){
			$req[] = "no";
			$req[1] = "step1";
			$tabs = [];
		}
		else {
			$all = App\User_steps::get($WorkWith);
			$zxc = [];
			foreach ($all as $key) {
				$zxc = $key;
			}
			$array = json_decode($zxc['FirstSelect'], true);
			if($req == []){
				if(json_decode($zxc['step3'], true) == NULL){
					$req[] = "no";
					for($i = 1; $i < 11; $i++){
						if(json_decode($zxc['step'.$i], true) != NULL){
							$req[1] = "step".($i + 1);						
						}
					}
				}
				else
					$req = json_decode($zxc['step3'], true);
			}
			else{
				if(isset($req['Политика']) && !empty($req['Рубрика'])){
					App\Policy::add($req, $array);
				}
				else{
					$json = json_encode($req);
					$id = App\User_steps::add($json, 'step3', $WorkWith);
					$arr = [];
					foreach (array_keys($req) as $name)
						if ($name != '_token')
							$arr[] = $name;

					$json = json_encode($arr);
					$id = App\User_steps::add($json, 'ThreatsEnvi', $id);
					setcookie('WorkWith', json_encode($id));
				}
			}
			$tabs = App\Policy::policy($array);
		}
		$target = "СОЗДАТЬ ПОЛИТИКУ";
		return view('Main.step4', compact('step', 'req', 'tabs', 'target'));
	}
	public function step5(){
		$step = 5;
		$req = Request::all();
		$WorkWith = json_decode($_COOKIE['WorkWith'], true);
		if($WorkWith == 0){
			$req[] = "no";
			$req[1] = "step1";
			$tabs = [];
		}
		else {
			$all = App\User_steps::get($WorkWith);
			$zxc = [];
			foreach ($all as $key) {
				$zxc = $key;
			}
			$array = json_decode($zxc['FirstSelect'], true);
			if($req == []){
				if(json_decode($zxc['step4'], true) == NULL){
					$req[] = "no";
					for($i = 1; $i < 11; $i++){
						if(json_decode($zxc['step'.$i], true) != NULL){
							$req[1] = "step".($i + 1);						
						}
					}
				}
				else
					$req = json_decode($zxc['step4'], true);
			}
			else{;
				if(isset($req['Категория_предположения']) && isset($req['Предположение']) && !empty($req['Рубрика'])){
					App\Hypothesis::add($req, $array);
				}
				else{
					$json = json_encode($req);
					$id = App\User_steps::add($json, 'step4', $WorkWith);
					$arr = [];
					foreach (array_keys($req) as $name)
						if ($name != '_token')
							$arr[] = $name;

					$json = json_encode($arr);
					$id = App\User_steps::add($json, 'Policy', $id);
					setcookie('WorkWith', json_encode($id));
				}
			}
			$tabs = App\Hypothesis::hypotheses($array);
		}
		$target = "СОЗДАТЬ ПРЕДПОЛОЖЕНИЕ";
		return view('Main.step5', compact('step', 'req', 'tabs', 'target'));
	}
	public function step6(){
		$step = 6;
		$req = Request::all();
		$WorkWith = json_decode($_COOKIE['WorkWith'], true);
		$all = App\User_steps::get($WorkWith);
		if($WorkWith == 0){
			$req[] = "no";
			$req[1] = "step1";
			$uses_aims = [];
			$id_un_uses_threats = [];
			$id_un_uses_policy = [];
			$other_threats = [];
			$other_policy = [];
			$target = "СФОРМИРОВАТЬ ЦЕЛЬ ОБЪЕКТА ОЦЕНКИ";
		}
		else {
			$zxc = [];
			foreach ($all as $key) {
				$zxc = $key;
			}
			$array = json_decode($zxc['FirstSelect'], true);
			$arrayThreats = json_decode($zxc['Threats'], true);
			$arrayPolicy = json_decode($zxc['Policy'], true);
			setcookie('Threats', json_encode($arrayThreats));
			setcookie('Policy', json_encode($arrayPolicy));
			$ret = "";
			if($req == []){
				if(json_decode($zxc['step5'], true) == NULL){
					$req[0] = "no";
					for($i = 1; $i < 11; $i++){
						if(json_decode($zxc['step'.$i], true) != NULL){
							$req[1] = "step".($i + 1);						
						}
					}
				}
				else
					$req = json_decode($zxc['step5'], true);
			}
			else{
				if(isset($req['Название']) && isset($req['Описание']) && isset($req['Обоснование']) && (!empty($req['ugroza']) || !empty($req['policy']))){
					$id_last_added_aim = App\Aims::add($req, $array);
					$ret = App\Aims_threats::add_aim($id_last_added_aim, $req);			
				}
				else {
					$json = json_encode($req);
					$id = App\User_steps::add($json, 'step5', $WorkWith);
					$arr = [];
					foreach (array_keys($req) as $name)
						if ($name != '_token')
							$arr[] = $name;

					$json = json_encode($arr);
					$id = App\User_steps::add($json, 'Hypothesis', $id);
					setcookie('WorkWith', json_encode($id));
				}
			}
			if(!isset($req[0])){
				$true_aims = App\Aims::aims($array);
				$uses_aims = App\Aims_threats::uses_aims($true_aims, $arrayThreats, $arrayPolicy);
				$un_uses_threats = App\Aims_threats::un_uses_threats($uses_aims, $arrayThreats);
				$id_un_uses_threats = App\Main::id_threats($un_uses_threats);
				$un_uses_policy = App\Aims_threats::un_uses_policy($uses_aims, $arrayPolicy);
				$id_un_uses_policy = App\Policy::id_policy($un_uses_policy);
				$target = "СФОРМИРОВАТЬ ЦЕЛЬ ОБЪЕКТА ОЦЕНКИ";
				$id_other_threats = App\Aims_threats::other_threats($un_uses_threats, $arrayThreats);
				$other_threats = App\Main::id_threats($id_other_threats);
				$id_other_policy = App\Aims_threats::other_policy($un_uses_policy, $arrayPolicy);
				$other_policy = App\Policy::id_policy($id_other_policy);
			}
			else {
				$uses_aims = [];
				$id_un_uses_threats = [];
				$id_un_uses_policy = [];
				$other_threats = [];
				$other_policy = [];
				$target = "СФОРМИРОВАТЬ ЦЕЛЬ ОБЪЕКТА ОЦЕНКИ";
			}
		}
		return view('Main.step6', compact('step', 'req', 'uses_aims', 'id_un_uses_threats', 'id_un_uses_policy', 'target', 'other_threats', 'other_policy'));
	}
	public function step7(){
		$step = 7;
		$req = Request::all();
		$WorkWith = json_decode($_COOKIE['WorkWith'], true);
		if($WorkWith == 0){
			$req[] = "no";
			$req[1] = "step1";
			$uses_aims = [];
			$id_un_uses_threats_envi = [];
			$id_un_uses_hypothesis = [];
			$other_threats_envi = [];
			$other_hypothesis = [];
			$target = "СФОРМИРОВАТЬ ЦЕЛЬ СРЕДЫ";
			return view('Main.step7', compact('step', 'req', 'uses_aims', 'id_un_uses_threats_envi', 'id_un_uses_hypothesis', 'target', 'other_threats_envi', 'other_hypothesis'));
		}
		else {
			$all = App\User_steps::get($WorkWith);
			$zxc = [];
			foreach ($all as $key) {
				$zxc = $key;
			}
			$array = json_decode($zxc['FirstSelect'], true);
			$arrayThreatsEnvi = json_decode($zxc['ThreatsEnvi'], true);
			$arrayHypothesis = json_decode($zxc['Hypothesis'], true);
			setcookie('ThreatsEnvi', json_encode($arrayThreatsEnvi));
			setcookie('Hypothesis', json_encode($arrayHypothesis));
			$ret = "<(0)";
			if($req == []){
				if(json_decode($zxc['step6'], true) == NULL){
					$req[] = "no";
					for($i = 1; $i < 11; $i++){
						if(json_decode($zxc['step'.$i], true) != NULL){
							$req[1] = "step".($i + 1);						
						}
					}
				}
				else
					$req = json_decode($zxc['step6'], true);
			}
			else{
				if(isset($req['Название']) && isset($req['Описание']) && isset($req['Обоснование']) && (!empty($req['ugroza']) || !empty($req['hypothesis']))){
					$id_last_added_aim_envi = App\Aims_envi::add($req, $array);
					$ret = App\Aims_threats_envi::add_aim($id_last_added_aim_envi, $req);			
				}
				else {
					$json = json_encode($req);
					$id = App\User_steps::add($json, 'step6', $WorkWith);
					$arr = [];
					foreach (array_keys($req) as $name)
						if ($name != '_token')
							$arr[] = $name;

					$json = json_encode($arr);
					$id = App\User_steps::add($json, 'Aims', $id);
					setcookie('WorkWith', json_encode($id));
				}
			}
			if(!isset($req[0])){
				$true_aims = App\Aims_envi::aims_envi($array);
				$uses_aims = App\Aims_threats_envi::uses_aims_envi($true_aims, $arrayThreatsEnvi, $arrayHypothesis);
				$un_uses_threats_envi = App\Aims_threats_envi::un_uses_threats_envi($uses_aims, $arrayThreatsEnvi);
				$id_un_uses_threats_envi = App\Threats_envi::id_threats_envi($un_uses_threats_envi);
				$un_uses_hypothesis = App\Aims_threats_envi::un_uses_hypothesis($uses_aims, $arrayHypothesis);
				$id_un_uses_hypothesis = App\Hypothesis::id_hypothesis($un_uses_hypothesis);
				$target = "СФОРМИРОВАТЬ ЦЕЛЬ СРЕДЫ";
				$id_other_threats_envi = App\Aims_threats_envi::other_threats_envi($un_uses_threats_envi, $arrayThreatsEnvi);
				$other_threats_envi = App\Threats_envi::id_threats_envi($id_other_threats_envi);
				$id_other_hypothesis = App\Aims_threats_envi::other_hypothesis($un_uses_hypothesis, $arrayHypothesis);
				$other_hypothesis = App\Hypothesis::id_hypothesis($id_other_hypothesis);
				if($ret != "<(0)")
					return view('Main.step7', compact('step', 'req', 'uses_aims', 'id_un_uses_threats_envi', 'id_un_uses_hypothesis', 'target', 'other_threats_envi', 'other_hypothesis', 'ret'));
				else
					return view('Main.step7', compact('step', 'req', 'uses_aims', 'id_un_uses_threats_envi', 'id_un_uses_hypothesis', 'target', 'other_threats_envi', 'other_hypothesis'));
			}
			else {
				$uses_aims = [];
				$id_un_uses_threats_envi = [];
				$id_un_uses_hypothesis = [];
				$other_threats_envi = [];
				$other_hypothesis = [];
				$target = "СФОРМИРОВАТЬ ЦЕЛЬ СРЕДЫ";
				return view('Main.step7', compact('step', 'req', 'uses_aims', 'id_un_uses_threats_envi', 'id_un_uses_hypothesis', 'target', 'other_threats_envi', 'other_hypothesis'));
			}
		}
	}
	public function step8(){
		$step = 8;
		$req = Request::all();
		$WorkWith = json_decode($_COOKIE['WorkWith'], true);
		if($WorkWith == 0){
			$req[] = "no";
			$req[1] = "step1";
			$component = [];
			$classtab = [];
			$allcomponent = [];
			$component_template = [];
			$uncomponent = [];
			$almost_all_class = [];
			$target = "ДОБАВИТЬ КОМПОНЕНТ ДОВЕРИЯ";
			$target2 = "СОЗДАТЬ КОМПОНЕНТ ДОВЕРИЯ";
			return view('Main.step8', compact('step', 'component', 'classtab', 'target', 'target2', 'allcomponent', 'component_template', 'uncomponent', 'req', 'almost_all_class'));
		}
		else {
			$all = App\User_steps::get($WorkWith);
			$zxc = [];
			foreach ($all as $key) {
				$zxc = $key;
			}
			$array = json_decode($zxc['FirstSelect'], true);
			if($req == []){
				if(json_decode($zxc['step7'], true) == NULL){
					$req[] = "no";
					for($i = 1; $i < 11; $i++){
						if(json_decode($zxc['step'.$i], true) != NULL){
							$req[1] = "step".($i + 1);						
						}
					}
				}
				else
					$req = json_decode($zxc['step7'], true);
			}
			else{		
				if(isset($req['Класс']) && isset($req['Компонент'])){
					if(isset($req['Зависимость'])){
						$zavisimost = App\Component_of_trust::idcomponent($req);
					}else{
						$zavisimost = 0;
					}
					$id_new_class = "none";
					if ($req['Класс'] == '0'){
						$id_new_class = App\Class_of_trust::add($req);
					}
					$id_new_comp = App\Component_of_trust::add($req, $zavisimost, $id_new_class);
					App\Template_level_3::add_temp($id_new_comp, $array);
					//return view('Main.step8', compact('step', 'component', 'classtab', 'target', 'target2', 'allcomponent', 'component_template', 'uncomponent', 'req', 'zavisimost'));
				}
				else {
					if(!isset($req['newcomponent'])){
						$json = json_encode($req);
						$id = App\User_steps::add($json, 'step7', $WorkWith);
						$arr = [];
						foreach (array_keys($req) as $name)
							if ($name != '_token')
								$arr[] = $name;

						$json = json_encode($arr);
						$id = App\User_steps::add($json, 'AimsThreats', $id);
						setcookie('WorkWith', json_encode($id));
					}
					else {

					}
				}
			}
			$almost_all_class = App\Class_of_trust::get_almost_all();
			$component_template_x = App\Template_level_3::get_temp($array);
			$component_template = array();
			foreach ($component_template_x as $value) {
				foreach ($value as $value2) {
					$component_template[] = $value2;
				}
			}
			$component = App\Component_of_trust::idcomponent($component_template);
			$allcomponent = App\Component_of_trust::get_almost_all();
			$classtab = App\Class_of_trust::all();
			$uncomponent = App\Component_of_trust::uncomponent($allcomponent, $component);
			$target = "ДОБАВИТЬ КОМПОНЕНТ ДОВЕРИЯ";
			$target2 = "СОЗДАТЬ КОМПОНЕНТ ДОВЕРИЯ";
			if(isset($req['newcomponent'])){
				$pointed = App\Component_of_trust::pointed($req);
				return view('Main.step8', compact('step', 'component', 'classtab', 'target', 'target2', 'allcomponent', 'component_template', 'uncomponent', 'req', 'pointed', 'almost_all_class'));
			}
			return view('Main.step8', compact('step', 'component', 'classtab', 'target', 'target2', 'allcomponent', 'component_template', 'uncomponent', 'req', 'almost_all_class'));
		}		
	}
	public function step9(){
		$step = 9;
		$req = Request::all();
		$target = "СОЗДАТЬ ПОДКОМПОНЕНТ ДОВЕРИЯ";
		$WorkWith = json_decode($_COOKIE['WorkWith'], true);
		if($WorkWith == 0){
			$req[] = "no";
			$req[1] = "step1";
			$components = [];
			$sub_comps = [];
			$us = [];
			return view('Main.step9', compact('step', 'req', 'components', 'sub_comps', 'target', 'us'));
		}
		else {
			$all = App\User_steps::get($WorkWith);
			$zxc = [];
			foreach ($all as $key) {
				$zxc = $key;
			}
			if($req == []){
				if(json_decode($zxc['step8'], true) == NULL){
					$req[] = "no";
					for($i = 1; $i < 11; $i++){
						if(json_decode($zxc['step'.$i], true) != NULL){
							$req[1] = "step".($i + 1);						
						}
					}
				}
				else{
					$req = json_decode($zxc['step8'], true);
					$s1 = json_decode($zxc['Components'], true);
					setcookie('Components', json_encode($s1));
					$s2 = json_decode($zxc['AelemetsADD'], true);
					setcookie('AelemetsADD', json_encode($s2));
					$s3 = json_decode($zxc['Aelemets'], true);
					setcookie('Aelemets', json_encode($s3));
				}
			}
			if (isset($req['component']) || isset($req['addcomponent'])){
				$json = json_encode($req);
				setcookie('Components', $json);
				$id = App\User_steps::add($json, 'step8', $WorkWith);
				$id = App\User_steps::add($json, 'Components', $id);
				$mass = array();
				$mass2 = array();
				if(isset($req['component'])){
					foreach ($req['component'] as $name){
							$mass[] = $name;
					}
					if (isset($req['addcomponent'])){
						foreach ($req['addcomponent'] as $name){
							$mass2[] = $name;
						}
						$json2 = json_encode($mass2);
						$id = App\User_steps::add($json2, 'AelemetsADD', $id);
					}
					$json = json_encode($mass);
					$id = App\User_steps::add($json, 'Aelemets', $id);
				}
				setcookie('WorkWith', json_encode($id));
				$components = App\Component_of_trust::idcomponent($req);
				$us = array();
				foreach ($components as $name)
					foreach ($name as $value)
						if($value['userid'] == Auth::user()->id){
							$us[] = $value['Компонент'];
						}
				$sub_comps = App\Sub_component_of_trust::all_sub_comp($components);
				return view('Main.step9', compact('step', 'req', 'components', 'sub_comps', 'target', 'us'));
			}
			else {
				if (isset($req['Название']) && isset($req['Описание']) && $req['Тип'] != '0' && $req['Комп'] != '0'){
					$comp = App\Component_of_trust::name_comp($req);
					$ret = App\Sub_component_of_trust::add_sub($req, $comp);
					//return view('Main.step9', compact('step', 'req', 'components', 'sub_comps', 'target', 'us', 'ret'));
				}
				if(!isset($req[0])){
					$array = json_decode($zxc['Components'], true);
					$components = App\Component_of_trust::idcomponent($array);
					$us = array();
					foreach ($components as $name)
						foreach ($name as $value)
							if($value['userid'] == Auth::user()->id){
								$us[] = $value['Компонент'];
							}
					$sub_comps = App\Sub_component_of_trust::all_sub_comp($components);
				}
				else {
					$components = [];
					$sub_comps = [];
					$us = [];
				}
				return view('Main.step9', compact('step', 'req', 'components', 'sub_comps', 'target', 'us'));
			}
		}		
	}
	public function step10(){
		$step = 10;
		$req = Request::all();
		$WorkWith = json_decode($_COOKIE['WorkWith'], true);
		if($WorkWith == 0){
			$req[] = "no";
			$req[1] = "step1";
			$arrayComps = [];
			$classtab = [];
			$nameAims = [];
			$allcomponent = [];
			$arrayIDcomps = [];
			$arrayAims = [];
		}
		else {
			$all = App\User_steps::get($WorkWith);
			$zxc = [];
			foreach ($all as $key) {
				$zxc = $key;
			}
			$array = json_decode($zxc['FirstSelect'], true);
			if($req == []){
				if(json_decode($zxc['step9'], true) == NULL){
					$req[] = "no";
					for($i = 1; $i < 11; $i++){
						if(json_decode($zxc['step'.$i], true) != NULL){
							$req[1] = "step".($i + 1);						
						}
					}
				}
				else{
					$req = json_decode($zxc['step9'], true);
					$s1 = json_decode($zxc['Aims'], true);
					setcookie('Aims', json_encode($s1));
				}
			}
			else {
				if(isset($req['Класс']) && isset($req['Компонент']) && isset($req['Aim']) && !empty($req['Рубрика'])){
					if(isset($req['Зависимость'])){
						$zavisimost = App\Component_of_trust_f::idcomponent($req);
					}else{
						$zavisimost = 0;
					}
					$id_new_class = "none";
					if ($req['Класс'] == '0'){
						$id_new_class = App\Class_of_trust_f::add($req);
					}
					$id_new_comp = App\Component_of_trust_f::add($req, $zavisimost, $id_new_class);
					$fck = App\Template_f::add($req['Aim'], $id_new_comp);
					//return view('Main.step10', compact('step', 'req', 'arrayComps', 'classtab', 'target2', 'nameAims', 'allcomponent', 'zavisimost'));
				}
				else{
					if(isset($req['Фки']) && isset($req['picked_aim'])){
						foreach ($req['Фки'] as $key) {
							$rez = App\Template_f::already_exist($req['picked_aim'], $key);
						}
					}
					else{
						$json = json_encode($req);
						$id = App\User_steps::add($json, 'step9', $WorkWith);
						$mass = array();
						if(isset($req['subcomponent'])){
							foreach ($req['subcomponent'] as $name){
								$mass[] = $name;
							}
							$json = json_encode($mass);
							$id = App\User_steps::add($json, 'Asubelements', $id);
						}
						setcookie('WorkWith', json_encode($id));
					}
				}
			}
			if(!isset($req[0])){
				$arrayAims = json_decode($zxc['Aims'], true); //json_decode($_COOKIE['Aims'], true);
				$almost_all_class = App\Class_of_trust_f::get_almost_all();
				$nameAims = App\Aims::id_aims($arrayAims);
				$arrayIDcomps = App\Template_f::get_comps($arrayAims);
				$arrayComps = "<(0)";
				$classtab = App\Class_of_trust_f::all();
				$allcomponent = App\Component_of_trust_f::get_almost_all();
				if(!empty($arrayIDcomps[0][0])){
					$arrayComps = App\Component_of_trust_f::get_comps($arrayIDcomps);
				}
			}
			else {
				$almost_all_class = [];
				$arrayComps = [];
				$classtab = [];
				$nameAims = [];
				$allcomponent = [];
				$arrayIDcomps = [];
				$arrayAims = [];
			}
		}
		$target = "СВЯЗАТЬ ЦЕЛЬ С КОМПОНЕНТАМИ";
		$target2 = "СОЗДАТЬ КОМПОНЕНТ ДОВЕРИЯ ДЛЯ ЦЕЛИ";
		//return view('Main.step10', compact('step', 'req', 'arrayAims', 'nameAims', 'arrayIDcomps'));
		return view('Main.step10', compact('step', 'req', 'arrayComps', 'classtab', 'target2', 'nameAims', 'allcomponent', 'arrayIDcomps', 'arrayAims', 'almost_all_class', 'target'));
	}
	public function step11(){
		$step = 11;
		$req = Request::all();
		$target = "СОЗДАТЬ ПОДКОМПОНЕНТ ДОВЕРИЯ";
		$WorkWith = json_decode($_COOKIE['WorkWith'], true);
		if($WorkWith == 0){
			$req[] = "no";
			$req[1] = "step1";
			$components = [];
			$sub_comps = [];
			$us = [];
			return view('Main.step11', compact('step', 'req', 'components', 'sub_comps', 'target', 'us'));
		}
		else {
			$all = App\User_steps::get($WorkWith);
			$zxc = [];
			foreach ($all as $key) {
				$zxc = $key;
			}
			if($req == []){
				if(json_decode($zxc['step10'], true) == NULL){
					$req[] = "no";
					for($i = 1; $i < 11; $i++){
						if(json_decode($zxc['step'.$i], true) != NULL){
							$req[1] = "step".($i + 1);						
						}
					}
				}
				else{
					$req = json_decode($zxc['step10'], true);
					$s1 = json_decode($zxc['Components_f_oo'], true);
					setcookie('Components_f_oo', json_encode($s1));
					$s2 = json_decode($zxc['FelemetsADD'], true);
					setcookie('FelemetsADD', json_encode($s2));
					$s3 = json_decode($zxc['Felemets'], true);
					setcookie('Felemets', json_encode($s3));
				}
			}
			if (isset($req['component']) || isset($req['addcomponent'])){
				$json = json_encode($req);
				setcookie('Components_f_oo', $json);

				$id = App\User_steps::add($json, 'step10', $WorkWith);
				$id = App\User_steps::add($json, 'Components_f_oo', $id);
				$mass = array();
				$mass2 = array();
				if (isset($req['component'])){
					foreach ($req['component'] as $name){
						$mass[] = $name;
					}
					if (isset($req['addcomponent'])){
						foreach ($req['addcomponent'] as $name){
							$mass2[] = $name;
						}
						$json2 = json_encode($mass2);
						$id = App\User_steps::add($json2, 'FelemetsADD', $id);
					}
					$json = json_encode($mass);
					$id = App\User_steps::add($json, 'Felemets', $id);
				}
				setcookie('WorkWith', json_encode($id));

				$components = App\Component_of_trust_f::idcomps($req);
				$us = array();
				foreach ($components as $name)
					foreach ($name as $value)
						if($value['userid'] == Auth::user()->id){
							$us[] = $value['Компонент'];
						}
				$sub_comps = App\Sub_component_of_trust_f::all_sub_comp($components);
				return view('Main.step11', compact('step', 'req', 'components', 'sub_comps', 'target', 'us'));
			}		
			else {
				if (isset($req['Название']) && isset($req['Описание']) && $req['Комп'] != '0'){
					$comp = App\Component_of_trust_f::name_comp($req);
					$ret = App\Sub_component_of_trust_f::add_sub($req, $comp);
					//return view('Main.step9', compact('step', 'req', 'components', 'sub_comps', 'target', 'us', 'ret'));
				}
				if(!isset($req[0])){
					$array = json_decode($zxc['Components_f_oo'], true);
					$components = App\Component_of_trust_f::idcomps($array);
					$us = array();
					foreach ($components as $name)
						foreach ($name as $value)
							if($value['userid'] == Auth::user()->id){
								$us[] = $value['Компонент'];
							}
					$sub_comps = App\Sub_component_of_trust_f::all_sub_comp($components);
				}
				else{
					$components = [];
					$sub_comps = [];
					$us = [];
				}
				return view('Main.step11', compact('step', 'req', 'components', 'sub_comps', 'target', 'us'));
			}
		}
	}
	public function step12(){
		$step = 12;
		$req = Request::all();
		$WorkWith = json_decode($_COOKIE['WorkWith'], true);
		if($WorkWith == 0){
			$req[] = "no";
			$req[1] = "step1";
		}
		else {
			$all = App\User_steps::get($WorkWith);
			$zxc = [];
			foreach ($all as $key) {
				$zxc = $key;
			}
			setcookie('FirstSelect', $zxc['FirstSelect']);		
			setcookie('Threats', $zxc['Threats']);		
			setcookie('ThreatsEnvi', $zxc['ThreatsEnvi']);		
			setcookie('Policy', $zxc['Policy']);		
			setcookie('Hypothesis', $zxc['Hypothesis']);		
			setcookie('Aims', $zxc['Aims']);		
			setcookie('AimsThreats', $zxc['AimsThreats']);		
			setcookie('Components', $zxc['Components']);		
			setcookie('AelemetsADD', $zxc['AelemetsADD']);		
			setcookie('Aelemets', $zxc['Aelemets']);
			//setcookie('Asubelements', $zxc['Asubelements']); fuck this shit
			setcookie('Components_f_oo', $zxc['Components_f_oo']);		
			setcookie('FelemetsADD', $zxc['FelemetsADD']);		
			setcookie('Felemets', $zxc['Felemets']);
			setcookie('Fsubelements', $zxc['Fsubelements']);
			if($req == []){
				if(json_decode($zxc['step11'], true) == NULL){
					$req[] = "no";
					for($i = 1; $i < 11; $i++){
						if(json_decode($zxc['step'.$i], true) != NULL){
							$req[1] = "step".($i + 1);						
						}
					}
				}
				else
					$req = json_decode($zxc['step11'], true);
			}
			else {
				$json = json_encode($req);
				$id = App\User_steps::add($json, 'step11', $WorkWith);
				$mass = array();
				foreach (array_keys($req) as $name){
					if ($name != '_token'){
						$mass[] = $name;
					}
				}
				$json = json_encode($mass);
				$id = App\User_steps::add($json, 'Fsubelements', $id);
				setcookie('WorkWith', json_encode($id));
			}
		}		
		return view('Main.step12', compact('step', 'req'));
	}
	public function step13(){
		$step = 13;
		$req = Request::all();
		return view('Main.step13', compact('step', 'req'));
	}
	public function usersparams(){
		$usersparams = "1";
		$req = Request::all();
		$step = 0;
		if(!empty($req)){
			$id = array_keys($req);
			$id = $id[0];
			if($id[strlen($id) - 1] == 'u'){
				$ret = App\Main::del($id);
			}
			elseif($id[strlen($id) - 1] == 'e'){
				$ret = App\Threats_envi::del($id);
			}
			elseif($id[strlen($id) - 1] == 'p'){
				$ret = App\Policy::del($id);
			}
			elseif($id[strlen($id) - 1] == 'h'){
				$ret = App\Hypothesis::del($id);
			}
			elseif($id[strlen($id) - 1] == 'a'){
				$ret = App\Aims::del($id);
				$ret = App\Aims_threats::del($id);
			}
			elseif($id[strlen($id) - 1] == 't'){
				$ret = App\Aims_envi::del($id);
				$ret = App\Aims_threats_envi::del($id);
			}
		}
		$threats = App\Main::all_for_user();
		$hypotheses = App\Hypothesis::all_for_user();
		$policy = App\Policy::all_for_user();
		$threats_envi = App\Threats_envi::all_for_user();
		$aims = App\Aims::all_for_user();
		$aims_threats = App\Aims_envi::all_for_user();
		return view('Main.usersparams', compact('usersparams', 'threats', 'threats_envi', 'policy', 'hypotheses', 'req', 'aims', 'aims_threats', 'step'));
	}
	public function usersaves(){
		$step = 0;
		$req = Request::all();
		if(!empty($req)){
			$id = array_keys($req);
			$id = $id[0];
			App\User_steps::del($id);
		}
		$all = App\User_steps::try_get();
		return view('Main.usersaves', compact('step', 'all', 'req'));
	}
}

