<?php

namespace App\Http\Controllers;

use Request;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App;

class Create extends Controller
{
	public function show($id){ 
		//$dl = File::find($id);
	//$url = Storage::url('test.txt');
		return Storage::download('test.txt', 'TEST');           
	}

	public function create(){
		$step = 0;

		
		$FirstSelect = json_decode($_COOKIE['FirstSelect'], true);

		$DefSocrs = App\Reductions::get_def($FirstSelect);
		$SelectedSocrs = App\Reductions::get_selected($FirstSelect);

		$envi = App\Environment::get_envi($FirstSelect['select1']);
		$type_opr = App\Types::get_type($FirstSelect);


		$cookieThreats = json_decode($_COOKIE['Threats'], true);
		$threats = App\Main::id_threats($cookieThreats);

		$cookieThreatsEnvi = json_decode($_COOKIE['ThreatsEnvi'], true);
		$ThreatsEnvi = App\Threats_envi::id_threats_envi($cookieThreatsEnvi);

		$cookiePolicy = json_decode($_COOKIE['Policy'], true);
		$policy = App\Policy::id_policy($cookiePolicy);

		$cookieHypothesis = json_decode($_COOKIE['Hypothesis'], true);
		$hypothesis = App\Hypothesis::id_hypothesis($cookieHypothesis);

		$cookieAims = json_decode($_COOKIE['Aims'], true);
		$Aims = App\Aims::id_aims($cookieAims);

		$cookieAimsThreats = json_decode($_COOKIE['AimsThreats'], true);
		$AimsThreats = App\Aims_envi::id_aims($cookieAimsThreats);

		$threatsForAims = App\Aims_threats::id_ugr($cookieAims);
		$PolicyForAims = App\Aims_threats::id_polit($cookieAims);
		$FelemetsForAims = App\Template_f::id_elements($cookieAims);

		$FelemetsOO = App\Template_f::id_oo();

		$cookieAims = json_decode($_COOKIE['Aims'], true);

		$cookie1 = json_decode($_COOKIE['Aelemets'], true);
		if(isset($_COOKIE['AelemetsADD'])){
			$cookie2 = json_decode($_COOKIE['AelemetsADD'], true);
		}
		$array = array();
		foreach ($cookie1 as $value) {
			$array[] = $value;
		}
		if(isset($_COOKIE['AelemetsADD'])){
			foreach ($cookie2 as $value) {
				$array[] = $value;
			}
		}
	//	$cookieComponent_of_trust = json_decode($_COOKIE['Aelemets'], true);
		$ComponentOfTrust = App\Component_of_trust::id_comp($array);
	//	$ComponentOfTrust = App\Component_of_trust::id_comp($cookieComponent_of_trust);
		if(isset($_COOKIE['AelemetsADD'])){
			$ComponentOfTrustADD = App\Component_of_trust::id_comp($cookie2);
		}
		//////////////////////////////////////////////////////////////////////////////


		$cookieF1 = json_decode($_COOKIE['Felemets'], true);
		if(isset($_COOKIE['FelemetsADD'])){
			$cookieF2 = json_decode($_COOKIE['FelemetsADD'], true);
		}
		$array2 = array();
		foreach ($cookieF1 as $value) {
			$array2[] = $value;
		}
		if(isset($_COOKIE['FelemetsADD'])){
			foreach ($cookieF2 as $value) {
				$array2[] = $value;
			}
		}
	//	$cookieComponent_of_trust_F = json_decode($_COOKIE['Felemets'], true);
		$ComponentOfTrustF = App\Component_of_trust_f::idcomps($array2);
	//	$ComponentOfTrustF = App\Component_of_trust_f::idcomps($cookieComponent_of_trust_F);
		if(isset($_COOKIE['FelemetsADD'])){
			$ComponentOfTrustFADD = App\Component_of_trust_f::idcomps($cookieF2);
		}

		$ClassOfTrustF = App\Class_of_trust_f::all();
		$ClassOfTrust = App\Class_of_trust::all();

		$cookieSubComponent_of_trust_F = json_decode($_COOKIE['Fsubelements'], true);
		$SubComponentOfTrustF = App\Sub_component_of_trust_f::id_sub($cookieSubComponent_of_trust_F);
		
		//$cookieSubComponent_of_trust = json_decode($_COOKIE['Asubelements'], true);
		$WorkWith = json_decode($_COOKIE['WorkWith'], true);
		$all = App\User_steps::get($WorkWith);
		$zxc = [];
		foreach ($all as $key) {
			$zxc = $key;
		}
		$cookieSubComponent_of_trust = json_decode($zxc['Asubelements'], true);

		$SubComponentOfTrust = App\Sub_component_of_trust::id_sub($cookieSubComponent_of_trust);


		if (isset($_COOKIE['AelemetsADD'])){
			if (isset($_COOKIE['FelemetsADD'])){
				return view('Main.create', compact('step', 'FirstSelect', 'threats', 'policy', 'ThreatsEnvi', 'hypothesis', 'Aims', 'AimsThreats', 'threatsForAims', 'PolicyForAims', 'ComponentOfTrust', 'ComponentOfTrustF', 'ClassOfTrustF', 'SubComponentOfTrustF', 'SubComponentOfTrust', 'ComponentOfTrustADD', 'ComponentOfTrustFADD', 'ClassOfTrust', 'FelemetsForAims', 'FelemetsOO', 'DefSocrs', 'SelectedSocrs', 'envi', 'type_opr'));
			}
			return view('Main.create', compact('step', 'FirstSelect', 'threats', 'policy', 'ThreatsEnvi', 'hypothesis', 'Aims', 'AimsThreats', 'threatsForAims', 'PolicyForAims', 'ComponentOfTrust', 'ComponentOfTrustF', 'ClassOfTrustF', 'SubComponentOfTrustF', 'SubComponentOfTrust', 'ComponentOfTrustADD', 'ClassOfTrust', 'FelemetsForAims', 'FelemetsOO', 'DefSocrs', 'SelectedSocrs', 'envi', 'type_opr'));
		}
		if (isset($_COOKIE['FelemetsADD'])){
			return view('Main.create', compact('step', 'FirstSelect', 'threats', 'policy', 'ThreatsEnvi', 'hypothesis', 'Aims', 'AimsThreats', 'threatsForAims', 'PolicyForAims', 'ComponentOfTrust', 'ComponentOfTrustF', 'ClassOfTrustF', 'SubComponentOfTrustF', 'SubComponentOfTrust', 'ComponentOfTrustFADD', 'ClassOfTrust', 'FelemetsForAims', 'FelemetsOO', 'DefSocrs', 'SelectedSocrs','envi','type_opr'));
		}
		return view('Main.create', compact('step', 'FirstSelect', 'threats', 'policy', 'ThreatsEnvi', 'hypothesis', 'Aims', 'AimsThreats', 'threatsForAims', 'PolicyForAims', 'ComponentOfTrust', 'ComponentOfTrustF', 'ClassOfTrustF', 'SubComponentOfTrustF', 'SubComponentOfTrust', 'ClassOfTrust', 'FelemetsForAims', 'FelemetsOO', 'DefSocrs', 'SelectedSocrs', 'envi', 'type_opr'));
	}

}
