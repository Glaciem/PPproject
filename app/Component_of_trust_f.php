<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Component_of_trust_f extends Model
{
	protected $table = 'component_of_trust_f';

	public static function get_comps($array){
		$rezult = array();
		foreach ($array as $s1) {
			foreach ($s1 as $s2) {
				$rezult[] = static::where('id', $s2['id_компонента'])->get();
			}
		}
		return $rezult;
	}

	public static function idcomps($array){
		$rezult = array();
		if (isset($array['component'])){
			foreach ($array['component'] as $value) {
				$rezult[] = static::where('id', $value)->get();
			}
			if (isset($array['addcomponent'])){
				foreach ($array['addcomponent'] as $value) {
					$rezult[] = static::where('id', $value)->get();
				}
			}
		}elseif(isset($array['addcomponent'])){
			foreach ($array['addcomponent'] as $value) {
				$rezult[] = static::where('id', $value)->get();
			}
			if (isset($array['component'])){
				foreach ($array['component'] as $value) {
					$rezult[] = static::where('id', $value)->get();
				}
			}
		}
		else{
			foreach ($array as $value) {
				$rezult[] = static::where('id', $value)->get();
			}
		}		
		return $rezult;
	}

	public static function idcomponent($array) {
		$rezult = array();
		if (isset($array['Зависимость'])){
			foreach ($array['Зависимость'] as $value) {
				$rezult[] = static::where('id', $value)->get();
			}
		}elseif (isset($array['component'])){
			foreach ($array['component'] as $value) {
				$rezult[] = static::where('id', $value)->get();
			}
			if (isset($array['addcomponent'])){
				foreach ($array['addcomponent'] as $value) {
					$rezult[] = static::where('id', $value)->get();
				}
			}
		}
		else{
			foreach ($array as $value) {
				$rezult[] = static::where('id', $value['id_компонента'])->get();
			}
		}		
		return $rezult;
	}

	public static function get_almost_all(){
		$rezult = static::where('userid', 0)
		->orwhere('userid', Auth::user()->id)
		->get();
		return $rezult;
	}

	public static function add($req, $zavisimost, $id_new_class) {
		$string = "Зависимости: ";
		if ($zavisimost == 0){
			$string .= "отсутствуют.";
		}else{
			foreach ($zavisimost as $value) {
				foreach ($value as $name) {
					$string .= $name['Компонент'];
					$string .= " ";
				}
			}
		}
		$co = array();
		foreach ($req['Рубрика'] as $name) {
			$co[] = $name;
		}
		$rub = implode(", ", $co);
		if ($id_new_class == "none"){		
			$id = static::insertGetId(['userid' => Auth::user()->id,
				'id_класса' => $req['Класс'],
				'Компонент' => $req['Компонент'],
				'Зависимость' => $string,
				'Рубрика' => $rub]);
		}
		else {
			$id = static::insertGetId(['userid' => Auth::user()->id,
				'id_класса' => $id_new_class,
				'Компонент' => $req['Компонент'],
				'Зависимость' => $string,
				'Рубрика' => $rub]);
		}			
		return $id;
	}

	public static function name_comp($array) {
		$rezult = static::where('Компонент', $array['Комп'])->get('id');
		return $rezult;
	}
}
