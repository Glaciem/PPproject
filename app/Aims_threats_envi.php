<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aims_threats_envi extends Model
{
    protected $table = 'aims_threats_envi';

    public static function uses_aims_envi($true_aims, $arrayThreatsEnvi, $arrayHypothesis){
        $uses_aims = array();
        foreach ($true_aims as $true_aims_id) {
            $id_ugroz_sredi = static::where('id_цели_среды', $true_aims_id['id'])->get('id_угроз');
            $id_pred = static::where('id_цели_среды', $true_aims_id['id'])->get('id_предположений'); 
            $ok_us = true;
            $ok_pr = true;
            foreach ($id_ugroz_sredi as $id_2) {
                if ($id_2['id_угроз'] == 0){}
                else {
                    if (in_array($id_2['id_угроз'], $arrayThreatsEnvi)) {}
                        else {
                            $ok_us = false;
                            break;
                    }
                }
            }          
            foreach ($id_pred as $id_2) {
                    if ($id_2['id_предположений'] == 0){}
                    else {
                        if (in_array($id_2['id_предположений'], $arrayHypothesis)) {}
                        else {
                            $ok_pr = false;
                            break;
                        }
                    }
                }
            if ($ok_us == true && $ok_pr == true)
                $uses_aims[] = $true_aims_id;
        }
        return $uses_aims;
    }

    public static function un_uses_threats_envi($uses_aims, $arrayThreatsEnvi){
        $un_uses_threats_envi = array();
        $id_ugroz = array();
        $id_ugroz2 = array();
        foreach ($uses_aims as $uses_aims_id) {
            $id_ugroz[] = static::where('id_цели_среды', $uses_aims_id['id'])->get('id_угроз');
        }
        foreach ($id_ugroz as $id) {
            foreach ($id as $id2) {
               $id_ugroz2[] = $id2['id_угроз'];
            }
        }
        foreach ($arrayThreatsEnvi as $id_2) {
            if (in_array($id_2, $id_ugroz2)){}
            else
                $un_uses_threats_envi[] = $id_2;
        }    
        return $un_uses_threats_envi;
    }

    public static function un_uses_hypothesis($uses_aims, $arrayHypothesis){
        $un_uses_hypothesis = array();
        $id_hypothesis = array();
        $id_hypothesis2 = array();
        foreach ($uses_aims as $uses_aims_id) {
            $id_hypothesis[] = static::where('id_цели_среды', $uses_aims_id['id'])->get('id_предположений');
        }
        foreach ($id_hypothesis as $id) {
            foreach ($id as $id2) {
               $id_hypothesis2[] = $id2['id_предположений'];
            }
        }
        foreach ($arrayHypothesis as $id_2) {
            if (in_array($id_2, $id_hypothesis2)){}
            else
                $un_uses_hypothesis[] = $id_2;
        }    
        return $un_uses_hypothesis;
    }

    public static function other_threats_envi($un_uses_threats_envi, $arrayThreatsEnvi){
        if(!empty($un_uses_threats_envi)){
            $other_threats_envi = array();
            foreach ($arrayThreatsEnvi as $id) {
                if (in_array($id, $un_uses_threats_envi)){}
                else
                    $other_threats_envi[] = $id;
            }
        }
        else
            $other_threats_envi = $arrayThreatsEnvi;
        return $other_threats_envi;
    }

    public static function other_hypothesis($un_uses_hypothesis, $arrayHypothesis){
        if(!empty($un_uses_hypothesis)){
            $other_hypothesis = array();
            foreach ($arrayHypothesis as $id) {
                if (in_array($id, $un_uses_hypothesis)){}
                else
                    $other_hypothesis[] = $id;
            }
        }
        else
            $other_hypothesis = $arrayHypothesis;
        return $other_hypothesis;
    }

    public static function add_aim($id_last_added_aim_envi, $array){
        $id_ugroz = array();
        $id_hypothesis = array();
        $u = 0;
        $h = 0;
        if(!empty($array['ugroza'])){
            foreach ($array['ugroza'] as $name){
                $id_ugroz[] = $name;
                $u++;
            }
        }
        if(!empty($array['hypothesis'])){
            foreach ($array['hypothesis'] as $name){
                $id_hypothesis[] = $name;
                $h++;
            }
        }
        if ($u > $h){ //если угроз > предположений
            for ($i = 0; $i < $u; $i++){
                if($h > 0){
                    $s = static::insert(['id_цели_среды' => $id_last_added_aim_envi[0]['id'], 
                        'id_угроз' => $id_ugroz[$i],
                        'id_предположений' => $id_hypothesis[$i]]);
                }
                else {
                    $s = static::insert(['id_цели_среды' => $id_last_added_aim_envi[0]['id'], 
                        'id_угроз' => $id_ugroz[$i],
                        'id_предположений' => 0]);
                }
                $h--;
            }            
        }
        else 
            if($u < $h){ //если угроз < предположений
                for ($i = 0; $i < $h; $i++){
                    if($u > 0){
                        $s = static::insert(['id_цели_среды' => $id_last_added_aim_envi[0]['id'], 
                            'id_угроз' => $id_ugroz[$i],
                            'id_предположений' => $id_hypothesis[$i]]);
                    }
                    else {
                        $s = static::insert(['id_цели_среды' => $id_last_added_aim_envi[0]['id'], 
                            'id_угроз' => 0,
                            'id_предположений' => $id_hypothesis[$i]]);
                    }
                    $u--;
                }
            }
            else { //если равное количество   
               for ($i = 0; $i < $h; $i++){
                    $s = static::insert(['id_цели_среды' => $id_last_added_aim_envi[0]['id'], 
                        'id_угроз' => $id_ugroz[$i],
                        'id_предположений' => $id_hypothesis[$i]]);
                }      
            }
        return "wowitswork";
    }

    public static function del($id){
        $id = mb_substr($id, 0, -1);
        static::where('id_цели_среды', $id)->delete();
        return "Great";
    }
}
