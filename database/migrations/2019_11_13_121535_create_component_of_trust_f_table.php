<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComponentOfTrustFTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('component_of_trust_f', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_класса');
            $table->text('Компонент');
            $table->longText('Зависимость');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('component_of_trust_f');
    }
}
