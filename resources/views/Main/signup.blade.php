@extends('Main.layout')
@section('content')
<br>
<br>
<br>
<div class="container">

	<div class="row">
		<div class="col-sm">
		</div>
		<div class="col-sm">
			<h3 class="thin text-center"><b>Регистрация</b></h3>

			@if ($name != 'NULL')
			<h3 class="thin text-center"><b>{{print_r($name)}}</b></h3>
			@endif
			<p class="text-center text-muted"><a href="http://project.std-322.ist.mospolytech.ru/index.php/signin">Авторизация</a></p>
			<hr>

			<form method="POST" action="{{ route('register') }}">
				@csrf
				{{ csrf_field() }}
				<div class="top-margin">
					<label>Имя<span class="text-danger">*</span></label>
					<input id="name" name="name" type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" required autocomplete="name" autofocus>
					@error('name')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                    @enderror
				</div>
				<div class="top-margin">
					<label>Фамилия</label>
					<input name="name2" type="text" class="form-control">
				</div>
				<div class="top-margin">
					<label>Email<span class="text-danger">*</span></label>
					<input id="email" type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email">
					@error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
				</div>

				<div class="row top-margin">
					<div class="col-sm-6">
						<label>Пароль<span class="text-danger">*</span></label>
						<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
					</div>
					<div class="col-sm-6">
						<label>Подтвердите пароль<span class="text-danger">*</span></label>
						<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
					</div>
				</div>

				<hr>
				<!--
				<div class="row">
					<div class="col-sm-1"></div>
					<div class="col-sm-11">
						<div class="g-recaptcha" data-sitekey="6LcGQsAUAAAAAJpDNuDA-5jPkE0i3c83zYmNrV8K"></div>                    
					</div>
				</div>

				<hr> 
			-->

				<div class="row">
					<div class="col-lg-12 text-center">
						<button class="btn btn-action" style="width:100%;" type="submit">Зарегистрироваться</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-sm">
		</div>
	</div>


</div>
@endsection