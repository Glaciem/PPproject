@if(isset($req[0]))
	<?php echo '<meta http-equiv="refresh" content="0; http://project.std-322.ist.mospolytech.ru/index.php/'.$req[1].'">'; ?>
@else
@extends('Main.layout')

@section('content')


<?PHP $array = array();

?>

@foreach (array_keys($req) as $name)
@if ($name != '_token')
<?php $array[] = $name ?>
@endif				
@endforeach

<?php

if(!isset($req['Обоснование'])){
	$json = json_encode($array);
	setcookie('Hypothesis', $json);
}

//if(isset($ret))
	//header("Refresh:0");



//foreach($req as $name)
	//foreach ($name['ugroza'] as $name2)
		//echo $ret[0]['id'];
?>

	<main role="main" class="col">
		<div class="jumbotron shadow">
			<h2 class="headers_text">Этап 6. Выбор целей объекта оценки</h2>
			@if(empty($uses_aims))
			<h4>Цели не сформированы!</h4>
			<h5>Сформируйте хотя бы одну цель, чтобы двигаться дальше</h5>
			@else
			<?php $s=0; $v=0;?>
			@for($i = 0; $i < 2; $i++) 
			<form method="POST" action= "step7">
				{{ csrf_field() }}
				<div class="accordion" id="accordionExample">
					@foreach ($uses_aims as $name)
					@if(($i == 0 && $name->userid == 0) || ($i == 1 && $name->userid == Auth::user()->id))
					@if($s == 0 && $i == 0 && $name->userid == 0)
					<?php $s = 1; ?>
					<h4>Стандартные цели объекта оценки:</h4>
					@endif
					@if($s == 0 && $i == 1)
					<?php $s = 1; ?>
					<h5>Стандартные цели объекта оценки не сформированы</h5>
					@endif
					@if($v == 0 && $i == 1 && $name->userid == Auth::user()->id)
					<?php $v = 1; ?>
					<br><h4>Ваши цели объекта оценки:</h4>
					@endif
					<div class="card">
						<div class="card-header row" id="heading{{$name->id}}">
						<div class="col-sm">
							<input class="bigcheck" name="{{$name->id}}" id="{{$name->id}}" type="checkbox" checked="checked" onclick="checkArrowNextButton();"/>
						</div>
						<div class="col-sm-11">
							<h5 class="mb-0">
								<button class="btn btn-link accordtext" type="button" data-toggle="collapse" data-target="#collapse{{$name->id}}" aria-expanded="true" aria-controls="collapse{{$name->id}}">
									{{$name->Название}}
								</button>
							</h5>
						</div>
						</div>
						<div id="collapse{{$name->id}}" class="collapse" aria-labelledby="heading{{$name->id}}" data-parent="#accordionExample">
							<div class="card-body">
								<b>Название:</b> {{$name->Название}} <br>
								<b>Описание:</b> {{$name->Описание}} <br>
								<b>Среда:</b> {{$name->Среда}} <br>
								<b>Тип:</b> {{$name->Тип}} <br>
								<b>Обоснование:</b> {{$name->Обоснование}} <br>				
							</div>
						</div>
					</div>
					@endif
					@endforeach
				</div>
				@endfor
				@endif
				@if(!empty($id_un_uses_threats) || !empty($id_un_uses_policy))
				<br>
				<hr>
				@endif
				@if(!empty($id_un_uses_threats))
				<h5>Нераспределенные угрозы объекта оценки:</h5>
				<div class="accordion" id="accordionExample1">
					@foreach ($id_un_uses_threats as $name2)
					@foreach ($name2 as $name)
					<div class="card">
						<div class="card-header row" id="heading{{$name->id}}ugroza">
						<div class="col-sm-12">
							<h5 class="mb-0">
								<button class="btn btn-link accordtext" type="button" data-toggle="collapse" data-target="#collapse{{$name->id}}ugroza" aria-expanded="true" aria-controls="collapse{{$name->id}}ugroza">
									{{$name->Аннотация}}
								</button>
							</h5>
						</div>
						</div>
						<div id="collapse{{$name->id}}ugroza" class="collapse" aria-labelledby="heading{{$name->id}}ugroza" data-parent="#accordionExample1">
							<div class="card-body">
								<b>Аннотация:</b> {{$name->Аннотация}} <br>
								<b>Источники:</b> {{$name->Источники}} <br>
								<b>Способ реализации:</b> {{$name->Способ_реализации}} <br>
								<b>Используемые уязвимости:</b> {{$name->Используемые_уязвимости}} <br>
								<b>Вид информационных ресурсов потенциально подверженных угрозе:</b> {{$name->Вид_информационных_ресурсов_потенциально_подверженных_угрозе}} <br>
								<b>Нарушаемые свойства безопасности информационных ресурсов:</b> {{$name->Нарушаемые_свойства_безопасности_информационных_ресурсов}} <br>
								<b>Возможные последствия реализации:</b> {{$name->Возможные_последствия_реализации}} <br>
								<b>Рубрика:</b> {{$name->Рубрика}} <br>
								<b>Мера:</b> {{$name->Мера}} <br>
								<b>Среда:</b> {{$name->Среда}} <br>
								<b>Тип:</b> {{$name->Тип}}								
							</div>
						</div>
					</div>
					@endforeach
					@endforeach
				</div>
				@endif
				@if(!empty($id_un_uses_policy))
				<br>
				<h5>Нераспределенные политики:</h5>
				<div class="accordion" id="accordionExample2">
					@foreach ($id_un_uses_policy as $name2)
					@foreach ($name2 as $name)
					<div class="card">
						<div class="card-header row" id="heading{{$name->id}}policy">
						<div class="col-sm-12">
							<h5 class="mb-0">
								<button class="btn btn-link accordtext" type="button" data-toggle="collapse" data-target="#collapse{{$name->id}}policy" aria-expanded="true" aria-controls="collapse{{$name->id}}policy">
									{{$name->Политика}}
								</button>
							</h5>
						</div>
						</div>
						<div id="collapse{{$name->id}}policy" class="collapse" aria-labelledby="heading{{$name->id}}policy" data-parent="#accordionExample2">
							<div class="card-body">
								<b>Политика:</b> {{$name->Политика}} <br>
								<b>Рубрика:</b> {{$name->Рубрика}} <br>
								<b>Мера:</b> {{$name->Мера}} <br>
								<b>Тип:</b> {{$name->Тип}} <br>
								<b>Среда:</b> {{$name->Среда}} <br>								
							</div>
						</div>
					</div>
					@endforeach
					@endforeach
				</div>
				@endif
		</div>
	</main>

@endsection
@endif