<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAimsThreatsEnviTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aims_threats_envi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_цели_среды');
            $table->integer('id_угроз');
            $table->integer('id_политик');
            $table->integer('id_предположений');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aims_threats_envi');
    }
}
