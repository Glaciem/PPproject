<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Hypothesis extends Model
{
    protected $table = 'hypotheses';

	public static function hypotheses($array) {
		$hypotheses = static::where('Среда', $array['select1'])
			->where('Тип', $array['select2'])
			->where('userid', 0)
    		->orwhere(function ($query) use ($array){
    			$query->where('Среда', $array['select1'])
    				->where('Тип', $array['select2'])
    				->where('userid', Auth::user()->id);
    		})
    		->get();
		return $hypotheses;
	}

	public static function add($array, $coockie) {
		$co = array();
		foreach ($array['Рубрика'] as $name) {
			$co[] = $name;
		}
		$rub = implode(", ", $co);
		$s = static::insert(['userid' => Auth::user()->id, 
  			'Категория' => $array['Категория_предположения'],
			'Предположение' => $array['Предположение'],
  			'Рубрика' => $rub,
  			'Мера' => $array['Мера'],
  		  	'Тип' => $coockie['select2'],  
  			'Среда' => $coockie['select1']]);
	}

	public static function id_hypothesis($array) {
		$hypothesis = array();
		foreach ($array as $id) {
			$hypothesis[] = static::where('id', $id)->get();
		}
		return $hypothesis;
	}

	public static function all_for_user(){
		$hypothesis = static::where('userid', Auth::user()->id)->get();
		return $hypothesis;
	}

	public static function del($id){
		$id = mb_substr($id, 0, -1);
		static::where('id', $id)->delete();
		return "Great";
	}
}
