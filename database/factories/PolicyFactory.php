<?php

use App\Policy;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Policy::class, function (Faker $faker) {

    return [
        'Политика' => $faker->sentence(10),
        'Рубрика' => $faker->sentence(10),
        'Мера' => $faker->sentence(10),
        'Среда_разработки' => $faker->sentence(10),
        'Среда' => $faker->randomElement(['ОС', 'МЭ']),
    ];
});
