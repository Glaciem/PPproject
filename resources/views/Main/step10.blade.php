

@extends('Main.layout')

@section('content')

<?PHP 
if(isset($req[0])){
	//echo $req[1];	
	echo '<meta http-equiv="refresh" content="0; http://project.std-322.ist.mospolytech.ru/index.php/'.$req[1].'">';
}

//if(isset($zavisimost))
//			header("Refresh:0");

$mass = array();
if(isset($req['subcomponent'])){
	foreach ($req['subcomponent'] as $name){
		$mass[] = $name;
	}
	$json = json_encode($mass);
	setcookie('Asubelements', $json);
}

$array = array();
$array2 = array();
$array3 = array();
$newcomponent2 = array();
$newcomponent = array();
$comparr = array();
$comparr = array();
$count3 = 0;

if($arrayComps != "<(0)"){
	foreach ($arrayComps as $name){
		foreach ($name as $value) {
			$newcomponent2[] = $value;
		}
	}
	foreach ($newcomponent2 as $name){
		$array3[] = $name['id'];
	}
	$array3 = array_values(array_unique($array3));
//echo '<pre>', print_r($array3);
	foreach ($newcomponent2 as $name){
		$i = -1;
		foreach ($array3 as $value) {
			$i++;
			if($name['id'] == $value){
				$newcomponent[] = $name;
				$array3[$i] = "<(0)";
			}
		}
	}
	foreach ($newcomponent as $name){
		$array[] = $name['id_класса'];
	}
	$class = array_count_values ($array);
	$id_class = array_values(array_unique($array));
}
?>

<main role="main" class="col">
	<div class="jumbotron shadow">
		<h2 class="headers_text">Этап 10. Выбор компонентов доверия для целей объекта оценки</h2>

		<form method="POST" action= "step11">
			{{ csrf_field() }}
			@if($arrayComps != "<(0)")
			<table class="table table-hover table-bordered" style="margin-bottom: 0rem;">
				<thead>
					<tr>
						<th scope="col" style="width:400px">Класс доверия</th>
						<th scope="col">Компонент доверия</th>
						<th scope="col" style="width:30px"> </th>
					</tr>
				</thead>
				<tbody>

					@foreach ($class as $index)
					<?PHP 
					$count2 = 0;
					?>
					@foreach ($newcomponent as $name)
					@if ($name['id_класса'] == $id_class[$count3])
					<?PHP $count2++; ?>
					@if ($count2 == 1)
					<tr>
						<td rowspan="{{$index}}" style="width:400px"><?PHP echo $classtab[$name['id_класса']-1]['Класс']; ?></td>
						@if ($name['userid'] == 0)
						<td>{{$name->Компонент}}</td>
						<td style="width:30px"><input name="component[]" value="{{$name->id}}" id="{{$name->id}}" type="checkbox" checked="checked" style="transform:scale(2.0);" onclick="checkArrowNextButton();"/></td>
						@else
						<td><b>{{$name->Компонент}}</b></td>
						<td style="width:30px"><input name="addcomponent[]" value="{{$name->id}}" id="{{$name->id}}" type="checkbox" checked="checked" style="transform:scale(2.0);" onclick="checkArrowNextButton();"/></td>
						@endif
					</tr>
					@else
					<tr>
						@if ($name['userid'] == 0)
						<td>{{$name->Компонент}}</td>
						<td style="width:30px"><input name="component[]" value="{{$name->id}}" id="{{$name->id}}" type="checkbox" checked="checked" style="transform:scale(2.0);" onclick="checkArrowNextButton();"/></td>
						@else
						<td><b>{{$name->Компонент}}</b></td>
						<td style="width:30px"><input name="addcomponent[]" value="{{$name->id}}" id="{{$name->id}}" type="checkbox" checked="checked" style="transform:scale(2.0);" onclick="checkArrowNextButton();"/></td>
						@endif
					</tr>
					@endif
					@endif
					@endforeach
					<?PHP $count3++; ?>
					@endforeach
					<?PHP
					$count2 = 0;
					$count3 = 0;
					?>
				</tbody>
			</table>
			@else
			<h4>Неудалось найти компоненты к выбранным целям объектов оценки. Необходимо их создать</h4>
			@endif
			<?php //echo '<pre>', print_r($arrayComps), '</pre>'; ?>
			<?php // echo '<pre>', print_r($classtab), '</pre>'; ?>
		</div>
	</main>
	@endsection