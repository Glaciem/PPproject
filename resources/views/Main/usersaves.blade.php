@extends('Main.layout')

@section('content')

@guest
<main role="main" class="col">
	<div class="jumbotron shadow">
		<div class="col">
			<h1 style="text-align: center;"><b><?php header('Refresh: 0; URL=/index.php'); ?></b></h1>
		</div>
	</div>
</main>
@else
<main role="main" class="col">
	<div class="jumbotron shadow">
		<h2 class="headers_text">Список создаваемых вами профилей защиты</h2>
			<br><button onclick="pechenki(this);" id="z0vstep1" class=" btn-primary send" type="submit" style="width:100%; padding: 10px; font-weight: bold; border-radius: 3px	;">CОЗДАТЬ НОВЫЙ ПРОФИЛЬ ЗАЩИТЫ</button>
			<div class="accordion" id="accordionExample{{1}}" style="padding-top: 10px;">
			@foreach($all as $name)
				<div class="card">
					<div class="card-header row" id="heading{{$name['id']}}">
						<div class="col-sm-12">
							<h5 class="mb-0">
								<button class="btn btn-link accordtext" type="button" data-toggle="collapse" data-target="#collapse{{$name['id']}}" aria-expanded="true" aria-controls="	collapse{{$name['id']}}">
									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" 	stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 	9"></polyline></svg>{{$name['name']}}
								</button>
							</h5>
						</div>
					</div>
					<div id="collapse{{$name['id']}}" class="collapse" aria-labelledby="heading{{$name['id']}}" data-parent="#accordionExample{{1}}">
						<div class="card-body" style="padding-top: 5px;">
							<?php $s = "#"; $o = 1;
								$steps = ['Этап 1. "Начало"', 'Этап 2. "Угрозы ОО"', 'Этап 3. "Угрозы среды"', 'Этап 4. "Политики"', 'Этап 5. "Предположения"', 'Этап 6. "Цели ОО"', 'Этап 7. "Цели среды"', 'Этап 8. "Компоненты доверия"', 'Этап 9. "Подкомпоненты"', 'Этап 10. "Компоненты требований"', 'Этап 11. "Подкомпоненты требований"', 'Этап 12. "Формирование"'];
								for($i = 1; $i < 12; $i++)
									if($name['step'.$i] != NULL){
										$s = "step".($i + 1);
										$o = $i;
									}
								echo '<b>'.$steps[$o].'</b>'; 
							?>
							<br><button onclick="pechenki(this);" id="z{{$name['id']}}v{{$s}}" class=" btn-primary send" type="submit" style="width:50%; padding: 10px; font-weight: bold; border-radius: 3px	;">ПРОДОЛЖИТЬ</button>
							<form method="POST" style="display:inline;">
							<input name="{{$name['id']}}" id="{{$name['id']}}check" hidden=true type="checkbox"/>
							{{ csrf_field() }}
							<button onclick="placeArrow(this);" id="{{$name['id']}}" class=" btn-primary send" type="submit" style="width:49%; padding: 10px; font-weight: bold; border-radius: 3px;">УДАЛИТЬ</button></form>			
						</div>
					</div>
				</div>
			@endforeach
			</div>		
	</div>	
</main>

<script type="text/javascript">
	function CookiesDelete() {
		var cookies = document.cookie.split(";");
		for (var i = 0; i < cookies.length; i++) {
			var cookie = cookies[i];
			var eqPos = cookie.indexOf("=");
			var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
			document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT;";
			document.cookie = name + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		}
	}
	function pechenki(el){
		CookiesDelete();
		var id = el.id.slice(1, el.id.indexOf("v"));
		document.cookie = "WorkWith=" + id;
		//alert(document.cookie);
		var step = el.id.slice(el.id.indexOf("v") + 1);
		window.location.href = "/index.php/" + step;
	}
	function placeArrow(el){
		document.getElementById(el.id + "check").checked="checked";
	}
</script>

@endguest
@endsection
