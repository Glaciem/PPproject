<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Policy extends Model
{
    protected $table = 'policy';

	public static function policy($array) {
		$policy = static::where('Среда', $array['select1'])
		->where('Тип', $array['select2'])
		->where('userid', 0)
		->orwhere(function ($query) use ($array){
              $query->where('Среда', $array['select1'])
                ->where('Тип', $array['select2'])
                ->where('userid', Auth::user()->id);
            })  
        ->get(); 
		return $policy;
	}

	public static function add($array, $coockie) {
		$co = array();
		foreach ($array['Рубрика'] as $name) {
			$co[] = $name;
		}
		$rub = implode(", ", $co);
		$s = static::insert(['userid' => Auth::user()->id, 
  			'Политика' => $array['Политика'],
  			'Рубрика' => $rub,
  			'Мера' => $array['Мера'],
  			'Тип' => $coockie['select2'],	  
  			'Среда' => $coockie['select1']]);
	}

	public static function id_policy($array) {
		$policy = array();
		foreach ($array as $id) {
			$policy[] = static::where('id', $id)->get();
		}
		return $policy;
	}

	public static function all_for_user(){
		$policy = static::where('userid', Auth::user()->id)->get();
		return $policy;
	}

	public static function del($id){
		$id = mb_substr($id, 0, -1);
		static::where('id', $id)->delete();
		return "Great";
	}
}
