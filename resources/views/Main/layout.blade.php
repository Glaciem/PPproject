<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	
	<title>Progressus</title>

	<!-- <link rel="shortcut icon" href="assets/images/gt_favicon.png"> -->
	<link rel="shortcut icon" href="{{ URL::asset('assets/images/gt_favicon.png') }}"/>

	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700"/>

	<!-- <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css"> -->

	<link rel="stylesheet" href="{{ URL::asset('bootstrap-4.3.1-dist/css/bootstrap.css') }}"/> 
	<!--<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	
	<link rel="stylesheet" href="assets/css/font-awesome.min.css"> 
	<link rel="stylesheet" href="assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="assets/css/main.css">
-->

<link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css') }}"/> 
<link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap-theme.css') }}" media="screen"/>
<link rel="stylesheet" href="{{ URL::asset('assets/css/main.css') }}"/>
<link rel="stylesheet" href="{{ URL::asset('assets/css/my.css') }}"/>

<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">-->


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
<![endif]-->


<style>
	@media screen and (max-width: 1600px){		
		.rs{ 
			display: none; 
		}
	}
	@media screen and (max-height: 830px){	
		.prokrutka2 {
			max-height: 400px;		
			overflow-y: auto;
		}
	}
	.headers_text {
		margin-top: 0px;
	}	
	.prokrutka {
		height: 400px; /* высота нашего блока */
		width: 100%; /* ширина нашего блока */
		background: #fff; /* цвет фона, белый */
		border: 1px solid #C1C1C1; /* размер и цвет границы блока */
		overflow-x: scroll; /* прокрутка по горизонтали */
		/*overflow-y: scroll; /* прокрутка по вертикали */
	}
	.bigcheck{
		position: absolute;
		top: 40%;
		left: 40px;
		transform:scale(5.0);
	}
	.bigcheckmodal{
		position: absolute;
		top: 40%;
		left: 40px;
		transform:scale(5.0);
	}
	.accordtext{
		text-align: left;
		padding: 10px 10px;
	}
	.table {
		background: #FFFFFF;
	}

</style>
</head>
<body>
	@include('layouts.nav')
	@if (isset($step))
	<br>
	<br>
	<br>
	<div class="container-fluid">
		<div class="row justify-content-md-center">
			@include('layouts.leftnav')
			@endif			
			@if (isset($step))<div class="col-md-8">@endif
				@if(isset($target))
				<form method="POST">
					{{ csrf_field() }}
					@include('layouts.modal')
				</form>
				@endif
				@if(isset($target2))
				<form method="POST">
					{{ csrf_field() }}
					@include('layouts.secondmodal')
				</form>
				@endif
				@if(!isset($usersparams)) @yield('content') @endif
			@if (isset($step))</div>@endif
			@if (isset($step))
			@include('layouts.rightnav')
		</div>
		@endif
	</div>

	@if (isset($usersparams))
	<div class="container-fluid">
		<div class="row justify-content-md-center">
			@endif			
			@if (isset($usersparams))<div class="col-md-8">
				@yield('content') @endif
			@if (isset($usersparams))</div>@endif
			@if (isset($usersparams))
		</div>
		@endif
	</div>

	@if (!isset($step))
	@include('layouts.footer')	
	@endif


	@if (isset($step))
	@if ($step == '2' || $step == '3' || $step == '4' || $step == '5' || $step == '6' || $step == '7' || $step == '8' || $step == '10')
	<script type="text/javascript">
		function checkArrowNextButton(){
			id = 0;
			ok = false;
			<?php
			$s;
			$s2;
			if ($step == '2' || $step == '3')
				$s = $threats;
			if ($step == '4' || $step == '5')
				$s = $tabs;
			if ($step == '6' || $step == '7')
				$s = $uses_aims;
			if ($step == '8' || $step == '10'){
				$s = $newcomponent;
				if(isset($pointed))
					$s2 = $pointed2;
			}
			?>
			<?php 
			foreach ($s as $name){ ?>
				id = <?php echo $name['id']; ?>;
				if(document.getElementById(id).checked == true){
					ok = true;						
				}
			<?php } 
			if($step == '8' && isset($pointed)){
				foreach ($s2 as $name){ ?>
					id = <?php echo $name['id']; ?>;
					if(document.getElementById(id).checked == true){
						ok = true;						
					}
				<?php } }?>
				if(ok == true)
					document.getElementById("NextButton").hidden = false;
				else
					document.getElementById("NextButton").hidden = true;
			}
		</script>
		@endif
		@endif

		<script src='https://www.google.com/recaptcha/api.js'></script>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

		<!-- JavaScript libs are placed at the end of the document so the pages load faster -->

	<!-- 
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	
	<script src="/project/resources/js/bootstrap.js"></script>
	<script src="/assets/js/headroom.min.js"></script>
	<script src="/assets/js/jQuery.headroom.min.js"></script>
	<script src="/assets/js/template.js"></script>-->
	<!-- <script src='/public/bootstrap-4.3.1-dist/js/bootstrap.js'></script> -->

<!--
<script type="text/javascript">
	var toggler = document.getElementsByClassName("caret");
	var i;

	for (i = 0; i < toggler.length; i++) {
		toggler[i].addEventListener("click", function() {
			this.parentElement.querySelector(".nested").classList.toggle("active");
			this.classList.toggle("caret-down");
		});
	}
	
</script>-->

</body>
</html>
